package com.stupidbot.universalcorep;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.stupidbot.universalcorep.custom_mobs.CustomMobs;
import com.stupidbot.universalcorep.listeners.CommandClass;
import com.stupidbot.universalcorep.listeners.OnAsyncPlayerChat;
import com.stupidbot.universalcorep.listeners.OnAsyncPlayerPreLogin;
import com.stupidbot.universalcorep.listeners.OnBlockBreak;
import com.stupidbot.universalcorep.listeners.OnBlockPlace;
import com.stupidbot.universalcorep.listeners.OnCraftItem;
import com.stupidbot.universalcorep.listeners.OnEat;
import com.stupidbot.universalcorep.listeners.OnEntityDamageEntity;
import com.stupidbot.universalcorep.listeners.OnEntityInteract;
import com.stupidbot.universalcorep.listeners.OnFish;
import com.stupidbot.universalcorep.listeners.OnFoodLevelChange;
import com.stupidbot.universalcorep.listeners.OnInventoryClick;
import com.stupidbot.universalcorep.listeners.OnInventoryClose;
import com.stupidbot.universalcorep.listeners.OnItemDespawn;
import com.stupidbot.universalcorep.listeners.OnItemPickup;
import com.stupidbot.universalcorep.listeners.OnJoin;
import com.stupidbot.universalcorep.listeners.OnLogOut;
import com.stupidbot.universalcorep.listeners.OnPlayerAnimation;
import com.stupidbot.universalcorep.listeners.OnPlayerDeath;
import com.stupidbot.universalcorep.listeners.OnPlayerInteract;
import com.stupidbot.universalcorep.listeners.OnProjectileHit;
import com.stupidbot.universalcorep.listeners.OnProjectileLaunch;
import com.stupidbot.universalcorep.listeners.OnSneak;
import com.stupidbot.universalcorep.message_channels.LOLIMAHCKER;
import com.stupidbot.universalcorep.miscellaneous.Placeholders;
import com.stupidbot.universalcorep.mutations.Mutations;
import com.stupidbot.universalcorep.utils.Configs;
import com.stupidbot.universalcorep.utils.Leaderboards;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

public class Main extends JavaPlugin implements Listener {
	public static Plugin plugin;
	public static Economy econ = null;
	public static Chat chat = null;
	public static Permission perms = null;
	public static final List<Entity> removeList = new ArrayList<>();

	@Override
	public void onEnable() {
		plugin = this;
		Configs.loadConfiguration();
		getServer().getMessenger().registerIncomingPluginChannel(this, "LOLIMAHCKER", new LOLIMAHCKER());
		registerEvents(this, new OnJoin(), new OnInventoryClick(), new OnLogOut(), new OnPlayerInteract(),
				new OnPlayerAnimation(), new OnEntityInteract(), new OnItemDespawn(), new OnItemPickup(),
				new OnBlockBreak(), new OnProjectileLaunch(), new OnProjectileHit(), new OnSneak(), new OnPlayerDeath(),
				new OnAsyncPlayerChat(), new OnAsyncPlayerPreLogin(), new OnFoodLevelChange(), new OnEat(),
				new OnBlockPlace(), new OnCraftItem(), new OnInventoryClose(), new OnEntityDamageEntity(),
				new OnFish());
		getCommand("spaceship").setExecutor(new CommandClass());
		getCommand("stats").setExecutor(new CommandClass());
		getCommand("hatchlootegg").setExecutor(new CommandClass());
		getCommand("givelootegg").setExecutor(new CommandClass());
		getCommand("givealllootegg").setExecutor(new CommandClass());
		getCommand("cosmetics").setExecutor(new CommandClass());
		getCommand("ban").setExecutor(new CommandClass());
		getCommand("shop").setExecutor(new CommandClass());
		getCommand("updateleaderboard").setExecutor(new CommandClass());
		getCommand("throw").setExecutor(new CommandClass());
		getCommand("throwall").setExecutor(new CommandClass());
		getCommand("boost").setExecutor(new CommandClass());
		getCommand("kaboom").setExecutor(new CommandClass());
		getCommand("kaboomall").setExecutor(new CommandClass());
		getCommand("ping").setExecutor(new CommandClass());
		getCommand("xp").setExecutor(new CommandClass());
		getCommand("deliverybot").setExecutor(new CommandClass());
		getCommand("redeemreferral").setExecutor(new CommandClass());
		getCommand("nothing").setExecutor(new CommandClass());
		getCommand("levelup").setExecutor(new CommandClass());
		getCommand("ourplans").setExecutor(new CommandClass());
		getCommand("setstamina").setExecutor(new CommandClass());
		getCommand("spectate").setExecutor(new CommandClass());
		getCommand("giverankmission").setExecutor(new CommandClass());
		getCommand("givevoucher").setExecutor(new CommandClass());
		getCommand("pv").setExecutor(new CommandClass());
		getCommand("givebooster").setExecutor(new CommandClass());
		getCommand("giveitemnametag").setExecutor(new CommandClass());
		getCommand("tip").setExecutor(new CommandClass());
		getCommand("universalcorepreload").setExecutor(new CommandClass());
		getCommand("spawncustommob").setExecutor(new CommandClass());
		getCommand("givelootchest").setExecutor(new CommandClass());
		getCommand("kit").setExecutor(new CommandClass());
		getCommand("rankup").setExecutor(new CommandClass());
		getCommand("testfishprize").setExecutor(new CommandClass());
		CustomMobs.registerEntities();
		getServer().getPluginManager().registerEvents(this, this);
		if (!setupEconomy())
			getServer().getPluginManager().disablePlugin(this);
		if (!(new File(plugin.getDataFolder() + File.separator + "Player Data").exists()))
			new File(plugin.getDataFolder() + File.separator + "Player Data").mkdirs();
		if (!(new File(plugin.getDataFolder() + File.separator + "Player Vaults").exists()))
			new File(plugin.getDataFolder() + File.separator + "Player Vaults").mkdirs();
		setupChat();
		setupPermissions();
		setupEnchants();
		Leaderboards.startLeaderboards();
		if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI"))
			new Placeholders().hook();
		System.out.println(getName() + " Is now enabled!");
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public void onDisable() {
		CustomMobs.unregisterEntities();
		for (Player all : Bukkit.getOnlinePlayers()) {
			all.setGameMode(GameMode.SURVIVAL);
			all.teleport(new Location(Bukkit.getServer().getWorld("world"), -10.5D, 99.5D, -15.5D, 0.0F, 0.0F));
			all.closeInventory();
			all.setAllowFlight(false);
		}
		for (Entity aRemoveList : removeList)
			aRemoveList.remove();
		Iterator<Location> iterator = OnBlockBreak.blockReset.keySet().iterator();
		for (int i = 0; i < OnBlockBreak.blockReset.size(); i++) {
			Location key = iterator.next();
			String[] blockType = OnBlockBreak.blockReset.get(key).split("\\:");
			key.getBlock().getChunk().load(true);
			key.getBlock().setType(Material.getMaterial(blockType[0]));
			key.getBlock().setData(Byte.parseByte(blockType[1]));
		}
		removeList.clear();
		OnBlockBreak.blockReset.clear();
		plugin = null;
		Bukkit.getScheduler().cancelAllTasks();
		try {
			Field byIdField = Enchantment.class.getDeclaredField("byId");
			Field byNameField = Enchantment.class.getDeclaredField("byName");
			byIdField.setAccessible(true);
			byNameField.setAccessible(true);
			HashMap<Integer, Enchantment> byId = (HashMap<Integer, Enchantment>) byIdField.get(null);
			HashMap<Integer, Enchantment> byName = (HashMap<Integer, Enchantment>) byNameField.get(null);
			for (Enchantment ench : Mutations.LIST) {
				byId.remove(ench.getId());
				byName.remove(ench.getName());
			}
		} catch (Exception e) { // Hide Silently
		}
	}

	private void setupEnchants() {
		try {
			Field f = Enchantment.class.getDeclaredField("acceptingNew");
			f.setAccessible(true);
			f.set(null, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			for (Enchantment ench : Mutations.LIST)
				Enchantment.registerEnchantment(ench);
		} catch (IllegalArgumentException e) { // Hide Error
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(getName() + " Is now disabled!");
	}

	private boolean setupEconomy() {
		RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager()
				.getRegistration(Economy.class);
		if (economyProvider != null) {
			econ = economyProvider.getProvider();
		}
		return econ != null;
	}

	private void setupChat() {
		RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
		chat = rsp.getProvider();
	}

	private void setupPermissions() {
		RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
		perms = rsp.getProvider();
	}

	private static void registerEvents(Plugin plugin, Listener... listeners) {
		Listener[] arrayOfListener;
		int j = (arrayOfListener = listeners).length;
		for (int i = 0; i < j; i++) {
			Listener listener = arrayOfListener[i];
			Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
		}
	}
}