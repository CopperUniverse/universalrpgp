package com.stupidbot.universalcorep.mutations.swords_only;

import com.stupidbot.universalcorep.mutations.Mutations;
import com.stupidbot.universalcorep.utils.Stats;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

public class Sharpener extends Mutations {
	public Sharpener() {
		super(107);
	}

	@Override
	public int getId() {
		return 107;
	}

	@Override
	public boolean canEnchantItem(ItemStack arg0) {
        return Stats.pvpItems.contains(arg0.getType());
	}

	@Override
	public boolean conflictsWith(Enchantment arg0) {
		return false;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return null;
	}

	@Override
	public int getMaxLevel() {
		return 10;
	}

	@Override
	public String getName() {
		return "Sharpener";
	}

	@Override
	public int getStartLevel() {
		return 1;
	}

	@Override
	public String getDescription() {
		return "Increases damage dealt, affected by 'Luck' stat.";
	}
}
