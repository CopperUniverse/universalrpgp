package com.stupidbot.universalcorep.mutations;

import com.stupidbot.universalcorep.mutations.multiple.Slippery;
import com.stupidbot.universalcorep.mutations.pickaxes_only.*;
import com.stupidbot.universalcorep.mutations.swords_only.Sharpener;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class Mutations extends Enchantment {
	public final static Enchantment SLIPPERY = new Slippery();
	public final static Enchantment EXPERIENCED = new Experienced();
	public final static Enchantment DUPLICATION = new Duplication();
	public final static Enchantment HEAVY = new Heavy();
	public final static Enchantment ENRICHER = new Enricher();
	public final static Enchantment ENERGIZER = new Energizer();
	public final static Enchantment SHARPENER = new Sharpener();
	// public final static Enchantment EFFICIENT = new Efficient();
	public static final List<Enchantment> GOOD_PICKAXES = new ArrayList<>();
	public static final List<Enchantment> BAD_PICKAXES = new ArrayList<>();
	public static final List<Enchantment> GOOD_SWORDS = new ArrayList<>();
	public static final List<Enchantment> BAD_SWORDS = new ArrayList<>();
	private static final List<Enchantment> GOOD = new ArrayList<>();
	private static final List<Enchantment> BAD = new ArrayList<>();
	public static final List<Enchantment> LIST = new ArrayList<>();
	static {
		GOOD_PICKAXES.add(EXPERIENCED);
		GOOD_PICKAXES.add(DUPLICATION);
		GOOD_PICKAXES.add(ENRICHER);
		GOOD_PICKAXES.add(ENERGIZER);
		// GOOD_PICKAXES.add(EFFICIENT);
		BAD_PICKAXES.add(SLIPPERY);
		BAD_PICKAXES.add(HEAVY);
		GOOD_SWORDS.add(SHARPENER);
		BAD_SWORDS.add(SLIPPERY);

		GOOD.addAll(GOOD_PICKAXES);
		BAD.addAll(BAD_PICKAXES);
		GOOD.addAll(GOOD_SWORDS);
		BAD.addAll(BAD_SWORDS);
		LIST.addAll(GOOD);
		LIST.addAll(BAD);
	}

	protected Mutations(int id) {
		super(id);
	}

	@Override
	public boolean canEnchantItem(ItemStack arg0) {
		return false;
	}

	@Override
	public boolean conflictsWith(Enchantment arg0) {
		return false;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return null;
	}

	@Override
	public int getMaxLevel() {
		return 0;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public int getStartLevel() {
		return 0;
	}

	public String getDescription() {
		return null;
	}
}