package com.stupidbot.universalcorep.mutations.multiple;

import com.stupidbot.universalcorep.mutations.Mutations;
import com.stupidbot.universalcorep.utils.Stats;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

public class Slippery extends Mutations {
	public Slippery() {
		super(101);
	}

	@Override
	public int getId() {
		return 101;
	}

	@Override
	public boolean canEnchantItem(ItemStack arg0) {
        return Stats.miningItems.contains(arg0.getType()) || Stats.pvpItems.contains(arg0.getType());
	}

	@Override
	public boolean conflictsWith(Enchantment arg0) {
		return false;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return null;
	}

	@Override
	public int getMaxLevel() {
		return 10;
	}

	@Override
	public String getName() {
		return "Slippery";
	}

	@Override
	public int getStartLevel() {
		return 1;
	}

	@Override
	public String getDescription() {
		return "Chance to drop your item while using it.";
	}
}
