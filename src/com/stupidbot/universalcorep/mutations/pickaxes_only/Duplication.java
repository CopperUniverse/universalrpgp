package com.stupidbot.universalcorep.mutations.pickaxes_only;

import com.stupidbot.universalcorep.mutations.Mutations;
import com.stupidbot.universalcorep.utils.Stats;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

public class Duplication extends Mutations {
	public Duplication() {
		super(103);
	}

	@Override
	public int getId() {
		return 103;
	}

	@Override
	public boolean canEnchantItem(ItemStack arg0) {
        return Stats.miningItems.contains(arg0.getType());
	}

	@Override
	public boolean conflictsWith(Enchantment arg0) {
		return false;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return null;
	}

	@Override
	public int getMaxLevel() {
		return 10;
	}

	@Override
	public String getName() {
		return "Duplication";
	}

	@Override
	public int getStartLevel() {
		return 1;
	}

	@Override
	public String getDescription() {
		return "Increases maximum possible drops on mined block, affected by 'Luck' stat.";
	}
}
