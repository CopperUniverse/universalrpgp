package com.stupidbot.universalcorep.listeners;

import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.utils.Configs;
import com.stupidbot.universalcorep.utils.ParticleEffect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

public class OnProjectileLaunch implements Listener {
	private static final HashMap<String, ParticleEffect> particleChoose = new HashMap<>();
	static {
		particleChoose.put("default", ParticleEffect.CRIT);
		particleChoose.put("snow", ParticleEffect.SNOW_SHOVEL);
		particleChoose.put("splash", ParticleEffect.WATER_DROP);
		particleChoose.put("slimey", ParticleEffect.SLIME);
		particleChoose.put("black_magic", ParticleEffect.SPELL_MOB);
		particleChoose.put("god's_dust", ParticleEffect.SPELL_INSTANT);
		particleChoose.put("grinded_emerald", ParticleEffect.VILLAGER_HAPPY);
		particleChoose.put("dirty", ParticleEffect.BLOCK_CRACK);
		particleChoose.put("electrocution", ParticleEffect.SPELL_MOB);
		particleChoose.put("enchanted", ParticleEffect.ENCHANTMENT_TABLE);
		particleChoose.put("p.f.u.d.o.r.", ParticleEffect.SPELL_MOB);
		particleChoose.put("curse", ParticleEffect.SPELL_WITCH);
		particleChoose.put("blood", ParticleEffect.BLOCK_DUST);
		particleChoose.put("compressed_air", ParticleEffect.CLOUD);
		particleChoose.put("default_v2", ParticleEffect.CRIT_MAGIC);
		particleChoose.put("smokey", ParticleEffect.SMOKE_NORMAL);
		particleChoose.put("twinkle", ParticleEffect.FIREWORKS_SPARK);
		particleChoose.put("rage", ParticleEffect.VILLAGER_ANGRY);
		particleChoose.put("magma_drop", ParticleEffect.DRIP_LAVA);
		particleChoose.put("water_leak", ParticleEffect.DRIP_WATER);
		particleChoose.put("love", ParticleEffect.HEART);
		particleChoose.put("real_talent", ParticleEffect.NOTE);
		particleChoose.put("crayon", ParticleEffect.REDSTONE);
		particleChoose.put("spicy", ParticleEffect.FLAME);
		particleChoose.put("bling", ParticleEffect.ITEM_CRACK);
		particleChoose.put("chipotle", ParticleEffect.LAVA);
	}

	@EventHandler
	public void onProjectileLaunch(ProjectileLaunchEvent e) {
		if (e.getEntity().getShooter() instanceof Player) {
			String trailString = Configs.loadPlayerFile((Player) e.getEntity().getShooter(), "Player Data")
					.getString("Cosmetics.ProjectileTrail");
			ParticleEffect particle = particleChoose.get(trailString);
			new BukkitRunnable() {
				int r = -1;
				int g = -1;
				int b = -1;

				@Override
				public void run() {
					if (!(trailString.contains("dirty")) && !(trailString.contains("electrocution"))
							&& !(trailString.contains("pfudor")) && !(trailString.contains("blood"))
							&& !(trailString.contains("real_talent")) && !(trailString.contains("bling"))
							&& !(trailString.contains("crayon")))
						particle.display(0, 0, 0, 0, 1, e.getEntity().getLocation(), 256);
					else if (trailString.contains("dirty"))
						particle.display(new ParticleEffect.BlockData(Material.DIRT, (byte) 0), 0, 0, 0, 0, 1,
								e.getEntity().getLocation(), 256);
					else if (trailString.contains("electrocution"))
						particle.display(new ParticleEffect.OrdinaryColor(ThreadLocalRandom.current().nextInt(245, 256),
								ThreadLocalRandom.current().nextInt(210, 220),
								ThreadLocalRandom.current().nextInt(0, 10)), e.getEntity().getLocation(), 256);
					else if (trailString.contains("pfudor"))
						particle.display(
								new ParticleEffect.OrdinaryColor(ThreadLocalRandom.current().nextInt(245, 256),
										ThreadLocalRandom.current().nextInt(60, 70),
										ThreadLocalRandom.current().nextInt(175, 185)),
								e.getEntity().getLocation(), 256);
					else if (trailString.contains("blood"))
						particle.display(new ParticleEffect.BlockData(Material.REDSTONE_BLOCK, (byte) 0), 0, 0, 0, 0, 1,
								e.getEntity().getLocation(), 256);
					else if (trailString.contains("real_talent"))
						particle.display(
								new ParticleEffect.NoteColor((byte) ThreadLocalRandom.current().nextInt(0, 25)),
								e.getEntity().getLocation(), 256);
					else if (trailString.contains("crayon")) {
						if (r == -1)
							switch (ThreadLocalRandom.current().nextInt(0, 13)) {
							case 0:
								r = 253;
								g = 188;
								b = 180;
								break;
							case 1:
								r = 255;
								g = 117;
								b = 56;
								break;
							case 2:
								r = 205;
								g = 149;
								b = 117;
								break;
							case 3:
								r = 255;
								g = 207;
								b = 171;
							case 4:
								r = 252;
								g = 232;
								b = 131;
							case 5:
								r = 29;
								g = 249;
								b = 20;
								break;
							case 6:
								r = 28;
								g = 172;
								b = 120;
								break;
							case 7:
								r = 120;
								g = 219;
								b = 226;
								break;
							case 8:
								r = 31;
								g = 117;
								b = 254;
								break;
							case 9:
								r = 116;
								g = 66;
								b = 200;
							case 10:
								r = 255;
								g = 29;
								b = 206;
								break;
							case 11:
								r = 237;
								g = 237;
								b = 237;
								break;
							case 12:
								r = 35;
								g = 35;
								b = 35;
								break;
							}

						particle.display(new ParticleEffect.OrdinaryColor(r, g, b), e.getEntity().getLocation(), 256);
					} else if (trailString.contains("bling")) {
						Material item = null;

						switch (ThreadLocalRandom.current().nextInt(0, 4)) {
						case 0:
							item = Material.GOLD_INGOT;
							break;
						case 1:
							item = Material.GOLD_SWORD;
							break;
						case 2:
							item = Material.GOLD_PICKAXE;
							break;
						case 3:
							item = Material.GOLD_BARDING;
							break;
						}
						particle.display(new ParticleEffect.ItemData(item, (byte) 0), 0, 0, 0, 0, 1,
								e.getEntity().getLocation(), 256);
					}
					if (e.getEntity().isDead())
						cancel();
				}
			}.runTaskTimerAsynchronously(Main.plugin, 2, 1);
		}
	}
}