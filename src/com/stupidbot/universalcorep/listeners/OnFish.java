package com.stupidbot.universalcorep.listeners;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.stupidbot.universalcorep.utils.MiscellaneousUtils;
import com.stupidbot.universalcorep.utils.Text;

public class OnFish implements Listener { // fish:min_size:max_size:min_weight:max_weight:name:lore:flavor_text:chance_weight
	// TODO refactor
	private static ItemStack[] lootChestCommonItemStacks = { new ItemStack(Material.STRING) };
	private static String[] lootChestCommonStrings = {
			"false:0:0:0:0:1:&rString:null:For some reason these are sought after...;Hello Internet and welcome to STRING-THEORY!:2", };
	private static ItemStack[] lootChestRareItemStacks = { new ItemStack(Material.STRING) };
	private static String[] lootChestRareStrings = {
			"false:0:0:0:0:1:&rString:null:For some reason these are sought after...;Hello Internet and welcome to STRING-THEORY!:2", };
	private static ItemStack[] lootChestLegendaryItemStacks = { new ItemStack(Material.STRING) };
	private static String[] lootChestLegendaryStrings = {
			"false:0:0:0:0:1:&rString:null:For some reason these are sought after...;Hello Internet and welcome to STRING-THEORY!:2", };
	private static ItemStack[] lootChestGodlyItemStacks = { new ItemStack(Material.STRING) };
	private static String[] lootChestGodlyStrings = {
			"false:0:0:0:0:1:&rString:null:For some reason these are sought after...;Hello Internet and welcome to STRING-THEORY!:2", };

	static {
		ArrayList<String> newLootChestCommonStrings = new ArrayList<>();
		ArrayList<ItemStack> newLootChestCommonItemStacks = new ArrayList<>();
		for (int i = 0; i < lootChestCommonStrings.length; i++) {
			String[] stringSplit = lootChestCommonStrings[i].split("\\:");
			for (int t = 0; t < Integer.parseInt(stringSplit[9]); t++) {
				newLootChestCommonStrings.add(lootChestCommonStrings[i]);
				newLootChestCommonItemStacks.add(lootChestCommonItemStacks[i]);
			}
		}
		lootChestCommonStrings = newLootChestCommonStrings.toArray(new String[0]);
		lootChestCommonItemStacks = newLootChestCommonItemStacks.toArray(new ItemStack[0]);

		ArrayList<String> newLootChestRareStrings = new ArrayList<>();
		ArrayList<ItemStack> newLootChestRareItemStacks = new ArrayList<>();
		for (int i = 0; i < lootChestRareStrings.length; i++) {
			String[] stringSplit = lootChestRareStrings[i].split("\\:");
			for (int t = 0; t < Integer.parseInt(stringSplit[9]); t++) {
				newLootChestRareStrings.add(lootChestRareStrings[i]);
				newLootChestRareItemStacks.add(lootChestRareItemStacks[i]);
			}
		}
		lootChestRareStrings = newLootChestRareStrings.toArray(new String[0]);
		lootChestRareItemStacks = newLootChestRareItemStacks.toArray(new ItemStack[0]);

		ArrayList<String> newLootChestLegendaryStrings = new ArrayList<>();
		ArrayList<ItemStack> newLootChestLegendaryItemStacks = new ArrayList<>();
		for (int i = 0; i < lootChestLegendaryStrings.length; i++) {
			String[] stringSplit = lootChestLegendaryStrings[i].split("\\:");
			for (int t = 0; t < Integer.parseInt(stringSplit[9]); t++) {
				newLootChestLegendaryStrings.add(lootChestLegendaryStrings[i]);
				newLootChestLegendaryItemStacks.add(lootChestLegendaryItemStacks[i]);
			}
		}
		lootChestLegendaryStrings = newLootChestLegendaryStrings.toArray(new String[0]);
		lootChestLegendaryItemStacks = newLootChestLegendaryItemStacks.toArray(new ItemStack[0]);

		ArrayList<String> newLootChestGodlyStrings = new ArrayList<>();
		ArrayList<ItemStack> newLootChestGodlyItemStacks = new ArrayList<>();
		for (int i = 0; i < lootChestGodlyStrings.length; i++) {
			String[] stringSplit = lootChestGodlyStrings[i].split("\\:");
			for (int t = 0; t < Integer.parseInt(stringSplit[9]); t++) {
				newLootChestGodlyStrings.add(lootChestGodlyStrings[i]);
				newLootChestGodlyItemStacks.add(lootChestGodlyItemStacks[i]);
			}
		}
		lootChestGodlyStrings = newLootChestGodlyStrings.toArray(new String[0]);
		lootChestGodlyItemStacks = newLootChestGodlyItemStacks.toArray(new ItemStack[0]);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onFish(PlayerFishEvent e) {
		if (e.getState().equals(State.CAUGHT_FISH))
			((Item) e.getCaught()).setItemStack(getFishPrize(e.getPlayer()));
	}

	public static ItemStack getFishPrize(final Player player) {
		ItemStack returnItem;
		String[] prizesChoose;
		ItemStack[] itemsChoose;
		int randomNum = ThreadLocalRandom.current().nextInt(0, 100);

		if (randomNum < 50) {
			prizesChoose = lootChestCommonStrings;
			itemsChoose = lootChestCommonItemStacks;
		} else if (randomNum >= 50 && randomNum < 80) {
			prizesChoose = lootChestRareStrings;
			itemsChoose = lootChestRareItemStacks;
		} else if (randomNum >= 80 && randomNum < 95) {
			prizesChoose = lootChestLegendaryStrings;
			itemsChoose = lootChestLegendaryItemStacks;
		} else {
			prizesChoose = lootChestGodlyStrings;
			itemsChoose = lootChestGodlyItemStacks;
		}

		int index = ThreadLocalRandom.current().nextInt(0, prizesChoose.length);
		String[] prizeSplit = prizesChoose[index].split("\\:");
		returnItem = itemsChoose[index];
		ItemMeta returnItemMeta = returnItem.getItemMeta();
		ArrayList<String> lore = new ArrayList<>();

		player.sendMessage("" + MiscellaneousUtils.getBiasedRandom(0, 5, 0.1));

		if (prizeSplit[0] == "true") { // Is fish
		} else
			Text.openBook(Text.newBook("Non-Fish Loot", "StupidBot", ""), player);

		returnItemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', prizeSplit[6]));

		if (!prizeSplit[7].contains("null")) {
			for (String s : prizeSplit[7].split(";"))
				lore.add(ChatColor.translateAlternateColorCodes('&', s));
		}

		returnItemMeta.setLore(lore);
		returnItem.setItemMeta(returnItemMeta);

		return returnItem;
	}
}