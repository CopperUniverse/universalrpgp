package com.stupidbot.universalcorep.listeners;

import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemDespawnEvent;

import java.util.ArrayList;
import java.util.List;

public class OnItemDespawn implements Listener {
	public static final List<Entity> stopDespawnList = new ArrayList<>();

	@EventHandler
	public void onItemDespawn(ItemDespawnEvent e) {
		if (stopDespawnList.contains(e.getEntity()))
			e.setCancelled(true);
	}
}
