package com.stupidbot.universalcorep.listeners;

import com.stupidbot.universalcorep.utils.Punishments;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class OnAsyncPlayerChat implements Listener {
	@EventHandler(priority = EventPriority.LOWEST)
	public void onAsyncPlayerChat(AsyncPlayerChatEvent e) throws ParseException {
		String mute = Punishments.getMute(e.getPlayer());
		if (mute != null) {
			e.setCancelled(true);
			String[] muteSplit = mute.split("#");
			if (muteSplit[0].contains("tempmute")) {
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				Date date1 = sdf.parse(muteSplit[4]);
				Date date2 = new Date();
				long millis = date1.getTime() - date2.getTime();
				e.getPlayer().sendMessage("\n�cYou are muted for �a"
						+ String.format("%d d, %d hr, %d min, %d sec", TimeUnit.MILLISECONDS.toDays(millis),
								TimeUnit.MILLISECONDS.toHours(millis)
										- TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)),
								TimeUnit.MILLISECONDS.toMinutes(millis)
										- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
								TimeUnit.MILLISECONDS.toSeconds(millis)
										- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))
						+ "\n�bReason: �e" + muteSplit[2] + "�7 (" + muteSplit[3] + ")");
			} else
				e.getPlayer().sendMessage(
						"\n�cYou are permanently muted\n�bReason: �e" + muteSplit[2] + "�7 (" + muteSplit[3] + ")");
			e.getPlayer().sendMessage("\n�2Unfairly muted? Appeal! �e�nhttps://copperuniverse.enjin.com/forums");
		}
	}
}
