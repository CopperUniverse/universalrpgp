package com.stupidbot.universalcorep.listeners;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.VisibilityManager;
import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.utils.Configs;
import com.stupidbot.universalcorep.utils.Stats;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.permissions.Permission;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class OnJoin implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onJoin(PlayerJoinEvent e) throws IOException, ParseException {

		Configs.setupPlayer(e.getPlayer());

		FileConfiguration config = Configs.loadPlayerFile(e.getPlayer(), "Player Data");
		config.set("Name.Name", e.getPlayer().getName());
		config.set("Name.Prefix", Main.chat.getPlayerPrefix(e.getPlayer()));
		config.set("Name.Suffix", Main.chat.getPlayerSuffix(e.getPlayer()));
		config.set("Punishments.IP", e.getPlayer().getAddress().toString().replaceFirst("/", ""));
		long millis = new Date().getTime()
				- new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(config.getString("Stats.LastPlayed")).getTime();

		Stats.setStamina(e.getPlayer(),
				(int) (config.getInt("Stats.Stamina") + (TimeUnit.MILLISECONDS.toHours(millis) * 12)));

		config.set("Stats.LastPlayed", new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));

		config.save(Configs.getPlayerFile(e.getPlayer(), "Player Data"));

		Stats.updateEXP(e.getPlayer());

		if (e.getPlayer().hasPermission(new Permission("universalmiscp.fly")))
			e.getPlayer().performCommand("fly");

		Hologram lootEggsHologram = HologramsAPI.createHologram(Main.plugin,
				new Location(Bukkit.getWorld("world"), 70, 104.4, -0.5));
		VisibilityManager visibilityManager = lootEggsHologram.getVisibilityManager();
		visibilityManager.setVisibleByDefault(false);
		visibilityManager.showTo(e.getPlayer());
		lootEggsHologram.appendTextLine("�4�lLoading your data");
		new BukkitRunnable() {
			boolean ignoreTimer = true;

			@Override
			public void run() {
				if (!(e.getPlayer().isOnline())) {
					cancel();
					lootEggsHologram.delete();
				} else if (Calendar.getInstance().get(Calendar.SECOND) == 0 || ignoreTimer) {
					ignoreTimer = false;
					FileConfiguration configL = Configs.loadPlayerFile(e.getPlayer(), "Player Data");
					lootEggsHologram.clearLines();
					lootEggsHologram.appendTextLine("�e�lLOOT EGG STATS");
					lootEggsHologram.appendTextLine("Loot Eggs Opened: �b"
							+ NumberFormat.getInstance(Locale.US).format(configL.getInt("LootEggs.Opened")));
					lootEggsHologram.appendTextLine("Common Prizes: �b"
							+ NumberFormat.getInstance(Locale.US).format(configL.getInt("LootEggs.Prizes.Commons")));
					lootEggsHologram.appendTextLine("Rare Prizes: �b"
							+ NumberFormat.getInstance(Locale.US).format(configL.getInt("LootEggs.Prizes.Rares")));
					lootEggsHologram.appendTextLine("Legendary Prizes: �b" + NumberFormat.getInstance(Locale.US)
							.format(configL.getInt("LootEggs.Prizes.Legendaries")));
					lootEggsHologram.appendTextLine("Duplicate Prizes: �b"
							+ NumberFormat.getInstance(Locale.US).format(configL.getInt("LootEggs.Prizes.Dupes")));
					lootEggsHologram.appendTextLine("Total Eggshells: �b"
							+ NumberFormat.getInstance(Locale.US).format(configL.getInt("LootEggs.TotalEggshells")));
					lootEggsHologram.appendTextLine("");
					lootEggsHologram.appendTextLine("");
					lootEggsHologram.appendTextLine("");
					lootEggsHologram.appendTextLine("�eLoot Eggs: �a"
							+ NumberFormat.getInstance(Locale.US).format(configL.getInt("LootEggs.Amount")));
				}
			}
		}.runTaskTimer(Main.plugin, 20 - (System.currentTimeMillis() % 20), 20);
		new BukkitRunnable() {
			boolean giveStamina = false;

			@Override
			public void run() {
				if (!(e.getPlayer().isOnline()))
					cancel();
				else {
					e.getPlayer().sendMessage("�8 �8 �1 �3 �3 �7 �8 ");
					if (giveStamina)
						Stats.setStamina(e.getPlayer(),
								Stats.staminaLevels.get(e.getPlayer().getUniqueId().toString()) + 2);
					else
						giveStamina = true;
				}
			}
		}.runTaskTimerAsynchronously(Main.plugin, 400, 12000);
	}
}
