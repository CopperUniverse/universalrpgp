package com.stupidbot.universalcorep.listeners;

import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.utils.Inventories;
import com.stupidbot.universalcorep.utils.LootChests;
import com.stupidbot.universalcorep.utils.Stats;
import com.stupidbot.universalcorep.utils.Text;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permission;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Objects;

public class OnPlayerInteract implements Listener {

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_AIR) {
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK)
				if (e.getClickedBlock().getType() == Material.ENDER_CHEST) {
					Inventories.generateGUI(e.getPlayer(), "pvs0");
					e.setCancelled(true);
				} else if (e.getClickedBlock().getType() == Material.DRAGON_EGG
						&& OnBlockBreak.blockReset.containsKey(e.getClickedBlock().getLocation())) {
					e.setCancelled(true);
					String[] info = OnBlockBreak.blockReset.get(e.getClickedBlock().getLocation()).split("\\:");
					if (!(e.getPlayer().getUniqueId().toString().contains(info[2]))) {
						OnBlockBreak.blockReset.remove(e.getClickedBlock().getLocation());
						e.getClickedBlock().setType(Material.AIR);
						Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(),
								"givevoucher " + e.getPlayer().getName() + " " + info[3] + " 1");
						Bukkit.broadcastMessage(
								StringUtils.substring(Main.chat.getPlayerPrefix(e.getPlayer()).replaceAll("&", "§"), 0,
										2) + e.getPlayer().getName() + " §ejust obtained a rank voucher!");
					} else
						e.getPlayer().sendMessage("§cYou can't mine your own rank mission!");
				} else if (e.getPlayer().getItemInHand() != null)
					if (e.getPlayer().getItemInHand() != null)
						if (e.getPlayer().getItemInHand().hasItemMeta())
							if (e.getPlayer().getItemInHand().getItemMeta().getDisplayName().contains("§eLoot Chest")) {
								e.setCancelled(true);
								if (e.getClickedBlock().getLocation().add(0, 1, 0).getBlock()
										.getType() == Material.AIR) {
									if (e.getPlayer().getItemInHand().getItemMeta().getDisplayName()
											.contains("§aCOMMON"))
										LootChests.openLootChest("common",
												e.getClickedBlock().getLocation().add(0, -1, 0), e.getPlayer());
									else if (e.getPlayer().getItemInHand().getItemMeta().getDisplayName()
											.contains("§5RARE"))
										LootChests.openLootChest("rare",
												e.getClickedBlock().getLocation().add(0, -1, 0), e.getPlayer());
									else if (e.getPlayer().getItemInHand().getItemMeta().getDisplayName()
											.contains("§6LEGENDARY"))
										LootChests.openLootChest("legendary",
												e.getClickedBlock().getLocation().add(0, -1, 0), e.getPlayer());
									else
										LootChests.openLootChest("godly",
												e.getClickedBlock().getLocation().add(0, -1, 0), e.getPlayer());
									if (e.getPlayer().getItemInHand().getAmount() == 1)
										e.getPlayer().setItemInHand(new ItemStack(Material.AIR));
									else
										e.getPlayer().getItemInHand()
												.setAmount(e.getPlayer().getItemInHand().getAmount() - 1);
								} else
									e.getPlayer().sendMessage("§cThere's not enough space above the chest.");
							}
			if (e.getPlayer().getItemInHand() != null)
				if ((e.getPlayer().getItemInHand().hasItemMeta()
						&& e.getPlayer().getItemInHand().getItemMeta().hasDisplayName()))
					if (e.getPlayer().getItemInHand().getType() == Material.PAPER && (e.getPlayer().getItemInHand()
							.getItemMeta().getDisplayName().contains("§aPLANET")
							|| e.getPlayer().getItemInHand().getItemMeta().getDisplayName().contains("§bGALAXY")
							|| e.getPlayer().getItemInHand().getItemMeta().getDisplayName().contains("§5UNIVERSE"))) {
						String rank = e.getPlayer().getItemInHand().getItemMeta().getDisplayName().replace(" §cVoucher",
								"");
						if (!(e.getPlayer().hasPermission(
								new Permission("universalrpgp.p2w." + ChatColor.stripColor(rank).toLowerCase())))) {
							Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(),
									"pex user " + e.getPlayer().getName() + " group set " + ChatColor.stripColor(rank));
							Bukkit.broadcastMessage(Text
									.sendCenteredMessage(Main.chat.getPlayerPrefix(e.getPlayer()).replaceAll("&", "§")
											+ e.getPlayer().getName(), 69));
							Bukkit.broadcastMessage(Text.sendCenteredMessage("§ejust redeemed a rank!", 69));
							if (e.getPlayer().getItemInHand().getAmount() == 1)
								e.getPlayer().setItemInHand(new ItemStack(Material.AIR));
							else
								e.getPlayer().getItemInHand().setAmount(e.getPlayer().getItemInHand().getAmount() - 1);
						} else
							e.getPlayer().sendMessage("§cYou already have a higher rank");
					} else if (e.getPlayer().getItemInHand().getItemMeta().getDisplayName()
							.contains("§cServer Booster")) {
						e.setCancelled(true);
						String booster = "Experience";
						if (e.getPlayer().getItemInHand().getItemMeta().getDisplayName().contains("Sell Prices"))
							booster = "Sell Prices";
						if ((Objects.equals(booster, "Experience") && Stats.globalXPMultiplier.split("\\:")[0].contains("server"))
								|| (Objects.equals(booster, "Sell Prices")
										&& Inventories.globalShopMultiplier.split("\\:")[0].contains("server"))) {
							String name = StringUtils.substring(Main.chat.getPlayerPrefix(e.getPlayer()), 0, 2)
									.replaceAll("&", "§") + e.getPlayer().getName();
							String uuid = e.getPlayer().getName();
							if (Objects.equals(booster, "Experience")) {
								Stats.globalXPMultiplier = e.getPlayer().getUniqueId() + ":" + (Integer
										.parseInt(e.getPlayer().getItemInHand().getItemMeta().getLore().get(4)
												.replace("§7Multiplier: §ex", ""))
										* Main.plugin.getConfig().getInt("Boosters.DefaultMultiplier.Experience"));
								int countdownDone = Integer.parseInt(e.getPlayer().getItemInHand().getItemMeta()
										.getLore().get(3).replace("§7Time: §9", "").replace(" min", ""));
								new BukkitRunnable() {
									int countdown = 0;

									@Override
									public void run() {
										if (countdown == countdownDone) {
											cancel();
											Stats.globalXPMultiplier = "server:" + Main.plugin.getConfig()
													.getInt("Boosters.DefaultMultiplier.Experience");
											Bukkit.broadcastMessage("§aExperience §emultiplier has returned to §6x"
													+ Main.plugin.getConfig()
															.getInt("Boosters.DefaultMultiplier.Experience"));
										} else if (countdown % 5 == 0)
											for (Player all : Bukkit.getOnlinePlayers())
												Text.sendJSONMessage(all,
														"[\"\",{\"text\":\"✪\",\"color\":\"light_purple\"},{\"text\":\" "
																+ name
																+ " \"},{\"text\":\"has an active \",\"color\":\"yellow\"},{\"text\":\""
																+ (Integer.parseInt(
																		Stats.globalXPMultiplier.split("\\:")[1])
																		/ Main.plugin.getConfig()
																				.getInt("Boosters.DefaultMultiplier.Experience"))
																+ "X EXPERIENCE \",\"color\":\"green\",\"bold\":\"true\"},{\"text\":\"booster! Tip them for free EXP by \",\"color\":\"yellow\"},{\"text\":\"» \",\"color\":\"gold\"},{\"text\":\"CLICKING HERE\",\"color\":\"aqua\",\"bold\":\"true\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/tip "
																+ uuid
																+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click to tip this player, you'll both get EXP!\",\"color\":\"gold\"}]}}},{\"text\":\" «\",\"color\":\"gold\"}]");
										countdown++;
									}
								}.runTaskTimerAsynchronously(Main.plugin, 0, 1200);
							} else {
								Inventories.globalShopMultiplier = e.getPlayer().getUniqueId() + ":" + (Integer
										.parseInt(e.getPlayer().getItemInHand().getItemMeta().getLore().get(4)
												.replace("§7Multiplier: §ex", ""))
										* Main.plugin.getConfig().getInt("Boosters.DefaultMultiplier.SellPrices"));
								int countdownDone = Integer.parseInt(e.getPlayer().getItemInHand().getItemMeta()
										.getLore().get(3).replace("§7Time: §9", "").replace(" min", ""));
								new BukkitRunnable() {
									int countdown = 0;

									@Override
									public void run() {
										if (countdown == countdownDone) {
											cancel();
											Inventories.globalShopMultiplier = "server:" + Main.plugin.getConfig()
													.getInt("Boosters.DefaultMultiplier.SellPrices");
											Bukkit.broadcastMessage("§aSell Prices §emultiplier has returned to §6x"
													+ Main.plugin.getConfig()
															.getInt("Boosters.DefaultMultiplier.SellPrices"));
										} else if (countdown % 5 == 0)
											for (Player all : Bukkit.getOnlinePlayers())
												Text.sendJSONMessage(all,
														"[\"\",{\"text\":\"✪\",\"color\":\"light_purple\"},{\"text\":\" "
																+ name
																+ " \"},{\"text\":\"has an active \",\"color\":\"yellow\"},{\"text\":\""
																+ (Integer
																		.parseInt(Inventories.globalShopMultiplier
																				.split("\\:")[1])
																		/ Main.plugin.getConfig()
																				.getInt("Boosters.DefaultMultiplier.SellPrices"))
																+ "X SELL PRICES \",\"color\":\"green\",\"bold\":\"true\"},{\"text\":\"booster! Tip them for free EXP by \",\"color\":\"yellow\"},{\"text\":\"» \",\"color\":\"gold\"},{\"text\":\"CLICKING HERE\",\"color\":\"aqua\",\"bold\":\"true\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/tip "
																+ uuid
																+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click to tip this player, you'll both get EXP!\",\"color\":\"gold\"}]}}},{\"text\":\" «\",\"color\":\"gold\"}]");
										countdown++;
									}
								}.runTaskTimerAsynchronously(Main.plugin, 0, 1200);
							}
							if (e.getPlayer().getItemInHand().getAmount() == 1)
								e.getPlayer().setItemInHand(new ItemStack(Material.AIR));
							else
								e.getPlayer().getItemInHand().setAmount(e.getPlayer().getItemInHand().getAmount() - 1);
						} else
							e.getPlayer().sendMessage("§cA booster of this type is already active!");
					}
		}
	}
}