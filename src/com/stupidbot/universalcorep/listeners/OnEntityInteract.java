package com.stupidbot.universalcorep.listeners;

import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

import java.util.ArrayList;
import java.util.List;

public class OnEntityInteract implements Listener {
	public static final List<Entity> stopInteractList = new ArrayList<>();

	@EventHandler
	public void onEntityInteract(PlayerInteractAtEntityEvent e) {
		if (stopInteractList.contains(e.getRightClicked()))
			e.setCancelled(true);
	}
}
