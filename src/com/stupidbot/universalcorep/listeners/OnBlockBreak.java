package com.stupidbot.universalcorep.listeners;

import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.mutations.Mutations;
import com.stupidbot.universalcorep.utils.*;
import org.apache.commons.lang3.text.WordUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class OnBlockBreak implements Listener {
	public static final Map<Location, String> blockReset = new HashMap<>();
	private static final Map<Material, String> blockInfo = new HashMap<>();

	static {
		// XP:Min Reset Time:Max Reset Time:Can Random Material?:If So What
		// Material? Else What Always Go To?:Also If So What Data? Else What
		// Always Go To?:Multi Mine?:If So How Many Times?:Item To Give:Data Of
		// Item To Give:Base Stamina
		blockInfo.put(Material.RED_SANDSTONE, "1:1:6:true:sandstone:0:false:0:red_sandstone:0:1");
		blockInfo.put(Material.SANDSTONE, "6:0:0:false:red_sandstone:0:false:0:sandstone:0:3");
		blockInfo.put(Material.COAL_ORE, "12:2:8:true:coal_block:0:false:0:coal_ore:0:15");
		blockInfo.put(Material.COAL_BLOCK, "27:0:0:false:coal_ore:0:false:0:coal:0:20");
		blockInfo.put(Material.IRON_ORE, "30:8:32:true:iron_block:0:false:0:iron_ore:0:45");
		blockInfo.put(Material.IRON_BLOCK, "40:0:0:false:iron_ore:0:false:0:iron_ingot:0:50");
		blockInfo.put(Material.GOLD_ORE, "90:16:64:true:gold_block:0:false:0:gold_ore:0:85");
		blockInfo.put(Material.GOLD_BLOCK, "100:0:0:false:gold_ore:0:false:0:gold_ingot:0:90");
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockBreak(BlockBreakEvent e) throws IOException {
		Block block = e.getBlock();
		if (MiscellaneousUtils.locIsInRegion(block.getLocation(), "droria_632")) {
			if (e.getPlayer().getGameMode() != GameMode.CREATIVE) {
				if (block.getType() == Material.DRAGON_EGG) {
				} else if (blockInfo.containsKey(block.getType())) {
					Player player = e.getPlayer();
					String[] info = blockInfo.get(block.getType()).split("\\:");
					int staminaUse = Integer.parseInt(info[10]);
					File config = Configs.getPlayerFile(player.getUniqueId(), "Player Data");
					FileConfiguration configLoaded = Configs.loadFile(config);
					int xp = Integer.parseInt(info[0]);
					if (player.getItemInHand() != null)
						if (Stats.miningItems.contains(player.getItemInHand().getType())) {
							staminaUse += (Integer
									.parseInt(player.getItemInHand().getItemMeta().getDisplayName().split(" �f�l")[1]
											.replaceAll(",", ""))
									- 1) / 4;
							switch (player.getItemInHand().getType()) {
							case WOOD_PICKAXE:
								staminaUse += 5;
								break;
							case STONE_PICKAXE:
								if (configLoaded.getInt("Level.Level") < 25) {
									Text.showTitle(player, "{\"text\":\"\"}", 5, 20, 5);
									Text.showSubtitle(player,
											"[{\"text\":\"REQUIRES LEVEL \",\"color\":\"yellow\",\"bold\":\"true\"}, {\"text\":\" 25\",\"color\":\"white\",\"bold\":\"true\"}]",
											5, 20, 5);
									e.setCancelled(true);
								} else
									staminaUse += 20;
								break;
							case IRON_PICKAXE:
								if (configLoaded.getInt("Level.Level") < 75) {
									Text.showTitle(player, "{\"text\":\"\"}", 5, 20, 5);
									Text.showSubtitle(player,
											"[{\"text\":\"REQUIRES LEVEL \",\"color\":\"yellow\",\"bold\":\"true\"}, {\"text\":\" 75\",\"color\":\"white\",\"bold\":\"true\"}]",
											5, 20, 5);
									e.setCancelled(true);
								} else
									staminaUse += 50;
								break;
							case DIAMOND_PICKAXE:
								if (configLoaded.getInt("Level.Level") < 150) {
									Text.showTitle(player, "{\"text\":\"\"}", 5, 20, 5);
									Text.showSubtitle(player,
											"[{\"text\":\"REQUIRES LEVEL \",\"color\":\"yellow\",\"bold\":\"true\"}, {\"text\":\" 150\",\"color\":\"white\",\"bold\":\"true\"}]",
											5, 20, 5);
									e.setCancelled(true);
								} else
									staminaUse += 100;
								break;
							case GOLD_PICKAXE:
								if (configLoaded.getInt("Level.Level") < 250) {
									Text.showTitle(player, "{\"text\":\"\"}", 5, 20, 5);
									Text.showSubtitle(player,
											"[{\"text\":\"REQUIRES LEVEL \",\"color\":\"yellow\",\"bold\":\"true\"}, {\"text\":\" 250\",\"color\":\"white\",\"bold\":\"true\"}]",
											5, 20, 5);
									e.setCancelled(true);
								} else
									staminaUse += 160;
								break;
							default:
								break;
							}
							if (!(e.isCancelled())) {
								if (player.getItemInHand().containsEnchantment(Mutations.EXPERIENCED))
									xp += (xp
											/ (11 - player.getItemInHand().getEnchantmentLevel(Mutations.EXPERIENCED)))
											+ (Integer.parseInt(player.getItemInHand().getItemMeta().getLore().get(5)
													.replace("�6Luck: �a", "").replaceAll(",", "")) / 10);

								if (player.getItemInHand().containsEnchantment(Mutations.HEAVY))
									staminaUse += staminaUse
											* (player.getItemInHand().getEnchantmentLevel(Mutations.HEAVY) / 10);

								if (player.getItemInHand().containsEnchantment(Mutations.ENERGIZER))
									if (ThreadLocalRandom.current().nextInt(0, 101) <= 3
											+ player.getItemInHand().getItemMeta().getEnchantLevel(Mutations.ENERGIZER)
											+ (Integer.parseInt(player.getItemInHand().getItemMeta().getLore().get(5)
													.replace("�6Luck: �a", "").replaceAll(",", "")) / 150))
										player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING,
												60 + (player.getItemInHand().getEnchantmentLevel(Mutations.ENERGIZER)
														* 10),
												(player.getItemInHand().getEnchantmentLevel(Mutations.ENERGIZER) / 5)),
												true);
							}
						}
					if (Stats.staminaLevels.getOrDefault(player.getUniqueId().toString(), 0) >= staminaUse
							&& !(e.isCancelled())) {
						e.setCancelled(true);
						Material typeB4 = block.getType();
						byte dataB4 = block.getData();

						configLoaded.set("Stats.BlocksMined", configLoaded.getInt("Stats.BlocksMined") + 1);
						configLoaded.save(config);
						if (info[3].contains("true")) {
							blockReset.put(block.getLocation(), typeB4 + ":" + dataB4);
							block.setType(Material.BARRIER);

							new BukkitRunnable() {
								final int random2 = ThreadLocalRandom.current().nextInt(0, 101);

								@Override
								public void run() {
									if (random2 <= 3 + (player.getItemInHand().getEnchantments() != null && player
											.getItemInHand().getEnchantments().containsKey(Mutations.ENRICHER)
													? player
															.getItemInHand().getItemMeta()
															.getEnchantLevel(Mutations.ENRICHER)
															+ (Integer.parseInt(
																	player.getItemInHand().getItemMeta().getLore()
																			.get(5).replace("�6Luck: �a", "")
																			.replaceAll(",", ""))
																	/ 100)
													: 0)) {
										block.getChunk().load(true);
										block.setType(Material.getMaterial(info[4].toUpperCase()));
										block.setData(Byte.parseByte(info[5]));
										block.getWorld().playSound(
												block.getLocation().add(block.getLocation().getX() > 0 ? 0.5 : -0.5,
														0.5, (block.getLocation().getZ() > 0 ? 0.5 : -0.5)),
												Sound.DRINK, 0.5f, 0.5f);
										ParticleEffect.SPELL_WITCH.display(0.5f, 0.5f, 0.5f, 0.1f, 50,
												block.getLocation().add(block.getLocation().getX() > 0 ? 0.5 : -0.5,
														0.5, (block.getLocation().getZ() > 0 ? 0.5 : -0.5)),
												256D);
									} else {
										block.getChunk().load(true);
										block.setType(typeB4);
										block.setData(dataB4);
										Bukkit.getWorld("world").playEffect(block.getLocation(),
												Effect.MOBSPAWNER_FLAMES, 1);
										block.getWorld().playSound(
												block.getLocation().add(block.getLocation().getX() > 0 ? 0.5 : -0.5,
														0.5, (block.getLocation().getZ() > 0 ? 0.5 : -0.5)),
												Sound.LAVA_POP, 0.5f, 1.5f);
									}
									blockReset.remove(block.getLocation());
								}
							}.runTaskLater(Main.plugin, ThreadLocalRandom.current().nextInt(Integer.parseInt(info[1]),
									Integer.parseInt(info[2]) + 1) * 20);
						} else {
							int random2 = ThreadLocalRandom.current().nextInt(0, 13 + 1);
							if (random2 == 1) {
								block.setType(Material.getMaterial(info[4].toUpperCase()));
								block.setData(Byte.parseByte(info[5]));
								block.getWorld().playSound(
										block.getLocation().add(block.getLocation().getX() > 0 ? 0.5 : -0.5, 0.5,
												(block.getLocation().getZ() > 0 ? 0.5 : -0.5)),
										Sound.LAVA_POP, 0.5f, 1.5f);
								Bukkit.getWorld("world").playEffect(block.getLocation(), Effect.MOBSPAWNER_FLAMES, 1);
							} else {
								ParticleEffect.CRIT_MAGIC.display(0.5f, 0.5f, 0.5f, 0.1f, 50,
										block.getLocation().add(block.getLocation().getX() > 0 ? 0.5 : -0.5, 0.5,
												(block.getLocation().getZ() > 0 ? 0.5 : -0.5)),
										256D);
								block.getWorld().playSound(
										block.getLocation().add(block.getLocation().getX() > 0 ? 0.5 : -0.5, 0.5,
												(block.getLocation().getZ() > 0 ? 0.5 : -0.5)),
										Sound.LEVEL_UP, 0.5f, 2);
							}
						}
						ItemStack giveItem = new ItemStack(Material.getMaterial(info[8].toUpperCase()),
								ThreadLocalRandom.current()
										.nextInt(1,
												4 + (player.getItemInHand().getEnchantments()
														.containsKey(Mutations.DUPLICATION)
																? player.getItemInHand().getItemMeta()
																		.getEnchantLevel(
																				Mutations.DUPLICATION)
																		+ Integer
																				.parseInt(player.getItemInHand()
																						.getItemMeta().getLore().get(5)
																						.replace("�6Luck: �a", "")
																						.replaceAll(",", ""))
																				/ 100
																: 0)));
						Text.showActionBar(player,
								"{\"text\":\"�bEXP: �a+"
										+ NumberFormat.getNumberInstance(Locale.US)
												.format((int) (xp
														* Double.parseDouble(Stats.globalXPMultiplier.split("\\:")[1])))
										+ " �6Stamina: �c-"
										+ NumberFormat.getNumberInstance(Locale.US).format(staminaUse) + " �d"
										+ WordUtils.capitalizeFully(giveItem.getType().toString().replaceAll("_", " "))
										+ ": �a+"
										+ NumberFormat.getNumberInstance(Locale.US).format(giveItem.getAmount())
										+ (player.getInventory().firstEmpty() != -1 ? "" : " �c[INVENTORY FULL]")
										+ "\"}");
						if (player.getInventory().firstEmpty() != -1)
							player.getInventory().addItem(giveItem);
						else
							player.getWorld().dropItemNaturally(player.getLocation(), giveItem);
						Stats.setStamina(player, Stats.staminaLevels.get(player.getUniqueId().toString()) - staminaUse);
						Stats.giveEXP(player, xp, true);
						if (player.getItemInHand().containsEnchantment(Mutations.SLIPPERY))
							if (ThreadLocalRandom.current().nextInt(1, 101) <= player.getItemInHand().getItemMeta()
									.getEnchantLevel(Mutations.SLIPPERY)) {
								ItemStack item = player.getItemInHand();
								player.playSound(player.getLocation(), Sound.BAT_TAKEOFF, 2, 2);
								player.sendMessage("�4" + Mutations.SLIPPERY.getName() + " "
										+ Text.toRoman(player.getItemInHand().getItemMeta()
												.getEnchantLevel(Mutations.SLIPPERY))
										+ " �emade you drop your "
										+ player.getItemInHand().getItemMeta().getDisplayName().split(" �f�l")[0]);
								player.setItemInHand(null);
								player.getWorld().dropItemNaturally(player.getEyeLocation(), item)
										.setVelocity(new Vector(player.getLocation().getDirection().getX(),
												player.getLocation().getDirection().getY(),
												player.getLocation().getDirection().getZ()).normalize().multiply(0.4));
							}
					} else if (!(e.isCancelled())) {
						e.setCancelled(true);
						Text.showTitle(player, "{\"text\":\"\"}", 5, 20, 5);
						Text.showSubtitle(player,
								"{\"text\":\"NOT ENOUGH STAMINA\",\"color\":\"red\",\"bold\":\"true\"}", 5, 20, 5);
					}
				} else {
					e.setCancelled(true);
					e.getPlayer().sendMessage("�5�lDENIED�d You can't do that action here!");
				}
			}
		}
	}
}
