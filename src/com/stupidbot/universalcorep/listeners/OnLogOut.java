package com.stupidbot.universalcorep.listeners;

import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.utils.Configs;
import com.stupidbot.universalcorep.utils.Ships;
import com.stupidbot.universalcorep.utils.Stats;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffectType;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OnLogOut implements Listener {
	@EventHandler(priority = EventPriority.LOWEST)
	public void onLogOut(PlayerQuitEvent e) throws IOException {
		Player player = e.getPlayer();
		Main.chat.setPlayerPrefix(player, null);
		player.setGameMode(GameMode.SURVIVAL);
		player.teleport(new Location(Bukkit.getServer().getWorld("world"), -10.5D, 99.5D, -15.5D, 0.0F, 0.0F));
		if (Ships.shipWait.containsKey(player))
			Ships.shipWait.remove(player.getUniqueId().toString());
		player.setAllowFlight(false);
		player.removePotionEffect(PotionEffectType.JUMP);
		player.setWalkSpeed(0.2f);
		FileConfiguration config = Configs.loadPlayerFile(e.getPlayer(), "Player Data");
		config.set("Punishments.IP", player.getAddress().toString());
		config.set("Stats.LastPlayed", new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
		config.set("Stats.Stamina", Stats.staminaLevels.get(player.getUniqueId().toString()) != null
				? Stats.staminaLevels.get(player.getUniqueId().toString()) : 0);
		config.save(Configs.getPlayerFile(player, "Player Data"));

		Stats.staminaLevels.remove(player.getUniqueId().toString());
	}
}
