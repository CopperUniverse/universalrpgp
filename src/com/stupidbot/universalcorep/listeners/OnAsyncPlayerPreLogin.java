package com.stupidbot.universalcorep.listeners;

import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.utils.Punishments;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerPreLoginEvent.Result;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("deprecation")
public class OnAsyncPlayerPreLogin implements Listener {
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent e) throws ParseException {
		if (e.getResult() == Result.KICK_FULL
				&& Main.perms.playerHas("world", Bukkit.getOfflinePlayer(e.getUniqueId()), "vipslot.allow"))
			e.allow();
		else if (e.getUniqueId().toString().contains("670cf4dd-237b-4010-aee5-8fcc9fde3b34")
				|| e.getUniqueId().toString().contains("c894ac1c-9a68-4c37-b5bf-11ecd519a7fb")
				|| e.getUniqueId().toString().contains("062e38c5-bd46-460f-8412-ae2ee72c1bd1")
				|| e.getUniqueId().toString().contains("c5361495-d57f-47a2-aaed-ac2aad791356"))
			e.allow();
		else {
			String ban = Punishments.getBan(Bukkit.getOfflinePlayer(e.getUniqueId()));
			if (ban != null) {
				String[] banSplit = ban.split("#");
				if (banSplit[0].contains("tempban")) {
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
					Date date1 = sdf.parse(banSplit[4]);
					Date date2 = new Date();
					long millis = date1.getTime() - date2.getTime();
					e.disallow(Result.KICK_BANNED, "�cYou have been banned from the server\n\n�bReason: �e"
							+ banSplit[2] + "\n�7(" + banSplit[3] + " to " + banSplit[4] + ")\n\n\n�dUnban In: �a"
							+ String.format("%d d, %d hr, %d min, %d sec", TimeUnit.MILLISECONDS.toDays(millis),
									TimeUnit.MILLISECONDS.toHours(millis)
											- TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)),
									TimeUnit.MILLISECONDS.toMinutes(millis)
											- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
									TimeUnit.MILLISECONDS.toSeconds(millis)
											- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))
							+ "\n\n\n�2Unfairly banned? Appeal! �e�nhttps://copperuniverse.enjin.com/forums");
				} else
					e.disallow(Result.KICK_BANNED, "�cYou have been banned from the server\n\n�bReason: �e"
							+ banSplit[2] + "\n�7(" + banSplit[3]
							+ ")\n\n\n�dUnban In:�a Never\n\n\n�2Unfairly banned? Appeal! �e�nhttps://copperuniverse.enjin.com/forums");
			}
		}
	}
}