package com.stupidbot.universalcorep.listeners;

import com.stupidbot.universalcorep.utils.Inventories;
import com.stupidbot.universalcorep.utils.LootEggs;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;

public class OnSneak implements Listener {

	@EventHandler
	public void onSneak(PlayerToggleSneakEvent e) {
		if (LootEggs.hatchEggTimer.containsKey(e.getPlayer().getUniqueId().toString()))
			e.getPlayer().openInventory(Inventories.cosmetics0);
	}
}
