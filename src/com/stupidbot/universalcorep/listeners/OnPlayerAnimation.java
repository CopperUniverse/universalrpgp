package com.stupidbot.universalcorep.listeners;

import com.stupidbot.universalcorep.utils.LootEggs;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerAnimationType;


public class OnPlayerAnimation implements Listener {
	
	@EventHandler
	public void onPlayerAnimation(PlayerAnimationEvent e) {
		if (e.getAnimationType() == PlayerAnimationType.ARM_SWING
				&& LootEggs.hatchEggTimer.getOrDefault(e.getPlayer().getUniqueId().toString(), 0) == 200)
			LootEggs.hatchEggTimer.put(e.getPlayer().getUniqueId().toString(), 201);
	}
}
