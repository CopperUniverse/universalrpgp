package com.stupidbot.universalcorep.listeners;

import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.utils.*;
import net.md_5.bungee.api.ChatColor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class OnInventoryClick implements Listener {

	@SuppressWarnings({ "deprecation", "unchecked" })
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) throws IOException, ParseException {
		Player player = (Player) e.getWhoClicked();
		ItemStack clicked = e.getCurrentItem();
		Inventory inventory = e.getInventory();
		ItemStack holding = e.getCursor();
		if (clicked != null)
			if (e.getRawSlot() < e.getInventory().getSize()) {
				if (Objects.equals(inventory.getName(), "�4Player Vaults")) {
					if (clicked.getType() == Material.ENDER_CHEST) {
						FileConfiguration configLoaded = Configs.loadPlayerFile(player, "Player Vaults");
						Inventory pv = Bukkit.createInventory(null, 54, "�4Player Vault �9#" + clicked.getAmount());
						List<ItemStack> itemList = (List<ItemStack>) configLoaded
								.get("Vault." + clicked.getAmount() + ".Items");
						ItemStack[] itemSet = itemList.toArray(new ItemStack[0]);
						pv.setContents(itemSet);
						player.openInventory(pv);
					}
					e.setCancelled(true);
				} else if (Objects.equals(inventory.getName(), "�dCosmetics")) {
					if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�6Kill Messages"))
						Inventories.generateCosmeticGUI("Kill Message", player, 1);
					else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�6Chat Tags"))
						Inventories.generateCosmeticGUI("Chat Tag", player, 1);
					else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�6Projectile Trails"))
						Inventories.generateCosmeticGUI("Projectile Trail", player, 1);
					else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�6Ship Skins"))
						Inventories.generateCosmeticGUI("Ship Skin", player, 1);
					else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�2How To Get Cosmetics")) {
						player.playSound(player.getLocation(), Sound.LEVEL_UP, 2F, 1F);
						Text.openBook(LootEggs.howToObtainLootEggs, player);
					}
					e.setCancelled(true);
				} else if (Objects.equals(inventory.getName(), "�5Rewards")) {
					if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�bReferrals")) {
						Text.openBook(
								Text.newBook("Referral Info", "StupidBot",
										"[\"\",{\"text\":\"      \"},{\"text\":\"REFERRALS\",\"color\":\"aqua\",\"bold\":\"true\"},{\"text\":\"\n\n\nGive friends (if you have any) your referral code, you'll BOTH get LOOT EGGS!\n\n\",\"color\":\"black\"},{\"text\":\"YOUR REFERRAL CODE: \",\"color\":\"dark_red\"},{\"text\":\"\n"
												+ Configs.loadPlayerFile(player, "Player Data")
														.getString("Codes.Referral.Code")
												+ "\n\",\"color\":\"gold\"},{\"text\":\"PEOPLE REFERRED:\",\"color\":\"dark_red\"},{\"text\":\"\n"
												+ NumberFormat.getNumberInstance(Locale.US)
														.format(Configs.loadPlayerFile(player, "Player Data")
																.getInt("Codes.Referral.Referrals"))
												+ "\n\n\",\"color\":\"gold\"},{\"text\":\"     \"},{\"text\":\"ENTER CODE\",\"color\":\"dark_purple\",\"bold\":\"true\",\"underlined\":\"true\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/redeemreferral\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"/redeemreferral\n\",\"color\":\"yellow\"},{\"text\":\"WARNING: \",\"color\":\"dark_red\",\"bold\":\"true\"},{\"text\":\"Code is case sensitive, you can only use a referral code once!\",\"color\":\"white\",\"bold\":\"false\"}]}}}]"),
								player);
					} else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�bVote"))
						player.performCommand("vote");
					e.setCancelled(true);
				} else if (inventory.getName().contains("�lPunish �� �4")) {
					if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�2Warn Player")) {
						String punishName = player.getOpenInventory().getTitle().replace("�lPunish �� �4", "");
						if (Bukkit.getPlayer(punishName) != null) {
							File config = Configs.getPlayerFile(UUID.fromString(player.getOpenInventory().getItem(4)
									.getItemMeta().getLore().get(2).replace("�cUUID: �f", "")), "Player Data");
							FileConfiguration configLoaded = Configs.loadFile(config);
							String reason = player.getOpenInventory().getItem(4).getItemMeta().getLore().get(0)
									.replace("�cPunish Reason: �f", "");
							List<String> list = new ArrayList<>();
							String date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
							list.add("warn#" + player.getUniqueId() + "#" + reason + "#" + date);
							if (configLoaded.getStringList("Punishments.History") != null)
								list.addAll(configLoaded.getStringList("Punishments.History"));
							configLoaded.set("Punishments.History", list);
							configLoaded.save(config);
							Bukkit.broadcast("�b[STAFF]�f: "
									+ StringUtils.substring(Main.chat.getPlayerPrefix(player), 0, 2).replaceAll("&",
											"�")
									+ player.getName() + " �fwarned "
									+ configLoaded.getString("Name.Prefix").replaceAll("&", "�") + punishName
									+ " �ffor �e" + reason + " �7(" + date + ")", "punishgui.use");
							Bukkit.getPlayer(punishName).sendMessage(
									"\n�cYou have been warned\n�bReason: �e" + reason + " �7(" + date + ")");
						} else
							player.sendMessage("�cYou can't warn an offline player!");
						player.closeInventory();
					} else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�2Kick Player")) {
						String punishName = player.getOpenInventory().getTitle().replace("�lPunish �� �4", "");
						if (Bukkit.getPlayer(punishName) != null) {
							File config = Configs.getPlayerFile(UUID.fromString(player.getOpenInventory().getItem(4)
									.getItemMeta().getLore().get(2).replace("�cUUID: �f", "")), "Player Data");
							FileConfiguration configLoaded = Configs.loadFile(config);
							String reason = player.getOpenInventory().getItem(4).getItemMeta().getLore().get(0)
									.replace("�cPunish Reason: �f", "");
							List<String> list = new ArrayList<>();
							String date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
							list.add("kick#" + player.getUniqueId() + "#" + reason + "#" + date);
							if (configLoaded.getStringList("Punishments.History") != null)
								list.addAll(configLoaded.getStringList("Punishments.History"));
							configLoaded.set("Punishments.History", list);
							configLoaded.save(config);
							Bukkit.broadcast("�b[STAFF]�f: "
									+ StringUtils.substring(Main.chat.getPlayerPrefix(player), 0, 2).replaceAll("&",
											"�")
									+ player.getName() + " �fkicked "
									+ configLoaded.getString("Name.Prefix").replaceAll("&", "�") + punishName
									+ " �ffor �e" + reason + " �7(" + date + ")", "punishgui.use");
							Bukkit.getPlayer(punishName)
									.kickPlayer("�cYou have been kicked from the server\n\n�bReason: �e" + reason
											+ "\n�7(" + date + ")");
						} else
							player.sendMessage("�cYou can't kick an offline player!");

						player.closeInventory();
					} else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�2Mute Player �e1 Hour")) {
						String punishName = player.getOpenInventory().getTitle().replace("�lPunish �� �4", "");
						File config = Configs.getPlayerFile(UUID.fromString(player.getOpenInventory().getItem(4)
								.getItemMeta().getLore().get(2).replace("�cUUID: �f", "")), "Player Data");
						FileConfiguration configLoaded = Configs.loadFile(config);
						String reason = player.getOpenInventory().getItem(4).getItemMeta().getLore().get(0)
								.replace("�cPunish Reason: �f", "");
						List<String> list = new ArrayList<>();
						String date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
						Calendar c = Calendar.getInstance();
						c.setTime(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(date));
						c.add(Calendar.HOUR, 1);
						String dateFin = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(c.getTime());
						list.add("tempmute#" + player.getUniqueId() + "#" + reason + "#" + date + "#" + dateFin);
						if (configLoaded.getStringList("Punishments.History") != null)
							list.addAll(configLoaded.getStringList("Punishments.History"));
						configLoaded.set("Punishments.History", list);
						configLoaded.save(config);
						Bukkit.broadcast(
								"�b[STAFF]�f: "
										+ StringUtils.substring(Main.chat.getPlayerPrefix(player), 0, 2).replaceAll("&",
												"�")
										+ player.getName() + " �fmuted "
										+ configLoaded.getString("Name.Prefix").replaceAll("&", "�") + punishName
										+ " �ffor �e" + reason + " �7(" + date + " to " + dateFin + ")",
								"punishgui.use");
						if (Bukkit.getPlayer(punishName) != null)
							Bukkit.getPlayer(punishName).sendMessage("\n�cYou have been muted for 1 hour\n�bReason: �e"
									+ reason + " �7(" + date + " to " + dateFin + ")");

						player.closeInventory();
					} else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�2Mute Player �e1 Day")) {
						String punishName = player.getOpenInventory().getTitle().replace("�lPunish �� �4", "");
						File config = Configs.getPlayerFile(UUID.fromString(player.getOpenInventory().getItem(4)
								.getItemMeta().getLore().get(2).replace("�cUUID: �f", "")), "Player Data");
						FileConfiguration configLoaded = Configs.loadFile(config);
						String reason = player.getOpenInventory().getItem(4).getItemMeta().getLore().get(0)
								.replace("�cPunish Reason: �f", "");
						List<String> list = new ArrayList<>();
						String date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
						Calendar c = Calendar.getInstance();
						c.setTime(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(date));
						c.add(Calendar.HOUR, 24);
						String dateFin = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(c.getTime());
						list.add("tempmute#" + player.getUniqueId() + "#" + reason + "#" + date + "#" + dateFin);
						if (configLoaded.getStringList("Punishments.History") != null)
							list.addAll(configLoaded.getStringList("Punishments.History"));
						configLoaded.set("Punishments.History", list);
						configLoaded.save(config);
						Bukkit.broadcast(
								"�b[STAFF]�f: "
										+ StringUtils.substring(Main.chat.getPlayerPrefix(player), 0, 2).replaceAll("&",
												"�")
										+ player.getName() + " �fmuted "
										+ configLoaded.getString("Name.Prefix").replaceAll("&", "�") + punishName
										+ " �ffor �e" + reason + " �7(" + date + " to " + dateFin + ")",
								"punishgui.use");
						if (Bukkit.getPlayer(punishName) != null)
							Bukkit.getPlayer(punishName).sendMessage("\n�cYou have been muted for 1 day\n�bReason: �e"
									+ reason + " �7(" + date + " to " + dateFin + ")");

						player.closeInventory();
					} else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�2Mute Player �e3 Days")) {
						String punishName = player.getOpenInventory().getTitle().replace("�lPunish �� �4", "");
						File config = Configs.getPlayerFile(UUID.fromString(player.getOpenInventory().getItem(4)
								.getItemMeta().getLore().get(2).replace("�cUUID: �f", "")), "Player Data");
						FileConfiguration configLoaded = Configs.loadFile(config);
						String reason = player.getOpenInventory().getItem(4).getItemMeta().getLore().get(0)
								.replace("�cPunish Reason: �f", "");
						List<String> list = new ArrayList<>();
						String date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
						Calendar c = Calendar.getInstance();
						c.setTime(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(date));
						c.add(Calendar.HOUR, 72);
						String dateFin = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(c.getTime());
						list.add("tempmute#" + player.getUniqueId() + "#" + reason + "#" + date + "#" + dateFin);
						if (configLoaded.getStringList("Punishments.History") != null)
							list.addAll(configLoaded.getStringList("Punishments.History"));
						configLoaded.set("Punishments.History", list);
						configLoaded.save(config);
						Bukkit.broadcast(
								"�b[STAFF]�f: "
										+ StringUtils.substring(Main.chat.getPlayerPrefix(player), 0, 2).replaceAll("&",
												"�")
										+ player.getName() + " �fmuted "
										+ configLoaded.getString("Name.Prefix").replaceAll("&", "�") + punishName
										+ " �ffor �e" + reason + " �7(" + date + " to " + dateFin + ")",
								"punishgui.use");
						if (Bukkit.getPlayer(punishName) != null)
							Bukkit.getPlayer(punishName).sendMessage("\n�cYou have been muted for 3 days\n�bReason: �e"
									+ reason + " �7(" + date + " to " + dateFin + ")");

						player.closeInventory();
					} else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�2Mute Player �e7 Days")) {
						String punishName = player.getOpenInventory().getTitle().replace("�lPunish �� �4", "");
						File config = Configs.getPlayerFile(UUID.fromString(player.getOpenInventory().getItem(4)
								.getItemMeta().getLore().get(2).replace("�cUUID: �f", "")), "Player Data");
						FileConfiguration configLoaded = Configs.loadFile(config);
						String reason = player.getOpenInventory().getItem(4).getItemMeta().getLore().get(0)
								.replace("�cPunish Reason: �f", "");
						List<String> list = new ArrayList<>();
						String date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
						Calendar c = Calendar.getInstance();
						c.setTime(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(date));
						c.add(Calendar.HOUR, 168);
						String dateFin = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(c.getTime());
						list.add("tempmute#" + player.getUniqueId() + "#" + reason + "#" + date + "#" + dateFin);
						if (configLoaded.getStringList("Punishments.History") != null)
							list.addAll(configLoaded.getStringList("Punishments.History"));
						configLoaded.set("Punishments.History", list);
						configLoaded.save(config);
						Bukkit.broadcast(
								"�b[STAFF]�f: "
										+ StringUtils.substring(Main.chat.getPlayerPrefix(player), 0, 2).replaceAll("&",
												"�")
										+ player.getName() + " �fmuted "
										+ configLoaded.getString("Name.Prefix").replaceAll("&", "�") + punishName
										+ " �ffor �e" + reason + " �7(" + date + " to " + dateFin + ")",
								"punishgui.use");
						if (Bukkit.getPlayer(punishName) != null)
							Bukkit.getPlayer(punishName).sendMessage("\n�cYou have been muted for 7 days\n�bReason: �e"
									+ reason + " �7(" + date + " to " + dateFin + ")");

						player.closeInventory();
					} else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�2Unmute Player")) {
						OfflinePlayer punished = Bukkit
								.getOfflinePlayer(player.getOpenInventory().getTitle().replace("�lPunish �� �4", ""));
						String mute = Punishments.getMute(punished);
						File config = Configs.getPlayerFile(UUID.fromString(player.getOpenInventory().getItem(4)
								.getItemMeta().getLore().get(2).replace("�cUUID: �f", "")), "Player Data");
						FileConfiguration configLoaded = Configs.loadFile(config);
						String reason = player.getOpenInventory().getItem(4).getItemMeta().getLore().get(0)
								.replace("�cPunish Reason: �f", "");
						String date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
						List<String> list = configLoaded.getStringList("Punishments.History");
						for (int i = 0; i < list.size(); i++)
							if (list.get(i).contains(mute)) {
								list.set(i, list.get(i) + "#" + player.getUniqueId() + "#" + reason + "#" + date);
								break;
							}
						Bukkit.broadcast(
								"�b[STAFF]�f: "
										+ StringUtils.substring(Main.chat.getPlayerPrefix(player), 0, 2).replaceAll("&",
												"�")
										+ player.getName() + " �funmuted "
										+ configLoaded.getString("Name.Prefix").replaceAll("&", "�")
										+ punished.getName() + " �ffor �e" + reason + " �7(" + date + ")",
								"punishgui.use");
						if (Bukkit.getPlayer(punished.getName()) != null)
							Bukkit.getPlayer(punished.getName()).sendMessage(
									"\n�cYou have been unmuted\n�bReason: �e" + reason + " �7(" + date + ")");
						configLoaded.set("Punishments.History", list);
						configLoaded.save(config);

						player.closeInventory();
					} else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�2Ban Player �e1 Day")) {
						String punishName = player.getOpenInventory().getTitle().replace("�lPunish �� �4", "");
						File config = Configs.getPlayerFile(UUID.fromString(player.getOpenInventory().getItem(4)
								.getItemMeta().getLore().get(2).replace("�cUUID: �f", "")), "Player Data");
						FileConfiguration configLoaded = Configs.loadFile(config);
						String reason = player.getOpenInventory().getItem(4).getItemMeta().getLore().get(0)
								.replace("�cPunish Reason: �f", "");
						List<String> list = new ArrayList<>();
						String date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
						Calendar c = Calendar.getInstance();
						c.setTime(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(date));
						c.add(Calendar.HOUR, 24);
						String dateFin = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(c.getTime());
						list.add("tempban#" + player.getUniqueId() + "#" + reason + "#" + date + "#" + dateFin);
						if (configLoaded.getStringList("Punishments.History") != null)
							list.addAll(configLoaded.getStringList("Punishments.History"));
						configLoaded.set("Punishments.History", list);
						configLoaded.save(config);
						Bukkit.broadcast(
								"�b[STAFF]�f: "
										+ StringUtils.substring(Main.chat.getPlayerPrefix(player), 0, 2).replaceAll("&",
												"�")
										+ player.getName() + " �fbanned "
										+ configLoaded.getString("Name.Prefix").replaceAll("&", "�") + punishName
										+ " �ffor �e" + reason + " �7(" + date + " to " + dateFin + ")",
								"punishgui.use");
						if (Bukkit.getPlayer(punishName) != null)
							Bukkit.getPlayer(punishName)
									.kickPlayer("�cYou have been banned from the server\n\n�bReason: �e" + reason
											+ "\n�7(" + date + " to " + dateFin
											+ ")\n\n\n�dUnban In:�a 1 d, 0 hr, 0 min, 0 sec\n\n\n�2Unfairly banned? Appeal! �e�nhttps://copperuniverse.enjin.com/forums");

						player.closeInventory();
					} else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�2Ban Player �e7 Days")) {
						String punishName = player.getOpenInventory().getTitle().replace("�lPunish �� �4", "");
						File config = Configs.getPlayerFile(UUID.fromString(player.getOpenInventory().getItem(4)
								.getItemMeta().getLore().get(2).replace("�cUUID: �f", "")), "Player Data");
						FileConfiguration configLoaded = Configs.loadFile(config);
						String reason = player.getOpenInventory().getItem(4).getItemMeta().getLore().get(0)
								.replace("�cPunish Reason: �f", "");
						List<String> list = new ArrayList<>();
						String date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
						Calendar c = Calendar.getInstance();
						c.setTime(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(date));
						c.add(Calendar.HOUR, 168);
						String dateFin = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(c.getTime());
						list.add("tempban#" + player.getUniqueId() + "#" + reason + "#" + date + "#" + dateFin);
						if (configLoaded.getStringList("Punishments.History") != null)
							list.addAll(configLoaded.getStringList("Punishments.History"));
						configLoaded.set("Punishments.History", list);
						configLoaded.save(config);
						Bukkit.broadcast(
								"�b[STAFF]�f: "
										+ StringUtils.substring(Main.chat.getPlayerPrefix(player), 0, 2).replaceAll("&",
												"�")
										+ player.getName() + " �fbanned "
										+ configLoaded.getString("Name.Prefix").replaceAll("&", "�") + punishName
										+ " �ffor �e" + reason + " �7(" + date + " to " + dateFin + ")",
								"punishgui.use");
						if (Bukkit.getPlayer(punishName) != null)
							Bukkit.getPlayer(punishName)
									.kickPlayer("�cYou have been banned from the server\n\n�bReason: �e" + reason
											+ "\n�7(" + date + " to " + dateFin
											+ ")\n\n\n�dUnban In:�a 7 d, 0 hr, 0 min, 0 sec\n\n\n�2Unfairly banned? Appeal! �e�nhttps://copperuniverse.enjin.com/forums");

						player.closeInventory();
					} else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�2Ban Player �e14 Days")) {
						String punishName = player.getOpenInventory().getTitle().replace("�lPunish �� �4", "");
						File config = Configs.getPlayerFile(UUID.fromString(player.getOpenInventory().getItem(4)
								.getItemMeta().getLore().get(2).replace("�cUUID: �f", "")), "Player Data");
						FileConfiguration configLoaded = Configs.loadFile(config);
						String reason = player.getOpenInventory().getItem(4).getItemMeta().getLore().get(0)
								.replace("�cPunish Reason: �f", "");
						List<String> list = new ArrayList<>();
						String date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
						Calendar c = Calendar.getInstance();
						c.setTime(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(date));
						c.add(Calendar.HOUR, 336);
						String dateFin = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(c.getTime());
						list.add("tempban#" + player.getUniqueId() + "#" + reason + "#" + date + "#" + dateFin);
						if (configLoaded.getStringList("Punishments.History") != null)
							list.addAll(configLoaded.getStringList("Punishments.History"));
						configLoaded.set("Punishments.History", list);
						configLoaded.save(config);
						Bukkit.broadcast(
								"�b[STAFF]�f: "
										+ StringUtils.substring(Main.chat.getPlayerPrefix(player), 0, 2).replaceAll("&",
												"�")
										+ player.getName() + " �fbanned "
										+ configLoaded.getString("Name.Prefix").replaceAll("&", "�") + punishName
										+ " �ffor �e" + reason + " �7(" + date + " to " + dateFin + ")",
								"punishgui.use");
						if (Bukkit.getPlayer(punishName) != null)
							Bukkit.getPlayer(punishName)
									.kickPlayer("�cYou have been banned from the server\n\n�bReason: �e" + reason
											+ "\n�7(" + date + " to " + dateFin
											+ ")\n\n\n�dUnban In:�a 14 d, 0 hr, 0 min, 0 sec\n\n\n�2Unfairly banned? Appeal! �e�nhttps://copperuniverse.enjin.com/forums");

						player.closeInventory();
					} else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�2Ban Player �e30 Days")) {
						String punishName = player.getOpenInventory().getTitle().replace("�lPunish �� �4", "");
						File config = Configs.getPlayerFile(UUID.fromString(player.getOpenInventory().getItem(4)
								.getItemMeta().getLore().get(2).replace("�cUUID: �f", "")), "Player Data");
						FileConfiguration configLoaded = Configs.loadFile(config);
						String reason = player.getOpenInventory().getItem(4).getItemMeta().getLore().get(0)
								.replace("�cPunish Reason: �f", "");
						List<String> list = new ArrayList<>();
						String date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
						Calendar c = Calendar.getInstance();
						c.setTime(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(date));
						c.add(Calendar.HOUR, 720);
						String dateFin = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(c.getTime());
						list.add("tempban#" + player.getUniqueId() + "#" + reason + "#" + date + "#" + dateFin);
						if (configLoaded.getStringList("Punishments.History") != null)
							list.addAll(configLoaded.getStringList("Punishments.History"));
						configLoaded.set("Punishments.History", list);
						configLoaded.save(config);
						Bukkit.broadcast(
								"�b[STAFF]�f: "
										+ StringUtils.substring(Main.chat.getPlayerPrefix(player), 0, 2).replaceAll("&",
												"�")
										+ player.getName() + " �fbanned "
										+ configLoaded.getString("Name.Prefix").replaceAll("&", "�") + punishName
										+ " �ffor �e" + reason + " �7(" + date + " to " + dateFin + ")",
								"punishgui.use");
						if (Bukkit.getPlayer(punishName) != null)
							Bukkit.getPlayer(punishName)
									.kickPlayer("�cYou have been banned from the server\n\n�bReason: �e" + reason
											+ "\n�7(" + date + " to " + dateFin
											+ ")\n\n\n�dUnban In:�a 30 d, 0 hr, 0 min, 0 sec\n\n\n�2Unfairly banned? Appeal! �e�nhttps://copperuniverse.enjin.com/forums");

						player.closeInventory();
					} else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�2Unban Player")) {
						OfflinePlayer punished = Bukkit
								.getOfflinePlayer(player.getOpenInventory().getTitle().replace("�lPunish �� �4", ""));
						String ban = Punishments.getBan(punished);
						File config = Configs.getPlayerFile(UUID.fromString(player.getOpenInventory().getItem(4)
								.getItemMeta().getLore().get(2).replace("�cUUID: �f", "")), "Player Data");
						FileConfiguration configLoaded = Configs.loadFile(config);
						String reason = player.getOpenInventory().getItem(4).getItemMeta().getLore().get(0)
								.replace("�cPunish Reason: �f", "");
						String date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
						List<String> list = configLoaded.getStringList("Punishments.History");
						for (int i = 0; i < list.size(); i++)
							if (list.get(i).contains(ban)) {
								list.set(i, list.get(i) + "#" + player.getUniqueId() + "#" + reason + "#" + date);
								break;
							}
						Bukkit.broadcast(
								"�b[STAFF]�f: "
										+ StringUtils.substring(Main.chat.getPlayerPrefix(player), 0, 2).replaceAll("&",
												"�")
										+ player.getName() + " �funbanned "
										+ configLoaded.getString("Name.Prefix").replaceAll("&", "�")
										+ punished.getName() + " �ffor �e" + reason + " �7(" + date + ")",
								"punishgui.use");
						configLoaded.set("Punishments.History", list);
						configLoaded.save(config);

						player.closeInventory();
					} else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�2Mute Player �ePERMANENT")) {
						String punishName = player.getOpenInventory().getTitle().replace("�lPunish �� �4", "");
						File config = Configs.getPlayerFile(UUID.fromString(player.getOpenInventory().getItem(4)
								.getItemMeta().getLore().get(2).replace("�cUUID: �f", "")), "Player Data");
						FileConfiguration configLoaded = Configs.loadFile(config);
						String reason = player.getOpenInventory().getItem(4).getItemMeta().getLore().get(0)
								.replace("�cPunish Reason: �f", "");
						List<String> list = new ArrayList<>();
						String date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
						list.add("mute#" + player.getUniqueId() + "#" + reason + "#" + date);
						if (configLoaded.getStringList("Punishments.History") != null)
							list.addAll(configLoaded.getStringList("Punishments.History"));
						configLoaded.set("Punishments.History", list);
						configLoaded.save(config);
						Bukkit.broadcast(
								"�b[STAFF]�f: "
										+ StringUtils.substring(Main.chat.getPlayerPrefix(player), 0, 2).replaceAll("&",
												"�")
										+ player.getName() + " �fpermanently banned "
										+ configLoaded.getString("Name.Prefix").replaceAll("&", "�") + punishName
										+ " �ffor �e" + reason + " �7(" + date + ")",
								"punishgui.use");
						if (Bukkit.getPlayer(punishName) != null)
							Bukkit.getPlayer(punishName).sendMessage(
									"\n�cYou have been permanetly muted\n�bReason: �e" + reason + " �7(" + date + ")");

						player.closeInventory();
					} else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�2Ban Player �ePERMANENT")) {
						String punishName = player.getOpenInventory().getTitle().replace("�lPunish �� �4", "");
						File config = Configs.getPlayerFile(UUID.fromString(player.getOpenInventory().getItem(4)
								.getItemMeta().getLore().get(2).replace("�cUUID: �f", "")), "Player Data");
						FileConfiguration configLoaded = Configs.loadFile(config);
						String reason = player.getOpenInventory().getItem(4).getItemMeta().getLore().get(0)
								.replace("�cPunish Reason: �f", "");
						List<String> list = new ArrayList<>();
						String date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
						list.add("ban#" + player.getUniqueId() + "#" + reason + "#" + date);
						if (configLoaded.getStringList("Punishments.History") != null)
							list.addAll(configLoaded.getStringList("Punishments.History"));
						configLoaded.set("Punishments.History", list);
						configLoaded.save(config);
						Bukkit.broadcast(
								"�b[STAFF]�f: "
										+ StringUtils.substring(Main.chat.getPlayerPrefix(player), 0, 2).replaceAll("&",
												"�")
										+ player.getName() + " �fpermanently banned "
										+ configLoaded.getString("Name.Prefix").replaceAll("&", "�") + punishName
										+ " �ffor �e" + reason + " �7(" + date + ")",
								"punishgui.use");
						if (Bukkit.getPlayer(punishName) != null)
							Bukkit.getPlayer(punishName)
									.kickPlayer("�cYou have been banned from the server\n\n�bReason: �e" + reason
											+ "\n�7(" + date
											+ ")\n\n\n�dUnban In:�a Never\n\n\n�2Unfairly banned? Appeal! �e�nhttps://copperuniverse.enjin.com/forums");

						player.closeInventory();
					}
					e.setCancelled(true);
				} else if (inventory.getName().contains("�8�lFood")) {
					if (clicked.hasItemMeta())
						if (clicked.getItemMeta().hasLore()) {
							double cost = Double.parseDouble(clicked.getItemMeta().getLore().get(2)
									.replace("�7Cost: �6$", "").replaceAll(",", ""));
							if (Main.econ.getBalance(player) >= cost) {
								Main.econ.withdrawPlayer(player, cost);

								ItemStack giveItem = new ItemStack(clicked.getType(), 1,
										clicked.getData().getData());
								ItemMeta giveItemMeta = giveItem.getItemMeta();
								giveItemMeta
										.setDisplayName(clicked.getItemMeta().getDisplayName().replace("�b�l", "�r"));
								ArrayList<String> giveItemLore = new ArrayList<>();
								giveItemLore.add(clicked.getItemMeta().getLore().get(0));
								giveItemMeta.setLore(giveItemLore);
								giveItem.setItemMeta(giveItemMeta);

								if (player.getInventory().firstEmpty() != -1)
									player.getInventory().addItem(giveItem);
								else
									player.getWorld().dropItemNaturally(player.getLocation(), giveItem);
							} else
								player.sendMessage("�cYou don't have enough money!");
						}
					e.setCancelled(true);
				} else if (inventory.getName().contains("�8�lSell")) {
					if (clicked.hasItemMeta())
						if (clicked.getItemMeta().hasLore()) {
							int itemAmount = 0;
							for (ItemStack item : player.getInventory().getContents())
								if (item != null)
									if (item.getType() == clicked.getType()) {
										itemAmount += item.getAmount();
										player.getInventory().removeItem(item);
									}
							if (itemAmount > 0) {
								double money = itemAmount * Double.parseDouble(clicked.getItemMeta().getLore().get(0)
										.replace("�7Price: �6$", "").replaceAll(",", ""));
								Main.econ.depositPlayer(player, money);
								player.closeInventory();
								Text.showActionBar(player,
										"{\"text\":\"�d"
												+ WordUtils.capitalizeFully(
														clicked.getType().toString().replaceAll("_", " "))
												+ ": �c-" + NumberFormat.getNumberInstance(Locale.US).format(itemAmount)
												+ " �a+$" + NumberFormat.getNumberInstance(Locale.US).format(money)
												+ "\"}");
							} else
								player.sendMessage("�cYou don't have any of this item!");
						}
					e.setCancelled(true);
				} else if (inventory.getName().contains("�8�lEquipment")) {
					if (clicked.hasItemMeta())
						if (clicked.getItemMeta().hasLore())
							if (Main.econ.getBalance(player) >= Integer.parseInt(clicked.getItemMeta().getLore().get(2)
									.replace("�7Cost: �6$", "").replaceAll(",", ""))) {
								Main.econ.withdrawPlayer(player, Integer.parseInt(clicked.getItemMeta().getLore().get(2)
										.replace("�7Cost: �6$", "").replaceAll(",", "")));
								ItemStack give = new ItemStack(clicked.getType(), 1);
								ItemMeta giveMeta = give.getItemMeta();
								giveMeta.setDisplayName(clicked.getItemMeta().getDisplayName() + " �f�l1");
								ArrayList<String> giveLore = new ArrayList<>();
								giveLore.add("�c�lEXP:");
								giveLore.add("�fProgress: �b0�7/�b10");
								giveLore.add(" ");
								giveLore.add("�c�lSTATS:");
								giveLore.add("�6Familiarity: �a0");
								giveLore.add("�6Luck: �a" + ThreadLocalRandom.current().nextInt(0, 25 + 1));
								giveLore.add(" ");
								giveLore.add("�c�lMUTATIONS:");
								giveLore.add("�7Level me up and I may mutate!");
								giveMeta.setLore(giveLore);
								giveMeta.spigot().setUnbreakable(true);
								giveMeta.addItemFlags(ItemFlag.values());
								give.setItemMeta(giveMeta);
								if (player.getInventory().firstEmpty() != -1)
									player.getInventory().addItem(give);
								else
									player.getWorld().dropItemNaturally(player.getLocation(), give);
							} else
								player.sendMessage("�cYou don't have enough money");
					e.setCancelled(true);
				} else if (inventory.getName().contains("Kill Message")) {
					if (clicked.getType() == Material.PAINTING)
						player.openInventory(Inventories.cosmetics0);
					else if (clicked.getType() == Material.ARROW)
						Inventories.generateCosmeticGUI("Kill Message", player,
								Integer.parseInt(clicked.getItemMeta().getDisplayName().replace("�ePage ", "")));
					else if (clicked.hasItemMeta() && clicked.getItemMeta().hasLore())
						if (clicked.getItemMeta().getLore().get(0).contains("�7Get the ")) {
							File config = Configs.getPlayerFile(player, "Player Data");
							FileConfiguration configLoaded = Configs.loadFile(config);
							if (clicked.getItemMeta().getLore().get(2).contains("�aCOMMON")
									&& configLoaded.getInt("LootEggs.Eggshells") >= 25) {
								configLoaded.set("LootEggs.Eggshells", configLoaded.getInt("LootEggs.Eggshells") - 25);
								configLoaded.save(config);
								Main.perms.playerAdd(player,
										"universalrpgp.kill_message." + ChatColor
												.stripColor(clicked.getItemMeta().getDisplayName())
												.replace(" Kill Message", "").toLowerCase().replaceAll(" ", "_"));
								Inventories.generateCosmeticGUI("Kill Message", player,
										Integer.parseInt(inventory.getName().replace("�6Kill Messages Page ", "")));
							} else if (clicked.getItemMeta().getLore().get(2).contains("�5RARE")
									&& configLoaded.getInt("LootEggs.Eggshells") >= 50) {
								configLoaded.set("LootEggs.Eggshells", configLoaded.getInt("LootEggs.Eggshells") - 50);
								configLoaded.save(config);
								Main.perms.playerAdd(player,
										"universalrpgp.kill_message." + ChatColor
												.stripColor(clicked.getItemMeta().getDisplayName())
												.replace(" Kill Message", "").toLowerCase().replaceAll(" ", "_"));
								Inventories.generateCosmeticGUI("Kill Message", player,
										Integer.parseInt(inventory.getName().replace("�6Kill Messages Page ", "")));
							} else if (clicked.getItemMeta().getLore().get(2).contains("�6LEGENDARY")
									&& configLoaded.getInt("LootEggs.Eggshells") >= 100) {
								configLoaded.set("LootEggs.Eggshells", configLoaded.getInt("LootEggs.Eggshells") - 100);
								configLoaded.save(config);
								Main.perms.playerAdd(player,
										"universalrpgp.kill_message." + ChatColor
												.stripColor(clicked.getItemMeta().getDisplayName())
												.replace(" Kill Message", "").toLowerCase().replaceAll(" ", "_"));
								Inventories.generateCosmeticGUI("Kill Message", player,
										Integer.parseInt(inventory.getName().replace("�6Kill Messages Page ", "")));
							}
						} else if (clicked.getItemMeta().getLore().contains("�eClick to select")) {
							File config = Configs.getPlayerFile(player, "Player Data");
							FileConfiguration configLoaded = Configs.loadFile(config);
							configLoaded.set("Cosmetics.KillMessage", ChatColor.stripColor(clicked.getItemMeta()
									.getDisplayName().replace(" Kill Message", "").toLowerCase().replaceAll(" ", "_")));
							configLoaded.save(config);
							Inventories.generateCosmeticGUI("Kill Message", player,
									Integer.parseInt(inventory.getName().replace("�6Kill Messages Page ", "")));
						}
					e.setCancelled(true);
				} else if (inventory.getName().contains("Chat Tag")) {
					if (clicked.getType() == Material.PAINTING)
						player.openInventory(Inventories.cosmetics0);
					else if (clicked.getType() == Material.ARROW)
						Inventories.generateCosmeticGUI("Chat Tag", player,
								Integer.parseInt(clicked.getItemMeta().getDisplayName().replace("�ePage ", "")));
					else if (clicked.hasItemMeta() && clicked.getItemMeta().hasLore())
						if (clicked.getItemMeta().getLore().get(0).contains("�7Get the ")) {
							File config = Configs.getPlayerFile(player, "Player Data");
							FileConfiguration configLoaded = Configs.loadFile(config);
							if (clicked.getItemMeta().getLore().get(2).contains("�aCOMMON")
									&& configLoaded.getInt("LootEggs.Eggshells") >= 25) {
								configLoaded.set("LootEggs.Eggshells", configLoaded.getInt("LootEggs.Eggshells") - 25);
								configLoaded.save(config);
								Main.perms.playerAdd(player,
										"universalrpgp.chat_tag."
												+ ChatColor.stripColor(clicked.getItemMeta().getDisplayName())
														.replace(" Chat Tag", "").toLowerCase().replaceAll(" ", "_"));
								Inventories.generateCosmeticGUI("Chat Tag", player,
										Integer.parseInt(inventory.getName().replace("�6Chat Tags Page ", "")));
							} else if (clicked.getItemMeta().getLore().get(2).contains("�5RARE")
									&& configLoaded.getInt("LootEggs.Eggshells") >= 50) {
								configLoaded.set("LootEggs.Eggshells", configLoaded.getInt("LootEggs.Eggshells") - 50);
								configLoaded.save(config);
								Main.perms.playerAdd(player,
										"universalrpgp.chat_tag."
												+ ChatColor.stripColor(clicked.getItemMeta().getDisplayName())
														.replace(" Chat Tag", "").toLowerCase().replaceAll(" ", "_"));
								Inventories.generateCosmeticGUI("Chat Tag", player,
										Integer.parseInt(inventory.getName().replace("�6Chat Tags Page ", "")));
							} else if (clicked.getItemMeta().getLore().get(2).contains("�6LEGENDARY")
									&& configLoaded.getInt("LootEggs.Eggshells") >= 100) {
								configLoaded.set("LootEggs.Eggshells", configLoaded.getInt("LootEggs.Eggshells") - 100);
								configLoaded.save(config);
								Main.perms.playerAdd(player,
										"universalrpgp.chat_tag."
												+ ChatColor.stripColor(clicked.getItemMeta().getDisplayName())
														.replace(" Chat Tag", "").toLowerCase().replaceAll(" ", "_"));
								Inventories.generateCosmeticGUI("Chat Tag", player,
										Integer.parseInt(inventory.getName().replace("�6Chat Tags Page ", "")));
							}
						} else if (clicked.getItemMeta().getLore().contains("�eClick to select")) {
							File config = Configs.getPlayerFile(player, "Player Data");
							FileConfiguration configLoaded = Configs.loadFile(config);
							configLoaded.set("Cosmetics.ChatTag", ChatColor.stripColor(clicked.getItemMeta()
									.getDisplayName().replace(" Chat Tag", "").toLowerCase().replaceAll(" ", "_")));
							configLoaded.save(config);
							Inventories.generateCosmeticGUI("Chat Tag", player,
									Integer.parseInt(inventory.getName().replace("�6Chat Tags Page ", "")));
						}
					e.setCancelled(true);
				} else if (inventory.getName().contains("Projectile Trail")) {
					if (clicked.getType() == Material.PAINTING)
						player.openInventory(Inventories.cosmetics0);
					else if (clicked.getType() == Material.ARROW)
						Inventories.generateCosmeticGUI("Projectile Trail", player,
								Integer.parseInt(clicked.getItemMeta().getDisplayName().replace("�ePage ", "")));
					else if (clicked.hasItemMeta() && clicked.getItemMeta().hasLore())
						if (clicked.getItemMeta().getLore().get(0).contains("�7Get the ")) {
							File config = Configs.getPlayerFile(player, "Player Data");
							FileConfiguration configLoaded = Configs.loadFile(config);
							if (clicked.getItemMeta().getLore().get(2).contains("�aCOMMON")
									&& configLoaded.getInt("LootEggs.Eggshells") >= 25) {
								configLoaded.set("LootEggs.Eggshells", configLoaded.getInt("LootEggs.Eggshells") - 25);
								configLoaded.save(config);
								Main.perms.playerAdd(player,
										"universalrpgp.projectile_trail." + ChatColor
												.stripColor(clicked.getItemMeta().getDisplayName())
												.replace(" Projectile Trail", "").toLowerCase().replaceAll(" ", "_"));
								Inventories.generateCosmeticGUI("Projectile Trail", player,
										Integer.parseInt(inventory.getName().replace("�6Projectile Trails Page ", "")));
							} else if (clicked.getItemMeta().getLore().get(2).contains("�5RARE")
									&& configLoaded.getInt("LootEggs.Eggshells") >= 50) {
								configLoaded.set("LootEggs.Eggshells", configLoaded.getInt("LootEggs.Eggshells") - 50);
								configLoaded.save(config);
								Main.perms.playerAdd(player,
										"universalrpgp.projectile_trail." + ChatColor
												.stripColor(clicked.getItemMeta().getDisplayName())
												.replace(" Projectile Trail", "").toLowerCase().replaceAll(" ", "_"));
								Inventories.generateCosmeticGUI("Projectile Trail", player,
										Integer.parseInt(inventory.getName().replace("�6Projectile Trails Page ", "")));
							} else if (clicked.getItemMeta().getLore().get(2).contains("�6LEGENDARY")
									&& configLoaded.getInt("LootEggs.Eggshells") >= 100) {
								configLoaded.set("LootEggs.Eggshells", configLoaded.getInt("LootEggs.Eggshells") - 100);
								configLoaded.save(config);
								Main.perms.playerAdd(player,
										"universalrpgp.projectile_trail." + ChatColor
												.stripColor(clicked.getItemMeta().getDisplayName())
												.replace(" Projectile Trail", "").toLowerCase().replaceAll(" ", "_"));
								Inventories.generateCosmeticGUI("Projectile Trail", player,
										Integer.parseInt(inventory.getName().replace("�6Projectile Trails Page ", "")));
							}
						} else if (clicked.getItemMeta().getLore().contains("�eClick to select")) {
							File config = Configs.getPlayerFile(player, "Player Data");
							FileConfiguration configLoaded = Configs.loadFile(config);
							configLoaded.set("Cosmetics.ProjectileTrail",
									ChatColor.stripColor(clicked.getItemMeta().getDisplayName()
											.replace(" Projectile Trail", "").toLowerCase().replaceAll(" ", "_")));
							configLoaded.save(config);
							Inventories.generateCosmeticGUI("Projectile Trail", player,
									Integer.parseInt(inventory.getName().replace("�6Projectile Trails Page ", "")));
						}
					e.setCancelled(true);
				} else if (inventory.getName().contains("Ship Skin")) {
					if (clicked.getType() == Material.PAINTING)
						player.openInventory(Inventories.cosmetics0);
					else if (clicked.getType() == Material.ARROW)
						Inventories.generateCosmeticGUI("Ship Skin", player,
								Integer.parseInt(clicked.getItemMeta().getDisplayName().replace("�ePage ", "")));
					else if (clicked.hasItemMeta() && clicked.getItemMeta().hasLore())
						if (clicked.getItemMeta().getLore().get(0).contains("�7Get the ")) {
							File config = Configs.getPlayerFile(player, "Player Data");
							FileConfiguration configLoaded = Configs.loadFile(config);
							if (clicked.getItemMeta().getLore().get(2).contains("�aCOMMON")
									&& configLoaded.getInt("LootEggs.Eggshells") >= 25) {
								configLoaded.set("LootEggs.Eggshells", configLoaded.getInt("LootEggs.Eggshells") - 25);
								configLoaded.save(config);
								Main.perms.playerAdd(player,
										"universalrpgp.ship_skin."
												+ ChatColor.stripColor(clicked.getItemMeta().getDisplayName())
														.replace(" Ship Skin", "").toLowerCase().replaceAll(" ", "_"));
								Inventories.generateCosmeticGUI("Ship Skin", player,
										Integer.parseInt(inventory.getName().replace("�6Ship Skins Page ", "")));
							} else if (clicked.getItemMeta().getLore().get(2).contains("�5RARE")
									&& configLoaded.getInt("LootEggs.Eggshells") >= 50) {
								configLoaded.set("LootEggs.Eggshells", configLoaded.getInt("LootEggs.Eggshells") - 50);
								configLoaded.save(config);
								Main.perms.playerAdd(player,
										"universalrpgp.ship_skin."
												+ ChatColor.stripColor(clicked.getItemMeta().getDisplayName())
														.replace(" Ship Skin", "").toLowerCase().replaceAll(" ", "_"));
								Inventories.generateCosmeticGUI("Ship Skin", player,
										Integer.parseInt(inventory.getName().replace("�6Ship Skins Page ", "")));
							} else if (clicked.getItemMeta().getLore().get(2).contains("�6LEGENDARY")
									&& configLoaded.getInt("LootEggs.Eggshells") >= 100) {
								configLoaded.set("LootEggs.Eggshells", configLoaded.getInt("LootEggs.Eggshells") - 100);
								configLoaded.save(config);
								Main.perms.playerAdd(player,
										"universalrpgp.ship_skin."
												+ ChatColor.stripColor(clicked.getItemMeta().getDisplayName())
														.replace(" Ship Skin", "").toLowerCase().replaceAll(" ", "_"));
								Inventories.generateCosmeticGUI("Ship Skin", player,
										Integer.parseInt(inventory.getName().replace("�6Ship Skins Page ", "")));
							}
						} else if (clicked.getItemMeta().getLore().contains("�eClick to select")) {
							File config = Configs.getPlayerFile(player, "Player Data");
							FileConfiguration configLoaded = Configs.loadFile(config);
							configLoaded.set("Cosmetics.ShipSkin", ChatColor.stripColor(clicked.getItemMeta()
									.getDisplayName().replace(" Ship Skin", "").toLowerCase().replaceAll(" ", "_")));
							configLoaded.save(config);
							Inventories.generateCosmeticGUI("Ship Skin", player,
									Integer.parseInt(inventory.getName().replace("�6Ship Skins Page ", "")));
						}
					e.setCancelled(true);
				} else if (Objects.equals(inventory.getName(), "�4Stats"))
					e.setCancelled(true);
				else if (Objects.equals(inventory.getName(), "�3Spaceship")) {
					if (clicked.hasItemMeta())
						if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�6Ship Skins"))
							Inventories.generateCosmeticGUI("Ship Skin", player, 1);
						else if (clicked.getType() == Material.PAINTING)
							Inventories.generateGUI(player, "spaceship0");
						else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�7Droria 632 Spawn"))
							Ships.enterSpaceship(player,
									new Location(Bukkit.getServer().getWorld("world"), 3944.5, 4.5, 4128.5, 180, 0),
									50);
						else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�7AIR Mine"))
							Ships.enterSpaceship(player,
									new Location(Bukkit.getServer().getWorld("world"), 4004.5, 5.5, 4042.5, -90, 0),
									50);
						else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�7DIRT Mine"))
							Ships.enterSpaceship(player,
									new Location(Bukkit.getServer().getWorld("world"), 3871.5, 63.5, 4212.0, -90, 0),
									50);
						else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�7GRASS Mine"))
							Ships.enterSpaceship(player,
									new Location(Bukkit.getServer().getWorld("world"), 3980.5, 4.5, 3856.5, -135, 0),
									50);
						else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�7Danger Zone �c�lPVP"))
							Ships.enterSpaceship(player,
									new Location(Bukkit.getServer().getWorld("world"), 4062.5, 12.5, 3937.5, -90, 0),
									50);
						else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�7Glosie Spawn"))
							Ships.enterSpaceship(player,
									new Location(Bukkit.getServer().getWorld("world"), 4612.5, 4.5, 3732.5, 0, 0), 50);
						else if (Objects.equals(clicked.getItemMeta().getDisplayName(), "�7LOG Mine"))
							Ships.enterSpaceship(player,
									new Location(Bukkit.getServer().getWorld("world"), 4614.5, 2.5, 3817.5, 45, 0), 50);
						else if (clicked.getType() == Material.BED)
							Ships.enterSpaceship(player,
									new Location(Bukkit.getServer().getWorld("world"), -10.5, 99.5, -15.5, 0, 0), 0);
					e.setCancelled(true);
				}
			} else if (inventory.getName().contains("�4Player Vault �9#")) {
				if (clicked.getType() == Material.SKULL_ITEM)
					if (((SkullMeta) clicked.getItemMeta()).getOwner() == null) {
						e.setCancelled(true);
						player.sendMessage("�cAn error occured trying to add this item to your vault.");
					}
			} else if (holding != null)
				if (holding.hasItemMeta())
					if (holding.getItemMeta().hasLore() && holding.getItemMeta().getLore()
							.contains("�7Drag and drop onto an item to rename it,")) {
						if (Stats.miningItems.contains(clicked.getType())
								|| Stats.pvpItems.contains(clicked.getType())
								|| clicked.getType() == Material.FISHING_ROD)
							if (holding.getAmount() == 1) {
								e.setCancelled(true);
								e.setCursor(new ItemStack(Material.AIR));
								player.updateInventory();
								new AnvilGUI(Main.plugin, player, clicked.getType(), holding, "Rename Item",
										(playerInner, reply) -> {
ItemMeta clickedNewMeta = clicked.getItemMeta();
											try {
												clickedNewMeta.setDisplayName("�d"
														+ ChatColor.translateAlternateColorCodes('&', reply)
																.replaceAll(" �f�l", "�f�l ").trim()
																.replaceAll("\\s+", " ")
														+ "�r �f�l"
														+ clicked.getItemMeta().getDisplayName().split(" �f�l")[1]);
											} catch (NullPointerException e1) {
												clickedNewMeta.setDisplayName(" �f�l"
														+ clicked.getItemMeta().getDisplayName().split(" �f�l")[1]);
											}
											clicked.setItemMeta(clickedNewMeta);

											playerInner.getInventory().setItem(e.getSlot(), clicked);

											return null;
										});
							} else {
								e.setCancelled(true);
								player.sendMessage("�cYou can only use 1 item nametag at a time");
							}
					}
	}
}
