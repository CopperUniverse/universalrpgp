package com.stupidbot.universalcorep.listeners;

import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.mutations.Mutations;
import com.stupidbot.universalcorep.utils.Configs;
import com.stupidbot.universalcorep.utils.MiscellaneousUtils;
import com.stupidbot.universalcorep.utils.Text;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.io.File;
import java.util.concurrent.ThreadLocalRandom;

public class OnEntityDamageEntity implements Listener {

	@EventHandler
	public void onEntityDamageEntity(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player)
			if (MiscellaneousUtils.locIsInRegion(e.getDamager().getLocation(), "danger_zone")) {
				Player player = (Player) e.getDamager();
				Entity damaged = e.getEntity();
				File config = Configs.getPlayerFile(player.getUniqueId(), "Player Data");
				FileConfiguration configLoaded = Configs.loadFile(config);
				switch (player.getItemInHand().getType()) {
				case WOOD_SWORD:
					if (configLoaded.getInt("Level.Level") < 15) {
						Text.showTitle(player, "{\"text\":\"\"}", 5, 20, 5);
						Text.showSubtitle(player,
								"[{\"text\":\"REQUIRES LEVEL \",\"color\":\"yellow\",\"bold\":\"true\"}, {\"text\":\" 15\",\"color\":\"white\",\"bold\":\"true\"}]",
								5, 20, 5);
						e.setCancelled(true);
					}
					break;
				default:
					break;
				}
				if (!(e.isCancelled())) {
					if (player.getItemInHand().containsEnchantment(Mutations.SHARPENER))
						e.setDamage(e.getDamage()
								* ((player.getItemInHand().getItemMeta().getEnchantLevel(Mutations.SLIPPERY) / 4) + 1));

					ArmorStand stand = Bukkit.getWorld("world")
							.spawn(damaged.getLocation().add(0, 1.75, 0), ArmorStand.class);
					stand.setVisible(false);
					stand.setVelocity(new Vector(ThreadLocalRandom.current().nextDouble(-0.15, 0.15), 0.5,
							ThreadLocalRandom.current().nextDouble(-0.15, 0.15)));
					stand.setMarker(true);
					stand.setCustomName("§c❤ " + (e.getDamage() > 0 ? ("-" + e.getDamage()) : ("§a+" + e.getDamage())));
					stand.setCustomNameVisible(true);
					Main.removeList.add(stand);
					OnEntityInteract.stopInteractList.add(stand);

					new BukkitRunnable() {
						@Override
						public void run() {
							if (stand.isOnGround()) {
								stand.remove();
								Main.removeList.remove(stand);
								OnEntityInteract.stopInteractList.remove(stand);
								cancel();
							} else if (damaged instanceof Player) {
								PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(stand.getEntityId());
								((CraftPlayer) damaged).getHandle().playerConnection.sendPacket(packet);
							}
						}
					}.runTaskTimer(Main.plugin, 0, 1);

					if (player.getItemInHand().containsEnchantment(Mutations.SLIPPERY))
						if (ThreadLocalRandom.current().nextInt(1, 101) <= player.getItemInHand().getItemMeta()
								.getEnchantLevel(Mutations.SLIPPERY)) {
							ItemStack item = player.getItemInHand();
							player.playSound(player.getLocation(), Sound.BAT_TAKEOFF, 2, 2);
							player.sendMessage("§4" + Mutations.SLIPPERY.getName() + " "
									+ Text.toRoman(
											player.getItemInHand().getItemMeta().getEnchantLevel(Mutations.SLIPPERY))
									+ " §emade you drop your "
									+ player.getItemInHand().getItemMeta().getDisplayName().split(" §f§l")[0]);
							player.setItemInHand(null);
							player.getWorld().dropItemNaturally(player.getEyeLocation(), item)
									.setVelocity(new Vector(player.getLocation().getDirection().getX(),
											player.getLocation().getDirection().getY(),
											player.getLocation().getDirection().getZ()).normalize().multiply(0.4));
						}
				}
			}
	}
}