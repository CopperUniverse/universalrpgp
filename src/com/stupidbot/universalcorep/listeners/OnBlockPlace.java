package com.stupidbot.universalcorep.listeners;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.line.TextLine;
import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.utils.MiscellaneousUtils;
import com.stupidbot.universalcorep.utils.ParticleEffect;
import io.netty.util.internal.ThreadLocalRandom;
import net.md_5.bungee.api.ChatColor;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class OnBlockPlace implements Listener {

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {
		if (MiscellaneousUtils.locIsInRegion(e.getBlock().getLocation(), "droria_632"))
			if (e.getBlock().getType() == Material.DRAGON_EGG) {
				Player player = e.getPlayer();
				Block block = e.getBlock();
				if (player.getItemInHand().hasItemMeta() && player.getItemInHand().getItemMeta().hasDisplayName()
						&& (player.getItemInHand().getItemMeta().getDisplayName().contains("�aPLANET")
								|| player.getItemInHand().getItemMeta().getDisplayName().contains("�bGALAXY")
								|| player.getItemInHand().getItemMeta().getDisplayName().contains("�5UNIVERSE"))) {
					String rank = player.getItemInHand().getItemMeta().getDisplayName().replace(" �cIngot Mission", "");
					OnBlockBreak.blockReset.put(block.getLocation(),
							"AIR:0:" + player.getUniqueId().toString() + ":" + ChatColor.stripColor(rank));
					Bukkit.broadcastMessage(
							StringUtils.substring(Main.chat.getPlayerPrefix(player).replaceAll("&", "�"), 0, 2)
									+ player.getName() + " �ejust placed a " + rank
									+ " �cIngot Mission �esomewhere in Droria 632, find it for a �d�n�lFREE RANK�e!");
					Bukkit.getWorld("world").playEffect(block.getLocation(), Effect.MOBSPAWNER_FLAMES, 1);
					Hologram countdown = HologramsAPI.createHologram(Main.plugin,
							block.getLocation().add(block.getLocation().getX() > 0 ? 0.5 : -0.5, 3.5,
									block.getLocation().getZ() > 0 ? 0.5 : -0.5));
					Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(),
							"givevoucher " + player.getName() + " " + ChatColor.stripColor(rank) + " 1");
					countdown.appendTextLine(
							StringUtils.substring(Main.chat.getPlayerPrefix(player).replaceAll("&", "�"), 0, 2)
									+ player.getName() + "'s " + rank + " �cIngot Mission");
					TextLine time = countdown.appendTextLine("�6�lDisappears in 60m 0s!");
					ItemStack item = new ItemStack(Material.DRAGON_EGG);
					ItemMeta itemMeta = item.getItemMeta();
					itemMeta.addEnchant(Enchantment.ARROW_DAMAGE, 0, true);
					item.setItemMeta(itemMeta);
					countdown.appendItemLine(item);
					e.setCancelled(true);
					countdown.appendTextLine("�e�oPunch me for a rank voucher!");// TODO
																					// Play
																					// sound
					if (e.getPlayer().getItemInHand().getAmount() == 1)
						e.getPlayer().setItemInHand(new ItemStack(Material.AIR));
					else
						e.getPlayer().getItemInHand().setAmount(e.getPlayer().getItemInHand().getAmount() - 1);

					new BukkitRunnable() {
						double t = 0;

						@Override
						public void run() {
							for (double i = 0; i < t * 3; i += 1) {
								double dx = Math.cos(i) * t / 4;
								double dz = Math.sin(i) * t / 4;

								Location pLoc = block.getLocation().add(
										(block.getLocation().getX() > 0 ? 0.5 : -0.5) + dx, 0.1,
										(block.getLocation().getZ() > 0 ? 0.5 : -0.5) + dz);
								ParticleEffect.REDSTONE.display(
										new ParticleEffect.OrdinaryColor(ThreadLocalRandom.current().nextInt(0, 256),
												ThreadLocalRandom.current().nextInt(0, 256),
												ThreadLocalRandom.current().nextInt(0, 256)),
										pLoc, 256);
							}
							if (t > 20)
								cancel();
							else
								t++;
						}
					}.runTaskTimerAsynchronously(Main.plugin, 0, 1);

					new BukkitRunnable() {
						int i = 3600;

						@Override
						public void run() {
							if (OnBlockBreak.blockReset.containsKey(block.getLocation())) {
								i--;
								Bukkit.getWorld("world").playEffect(block.getLocation(), Effect.MOBSPAWNER_FLAMES, 1);
								time.setText("�6�lDisappears in " + (i / 60) + "m " + (i % 60) + "s" + "!");
								if (i == 0) {
									countdown.delete();
									block.getChunk().load(true);
									block.setType(Material.AIR);
									OnBlockBreak.blockReset.remove(block.getLocation());
									Bukkit.broadcastMessage(StringUtils
											.substring(Main.chat.getPlayerPrefix(player).replaceAll("&", "�"), 0, 2)
											+ player.getName() + "'s " + rank + " �cIngot Mission �edisappeared :O!");
									cancel();
								}
							} else {
								countdown.delete();
								cancel();
							}
						}
					}.runTaskTimer(Main.plugin, 20, 20);
					new BukkitRunnable() {
						@Override
						public void run() {
							block.setType(Material.DRAGON_EGG);
						}
					}.runTaskLater(Main.plugin, 1);
				} else if (!(e.getPlayer().getGameMode() == GameMode.CREATIVE)) {
					e.getPlayer().sendMessage("�5�lDENIED�d You can't do that action here!");
					e.setCancelled(true);
				}
			} else if (!(e.getPlayer().getGameMode() == GameMode.CREATIVE)) {
				e.getPlayer().sendMessage("�5�lDENIED�d You can't do that action here!");
				e.setCancelled(true);
			}
	}
}
