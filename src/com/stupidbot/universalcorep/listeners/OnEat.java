package com.stupidbot.universalcorep.listeners;

import com.stupidbot.universalcorep.utils.Stats;
import com.stupidbot.universalcorep.utils.Text;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;

import java.text.NumberFormat;
import java.util.Locale;

public class OnEat implements Listener {

	@EventHandler
	public void onEat(PlayerItemConsumeEvent e) {
		int stamina = Integer
				.parseInt(ChatColor.stripColor(e.getItem().getItemMeta().getLore().get(0)).replaceAll("[^0-9.]", ""));
		if (stamina != 0) {
			Stats.setStamina(e.getPlayer(), Stats.staminaLevels.get(e.getPlayer().getUniqueId().toString()) + stamina);
			Text.showActionBar(e.getPlayer(), "{\"text\":\"�3Stamina: " + (stamina > 0 ? "�a+" : "�c-")
					+ NumberFormat.getNumberInstance(Locale.US).format(stamina) + " �7("
					+ NumberFormat.getNumberInstance(Locale.US)
							.format(Stats.staminaLevels.get(e.getPlayer().getUniqueId().toString()))
					+ "/" + NumberFormat.getNumberInstance(Locale.US).format(Stats.getMaxStamina(e.getPlayer()))
					+ ")\"}");
		}
	}
}
