package com.stupidbot.universalcorep.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.permissions.Permission;

public class OnCraftItem implements Listener {

	@EventHandler
	public void onCraftItem(CraftItemEvent e) {
		if (!(e.getWhoClicked().hasPermission(new Permission("universalrpgp.throw"))))
			e.setCancelled(true);
	}
}
