package com.stupidbot.universalcorep.listeners;

import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

import java.util.ArrayList;
import java.util.List;

public class OnItemPickup implements Listener {
	public static final List<Entity> stopPickupList = new ArrayList<>();

	@EventHandler
	public void onItemPickup(PlayerPickupItemEvent e) {
		if (stopPickupList.contains(e.getItem()))
			e.setCancelled(true);
	}
}
