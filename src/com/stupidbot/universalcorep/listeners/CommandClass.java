package com.stupidbot.universalcorep.listeners;

import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.custom_mobs.mobs.Orc;
import com.stupidbot.universalcorep.utils.*;

import net.md_5.bungee.api.ChatColor;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.permissions.Permission;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class CommandClass implements CommandExecutor {
	private static final ItemStack ourPlans = Text.newBook("Our Plans", "StupidBot",
			"[{\"text\":\"     OUR PLANS\n\n\n\",\"color\":\"dark_blue\",\"bold\":\"true\"},{\"text\":\"We have HUGE plans for the cells, these won't be ordinary places to store your things and setup shop, 'cells' are more of a blanket term!\n\n\n\n     \",\"color\":\"black\",\"bold\":\"false\"},{\"text\":\"LEARN MORE\",\"color\":\"dark_green\",\"bold\":\"true\",\"underlined\":\"true\",\"clickEvent\":{\"action\":\"open_url\",\"value\":\"https://copperuniverse.enjin.com/forums/m/41521859/viewthread/31033574-what-are-cells-all-about\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"https://copperuniverse.enjin.com/forums/m/41521859/viewthread/31033574-what-are-cells-all-about\",\"color\":\"yellow\"}]}}}]");
	private static final Map<String, Integer> tipCooldown = new HashMap<>();

	@Override
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("spaceship") && sender instanceof Player) {
			Player player = (Player) sender;
			if (sender.hasPermission(new Permission("universalrpgp.ship.gui")))
				if (!(Ships.shipWait.containsKey(player.getUniqueId().toString())))
					if (!(LootEggs.hatchEggTimer.containsKey(player.getUniqueId().toString())))
						if (sender.isOp())
							new BukkitRunnable() {
								@Override
								public void run() { // Running in different
													// thread
													// so bob works correctly
									Inventories.generateGUI(player, "spaceship0");
								}
							}.runTaskLater(Main.plugin, 0);
						else
							Inventories.generateGUI(player, "spaceship0");
					else
						player.sendMessage("�cFinish hatching your loot egg(s) first!");
				else
					sender.sendMessage("�cWait until you reach your destination!");
			else
				sender.sendMessage("�cTalk to �b�oBob�c to fast-travel!");

		} else if (cmd.getName().equalsIgnoreCase("stats") && sender instanceof Player) {
			Player player = (Player) sender;
			Inventories.generateGUI(player, "stats0");
		} else if (cmd.getName().equalsIgnoreCase("hatchlootegg") && sender instanceof Player) {
			Player player = (Player) sender;
			if (sender.hasPermission(new Permission("universalrpgp.p2w.universe")))
				if (!(Ships.shipWait.containsKey(player.getUniqueId().toString())))
					if (!(LootEggs.hatchEggTimer.containsKey(player.getUniqueId().toString())))
						LootEggs.hatchLootEgg(player);
					else
						sender.sendMessage("�cYou're already in the loot egg hatching menu!");
				else
					sender.sendMessage("�cWait until you reach your destination!");
			else
				player.sendMessage("�cRequires �5UNIVERSE�c rank to access remotely, Talk to �b�oElizabeth�c!");
		} else if (cmd.getName().equalsIgnoreCase("givelootegg")) {
			if (sender.hasPermission(new Permission("universalrpgp.givelooteggs")))
				if (args.length > 1)
					try {
						File config = Configs.getPlayerFile(Bukkit.getPlayer(args[1]), "Player Data");
						FileConfiguration configLoaded = Configs.loadFile(config);
						configLoaded.set("LootEggs.Amount",
								configLoaded.getInt("LootEggs.Amount") + Integer.parseInt(args[0]));
						try {
							configLoaded.save(config);
						} catch (IOException e) {
							e.printStackTrace();
						}
						sender.sendMessage(
								"�aGave �6" + args[0] + " �aLoot Eggs to �6" + Bukkit.getPlayer(args[1]).getName());
						Text.showActionBar(Bukkit.getPlayer(args[1]), "{\"text\":\"�6Loot Egg �a+"
								+ NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(args[0])) + "\"}");
					} catch (NumberFormatException e) {
						sender.sendMessage("�6" + args[0] + " �cisn't a number!");
					}
				else
					sender.sendMessage("�cIncorrect usage. /givelootegg <amount> <player>");
			else
				sender.sendMessage("�cYou don't have permission to use this command.");
		} else if (cmd.getName().equalsIgnoreCase("givealllootegg")) {
			if (sender.hasPermission(new Permission("universalrpgp.givelooteggs")))
				if (args.length > 0)
					try {
						for (Player all : Bukkit.getOnlinePlayers()) {
							File config = Configs.getPlayerFile(Bukkit.getPlayer(args[1]), "Player Data");
							FileConfiguration configLoaded = Configs.loadFile(config);
							configLoaded.set("LootEggs.Amount",
									configLoaded.getInt("LootEggs.Amount") + Integer.parseInt(args[0]));
							try {
								configLoaded.save(config);
							} catch (IOException e) {
								e.printStackTrace();
							}
							Text.showActionBar(all, "{\"text\":\"�6Loot Egg �a+"
									+ NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(args[0]))
									+ "\"}");
						}
						sender.sendMessage("�aGave �6" + args[0] + " �aLoot Eggs to �6Everyone");
					} catch (NumberFormatException e) {
						sender.sendMessage("�6" + args[0] + " �cisn't a number!");
					}
				else
					sender.sendMessage("�cIncorrect usage. /givealllootegg <amount>");
			else
				sender.sendMessage("�cYou don't have permission to use this command.");
		} else if (cmd.getName().equalsIgnoreCase("cosmetics") && sender instanceof Player) {
			Player player = (Player) sender;
			player.openInventory(Inventories.cosmetics0);
		} else if (cmd.getName().equalsIgnoreCase("ban"))
			if (sender.hasPermission(new Permission("punishgui.use")))
				if (args.length > 1)
					if (sender instanceof Player) {
						Player player = (Player) sender;

						if (Bukkit.getServer().getOfflinePlayer(args[0]).hasPlayedBefore()
								|| Bukkit.getServer().getPlayer(args[0]) != null) {
							OfflinePlayer punished = Bukkit.getServer().getOfflinePlayer(args[0]);
							if (!(Main.perms.playerHas("world", punished, "punishgui.use"))) {
								StringBuilder reason = new StringBuilder();
								for (int i = 1; i < args.length; i++) {
									if (!Objects.equals(reason.toString(), ""))
										reason.append(" ");
									reason.append(args[i]);
								}
								try {
									Inventories.generateBanGUI(player, punished, reason.toString().replaceAll("#", ""));
								} catch (ParseException e) {
									e.printStackTrace();
								}
							} else
								player.sendMessage("�cYou can't punish this player!");
						} else
							player.sendMessage("�cThis player has never played on the server!");
					} else
						sender.sendMessage("�cTo avoid abuse this command must be run from in-game!");
				else
					sender.sendMessage("�cIncorrect usage! /ban <player> <reason>");
			else
				sender.sendMessage("�cYou don't have permission to use this command.");
		else if (cmd.getName().equalsIgnoreCase("shop") && sender instanceof Player) {
			if (sender.hasPermission(new Permission("universalrpgp.throw")))
				if (args.length > 0)
					Inventories.generateShopGUI(args[0], ((Player) sender), Double.parseDouble(args[1]));
				else
					sender.sendMessage("�cIncorrect usage. /shop <type> <multiplier>");
			else
				sender.sendMessage("�cYou don't have permission to use this command.");
		} else if (cmd.getName().equalsIgnoreCase("updateleaderboard"))
			if (sender.hasPermission(new Permission("universalrpgp.updateleaderboard"))) {
				try {
					Leaderboards.updateLeaderboards();
				} catch (IOException e) {
					e.printStackTrace();
				}
				if (sender instanceof Player)
					Bukkit.broadcastMessage(Main.chat.getPlayerPrefix((Player) sender).replaceAll("&", "�")
							+ sender.getName() + " �a�lUpdated The Leaderboards");
				else
					Bukkit.broadcastMessage("�cConsole �a�lUpdated The Leaderboards");
			} else
				sender.sendMessage("�cYou don't have permission to use this command.");
		else if (cmd.getName().equalsIgnoreCase("throw"))
			if (sender.hasPermission(new Permission("universalrpgp.throw")))
				if (args.length > 0)
					if (Bukkit.getPlayer(args[0]) != null) {
						Bukkit.getPlayer(args[0])
								.setVelocity(new Vector(ThreadLocalRandom.current().nextDouble(-10, 10), 5,
										ThreadLocalRandom.current().nextDouble(-10, 10)));
						Bukkit.getPlayer(args[0]).sendMessage("�aYou have been thrown!");
					} else
						sender.sendMessage("�cPlayer cannot be found!");
				else
					sender.sendMessage("�cIncorrect usage! /throw <player>");
			else
				sender.sendMessage("�cYou don't have permission to use this command.");
		else if (cmd.getName().equalsIgnoreCase("throwall"))
			if (sender.hasPermission(new Permission("universalrpgp.throw"))) {
				for (Player all : Bukkit.getOnlinePlayers())
					all.setVelocity(new Vector(ThreadLocalRandom.current().nextDouble(-10, 10), 5,
							ThreadLocalRandom.current().nextDouble(-10, 10)));
				Bukkit.broadcastMessage("�aEveryone has been thrown!");
			} else
				sender.sendMessage("�cYou don't have permission to use this command.");
		else if (cmd.getName().equalsIgnoreCase("boost"))
			if (sender.hasPermission(new Permission("universalrpgp.throw")))
				if (args.length > 0)
					if (Bukkit.getPlayer(args[0]) != null) {
						Bukkit.getPlayer(args[0])
								.setVelocity(new Vector(Bukkit.getPlayer(args[0]).getLocation().getDirection().getX(),
										0.1, Bukkit.getPlayer(args[0]).getLocation().getDirection().getZ()).normalize()
												.multiply(10));
						Bukkit.getPlayer(args[0]).sendMessage("�aYou have been boosted!");
					} else
						sender.sendMessage("�cPlayer cannot be found!");
				else
					sender.sendMessage("�cIncorrect usage! /boost <player>");
			else
				sender.sendMessage("�cYou don't have permission to use this command.");
		else if (cmd.getName().equalsIgnoreCase("kaboom"))
			if (sender.hasPermission(new Permission("universalrpgp.throw")))
				if (args.length > 0)
					if (Bukkit.getPlayer(args[0]) != null) {
						Bukkit.getPlayer(args[0]).getLocation().getWorld()
								.strikeLightningEffect(Bukkit.getPlayer(args[0]).getLocation());
						Bukkit.getPlayer(args[0]).setVelocity(new Vector(0, 5, 0));
						Bukkit.getPlayer(args[0]).sendMessage("�aYou have been KABOOMED!");
					} else
						sender.sendMessage("�cPlayer cannot be found!");
				else
					sender.sendMessage("�cIncorrect usage! /kaboom <player>");
			else
				sender.sendMessage("�cYou don't have permission to use this command.");
		else if (cmd.getName().equalsIgnoreCase("kaboomall"))
			if (sender.hasPermission(new Permission("universalrpgp.throw"))) {
				for (Player all : Bukkit.getOnlinePlayers()) {
					all.getLocation().getWorld().strikeLightningEffect(all.getLocation());
					all.setVelocity(new Vector(0, 5, 0));
				}
				Bukkit.broadcastMessage("�aEveryone has been KABOOMED!");
			} else
				sender.sendMessage("�cYou don't have permission to use this command.");
		else if (cmd.getName().equalsIgnoreCase("ping"))
			if (args.length > 0)
				if (Bukkit.getPlayer(args[0]) != null)
					sender.sendMessage(StringUtils.substring(Main.chat.getPlayerPrefix(Bukkit.getPlayer(args[0])), 0, 2)
							.replaceAll("&", "�") + Bukkit.getPlayer(args[0]).getName() + "'s �eping: �d"
							+ NumberFormat.getNumberInstance(Locale.US)
									.format(((CraftPlayer) Bukkit.getPlayer(args[0])).getHandle().ping)
							+ "ms");
				else
					sender.sendMessage("�cPlayer cannot be found!");
			else
				sender.sendMessage("�cIncorrect usage! /ping <player>");
		else if (cmd.getName().equalsIgnoreCase("xp"))
			if (sender.hasPermission(new Permission("universalrpgp.throw")))
				if (args.length > 1)
					if (Bukkit.getPlayer(args[0]) != null) {
						try {
							Stats.giveEXP(Bukkit.getPlayer(args[0]), Integer.parseInt(args[1]), true);
							sender.sendMessage(
									"�aGave �6" + Bukkit.getPlayer(args[0]).getName() + " �b" + args[1] + " exp");
							Text.showActionBar(Bukkit.getPlayer(args[0]),
									"{\"text\":\"�bEXP: �a+"
											+ NumberFormat.getInstance(Locale.US)
													.format(Integer.parseInt(args[1]) * Double
															.parseDouble(Stats.globalXPMultiplier.split("\\:")[1]))
											+ "\"}");
						} catch (NumberFormatException e) {
							sender.sendMessage("�6" + args[1] + " �cisn't a number!");
						} catch (IOException e) {
							e.printStackTrace();
						}
					} else
						sender.sendMessage("�cPlayer cannot be found!");
				else
					sender.sendMessage("�cIncorrect usage! /xp <player> <amount>");
			else
				sender.sendMessage("�cYou don't have permission to use this command.");
		else if (cmd.getName().equalsIgnoreCase("deliverybot"))
			if (sender instanceof Player)
				Inventories.generateGUI((Player) sender, "dailyrewards0");
			else
				sender.sendMessage("�cA player must run this command!");
		else if (cmd.getName().equalsIgnoreCase("redeemreferral")) {
			if (sender instanceof Player) {
				FileConfiguration config = Configs.loadPlayerFile((Player) sender, "Player Data");
				Boolean beenReferred = false;

				if (config.getStringList("Codes.Used") != null)
					for (String used : config.getStringList("Codes.Used"))
						if (used.contains("REF-")) {
							beenReferred = true;
							break;
						}
				if (!(beenReferred))
					if (args.length == 0)
						new SignGUI(Main.plugin).open((Player) sender,
								new String[] { "REF-????-????", "�e^^^^^^^^^^^^^^^", "�fEnter the", "�freferral code" },
								(player, lines) -> player
										.performCommand("redeemreferral " + lines[0].replaceAll("\"", "")));
					else if (args[0].length() == 13) {
						if (!(config.getString("Codes.Referral.Code").contains(args[0]))) {
							if (new File(Main.plugin.getDataFolder() + File.separator + "Player Data").exists()
									&& Objects.requireNonNull(Objects.requireNonNull(
											new File(Main.plugin.getDataFolder() + File.separator + "Player Data")
													.list())).length > 0)
								try {
									Path referralerB4Finder = Files
											.list(new File(Main.plugin.getDataFolder() + File.separator + "Player Data")
													.toPath())
											.filter(path -> Configs.loadFile(path.toFile())
													.getString("Codes.Referral.Code").contains(args[0]))
											.findFirst().orElse(null);
									if (referralerB4Finder != null) {
										File referralerB4 = referralerB4Finder.toFile();
										FileConfiguration referraler = Configs.loadFile(referralerB4);
										config.set("LootEggs.Amount", config.getInt("LootEggs.Amount") + 1);
										List<String> list = new ArrayList<>();
										list.add(args[0]);
										if (config.getStringList("Codes.Used") != null)
											list.addAll(config.getStringList("Codes.Used"));
										config.set("Codes.Used", list);
										try {
											config.save(Configs.getPlayerFile((Player) sender, "Player Data"));
										} catch (IOException e) {
											e.printStackTrace();
										}
										referraler.set("Codes.Referral.Referrals",
												referraler.getInt("Codes.Referral.Referrals") + 1);
										referraler.set("LootEggs.Amount", referraler.getInt("LootEggs.Amount") + 1);
										try {
											referraler.save(referralerB4);
										} catch (IOException e) {
											sender.sendMessage("�7" + e.getMessage()
													+ "�cPlease report the error above to an admin.");
										}
										sender.sendMessage("�eYou have just redeemed "
												+ referraler.getString("Name.Prefix").replaceAll("&", "�")
												+ referraler.getString("Name.Name")
												+ "�e's referral code! A Loot Egg has been added to both of your accounts! �aOpen loot eggs to the left of spawn");
										if (Bukkit.getPlayer(config.getName().replace(".yml", "")) != null)
											Bukkit.getPlayer(config.getName().replace(".yml", ""))
													.sendMessage(StringUtils
															.substring(Main.chat.getPlayerPrefix((Player) sender), 0, 2)
															.replaceAll("&", "�") + sender.getName()
															+ " �ehas just redeemed your referral code! �bLoot Eggs: �a+1�e.");
									} else
										sender.sendMessage(
												"�cReferral code not found, did you type it correctly? Referral codes are cAsE-SeNsItIvE!");
								} catch (IOException e1) {
									e1.printStackTrace();
								}
						} else
							sender.sendMessage(
									"�cWe know you're lonely and all, but, you can't use your own referral code!");
					} else
						sender.sendMessage("�cInvalid code! Referral codes look like this: �aREF-aBcD-1234�c.");
				else
					sender.sendMessage("�cYou've already used a referral code!");
			} else
				sender.sendMessage("�cA player must run this command!");
		} else if (cmd.getName().equalsIgnoreCase("nothing")) { // Intentionally
																// does nothing
		} else if (cmd.getName().equalsIgnoreCase("levelup"))
			if (sender.hasPermission(new Permission("universalrpgp.throw"))) {
				if (args.length > 0) {
					if (Bukkit.getPlayer(args[0]) != null) {
						try {
							Stats.giveEXP(Bukkit.getPlayer(args[0]), Configs.loadFile(
									Configs.getPlayerFile(Bukkit.getPlayer(args[0]).getUniqueId(), "Player Data"))
									.getInt("Level.EXPNeeded"), false);
							sender.sendMessage("�aLeveled up �6" + Bukkit.getPlayer(args[0]).getName());
						} catch (IOException e) {
							e.printStackTrace();
						}
					} else
						sender.sendMessage("�6" + args[0] + " �cis not online.");
				} else
					sender.sendMessage("�cIncorrect usage! /levelup <player>");
			} else
				sender.sendMessage("�cYou don't have permission to use this command.");
		else if (cmd.getName().equalsIgnoreCase("ourplans"))
			Text.openBook(ourPlans, (Player) sender);
		else if (cmd.getName().equalsIgnoreCase("setstamina")) {
			if (sender.hasPermission(new Permission("universalrpgp.throw")))
				if (args.length > 1)
					Stats.setStamina(Bukkit.getPlayer(args[1]), Integer.parseInt(args[0]));
				else
					sender.sendMessage("�cIncorrect usage! /setstamina <amount> <player>");
			else
				sender.sendMessage("�cYou don't have permission to use this command.");
		} else if (cmd.getName().equalsIgnoreCase("spectate")) {
			if (sender.hasPermission(new Permission("universalrpgp.throw")))// if
																			// (sender.hasPermission(new
																			// Permission("universalrpgp.p2w.universe")))
				if (Bukkit.getPlayer(args[0]) != null)
					((CraftPlayer) sender).getHandle()
							.setSpectatorTarget(((CraftEntity) Bukkit.getPlayer(args[0])).getHandle());
				else
					sender.sendMessage("�cCannot find player �6" + args[0]);
			else
				sender.sendMessage("�cRequires �5UNIVERSE�c rank to access!");
		} else if (cmd.getName().equalsIgnoreCase("giverankmission"))
			if (sender.hasPermission(new Permission("universalrpgp.throw")))
				if (args.length > 2) {
					Player player = Bukkit.getPlayer(args[0]);
					ItemStack mission = new ItemStack(Material.DRAGON_EGG, Integer.parseInt(args[2]));
					ItemMeta missionMeta = mission.getItemMeta();
					String rank;
					if (args[1].equalsIgnoreCase("planet"))
						rank = "�aPLANET";
					else if (args[1].equalsIgnoreCase("galaxy"))
						rank = "�bGALAXY";
					else
						rank = "�5UNIVERSE";
					missionMeta.setDisplayName(rank + " �cIngot Mission");
					ArrayList<String> missionLore = new ArrayList<>();
					missionLore.add("�7Think you got what it takes");
					missionLore.add("�7to become a " + rank + "�7?");
					missionLore.add(" ");
					missionLore.add("�d�oPlace this anywhere on Droria 632 then");
					missionLore.add("�d�oyou get a rank voucher, that's all!");
					missionLore.add(" ");
					missionLore.add("�eUnlocked by "
							+ StringUtils.substring(Main.chat.getPlayerPrefix(player).replaceAll("&", "�"), 0, 2)
							+ player.getName() + "�e!");
					missionMeta.setLore(missionLore);
					missionMeta.addEnchant(Enchantment.ARROW_DAMAGE, 0, true);
					missionMeta.addItemFlags(ItemFlag.values());
					mission.setItemMeta(missionMeta);

					if (player.getInventory().firstEmpty() != -1)
						player.getInventory().addItem(mission);
					else
						player.getWorld().dropItemNaturally(player.getLocation(), mission);
				} else
					sender.sendMessage("�cIncorrect usage! /giverankmission <player> <type> <amount>");
			else
				sender.sendMessage("�cYou don't have permission to use this command.");
		else if (cmd.getName().equalsIgnoreCase("givevoucher"))
			if (sender.hasPermission(new Permission("universalrpgp.throw")))
				if (args.length > 2) {
					Player player = Bukkit.getPlayer(args[0]);
					ItemStack voucher = new ItemStack(Material.PAPER, Integer.parseInt(args[2]));
					ItemMeta voucherMeta = voucher.getItemMeta();
					String rank;
					if (args[1].equalsIgnoreCase("planet"))
						rank = "�aPLANET";
					else if (args[1].equalsIgnoreCase("galaxy"))
						rank = "�bGALAXY";
					else
						rank = "�5UNIVERSE";
					voucherMeta.setDisplayName(rank + " �cVoucher");
					ArrayList<String> voucherLore = new ArrayList<>();
					voucherLore.add("�7Right-click to become a " + rank + "�7!");
					voucherLore.add(" ");
					voucherLore.add("�eUnlocked by "
							+ StringUtils.substring(Main.chat.getPlayerPrefix(player).replaceAll("&", "�"), 0, 2)
							+ player.getName() + "�e!");
					voucherMeta.setLore(voucherLore);
					voucherMeta.addEnchant(Enchantment.ARROW_DAMAGE, 0, true);
					voucherMeta.addItemFlags(ItemFlag.values());
					voucher.setItemMeta(voucherMeta);

					if (player.getInventory().firstEmpty() != -1)
						player.getInventory().addItem(voucher);
					else
						player.getWorld().dropItemNaturally(player.getLocation(), voucher);
				} else
					sender.sendMessage("�cIncorrect usage! /givevoucher <player> <type> <amount>");
			else
				sender.sendMessage("�cYou don't have permission to use this command.");
		else if (cmd.getName().equalsIgnoreCase("pv"))
			if (sender.hasPermission(new Permission("universalrpgp.pv")))
				Inventories.generateGUI((Player) sender, "pvs0");
			else
				sender.sendMessage("�cRequires �aPLANET�c rank to access!");
		else if (cmd.getName().equalsIgnoreCase("givebooster")) {
			if (sender.hasPermission(new Permission("universalrpgp.throw")))
				if (args.length > 3) {
					Player player = Bukkit.getPlayer(args[0]);
					ItemStack voucher = new ItemStack(Material.BREWING_STAND_ITEM, Integer.parseInt(args[4]));
					ItemMeta voucherMeta = voucher.getItemMeta();
					String rank;
					if (args[1].equalsIgnoreCase("experience"))
						rank = "Experience";
					else
						rank = "Sell Prices";
					voucherMeta
							.setDisplayName("�9" + args[2] + " min �ex" + args[3] + " �a" + rank + " �cServer Booster");
					ArrayList<String> voucherLore = new ArrayList<>();
					voucherLore.add("�7Right-click to activate a �cServer Booster�7!");
					voucherLore.add(" ");
					voucherLore.add("�7Type: �a" + rank);
					voucherLore.add("�7Time: �9" + args[2] + " min");
					voucherLore.add("�7Multiplier: �ex" + args[3]);
					voucherLore.add(" ");
					voucherLore.add("�eUnlocked by "
							+ StringUtils.substring(Main.chat.getPlayerPrefix(player).replaceAll("&", "�"), 0, 2)
							+ player.getName() + "�e!");
					voucherMeta.setLore(voucherLore);
					voucherMeta.addEnchant(Enchantment.ARROW_DAMAGE, 0, true);
					voucherMeta.addItemFlags(ItemFlag.values());
					voucher.setItemMeta(voucherMeta);

					if (player.getInventory().firstEmpty() != -1)
						player.getInventory().addItem(voucher);
					else
						player.getWorld().dropItemNaturally(player.getLocation(), voucher);
				} else
					sender.sendMessage(
							"�cIncorrect usage! /givebooster <player> <type> <time in minutes> <multiplier> <amount>");
			else
				sender.sendMessage("�cYou don't have permission to use this command.");
		}
		if (cmd.getName().equalsIgnoreCase("giveitemnametag")) {
			if (sender.hasPermission(new Permission("universalrpgp.throw")))
				if (args.length > 1) {
					Player player = Bukkit.getPlayer(args[0]);
					ItemStack voucher = new ItemStack(Material.NAME_TAG, Integer.parseInt(args[1]));
					ItemMeta voucherMeta = voucher.getItemMeta();
					voucherMeta.setDisplayName("�eItem Nametag");
					ArrayList<String> voucherLore = new ArrayList<>();
					voucherLore.add("�7Drag and drop onto an item to rename it,");
					voucherLore.add("�7supports �d�lC�a�lO�b�lL�4�lO�2�lR�6�lS �7(use & symbol)");
					voucherLore.add(" ");
					voucherLore.add("�eUnlocked by "
							+ StringUtils.substring(Main.chat.getPlayerPrefix(player).replaceAll("&", "�"), 0, 2)
							+ player.getName() + "�e!");
					voucherMeta.setLore(voucherLore);
					voucherMeta.addEnchant(Enchantment.ARROW_DAMAGE, 0, true);
					voucherMeta.addItemFlags(ItemFlag.values());
					voucher.setItemMeta(voucherMeta);

					if (player.getInventory().firstEmpty() != -1)
						player.getInventory().addItem(voucher);
					else
						player.getWorld().dropItemNaturally(player.getLocation(), voucher);
				} else
					sender.sendMessage("�cIncorrect usage! /giveitemnametag <player> <amount>");
			else
				sender.sendMessage("�cYou don't have permission to use this command.");
		} else if (cmd.getName().equalsIgnoreCase("tip")) {
			if (args.length > 0)
				if (Bukkit.getPlayer(args[0]) != sender)
					if (Bukkit.getPlayer(args[0]) != null)
						if (!(tipCooldown.containsKey(((Player) sender).getUniqueId().toString()))) {
							tipCooldown.put(((Player) sender).getUniqueId().toString(), 60);
							try {
								Stats.giveEXP((Player) sender, 15, false);
								sender.sendMessage("�a+15 �bEXP");
								Stats.giveEXP(Bukkit.getPlayer(args[0]), 45, false);
								Bukkit.getPlayer(args[0])
										.sendMessage("�a+45 �bEXP �e(" + StringUtils
												.substring(Main.chat.getPlayerPrefix(((Player) sender)), 0, 2)
												.replaceAll("&", "�") + sender.getName() + "'s �3tip�e)");
							} catch (IOException e) {
								e.printStackTrace();
							}
							new BukkitRunnable() {
								@Override
								public void run() {
									tipCooldown.put(((Player) sender).getUniqueId().toString(),
											tipCooldown.get(((Player) sender).getUniqueId().toString()) - 1);
									if (tipCooldown.get(((Player) sender).getUniqueId().toString()) == 0) {
										tipCooldown.remove(((Player) sender).getUniqueId().toString());
										cancel();
									}
								}
							}.runTaskTimerAsynchronously(Main.plugin, 1200, 1200);
						} else
							sender.sendMessage("�cCommand on cooldown! try again in �6"
									+ tipCooldown.get(((Player) sender).getUniqueId().toString()) + "m�c!");
					else
						sender.sendMessage("�cThis is not an online player.");
				else
					sender.sendMessage("�cYou can not tip yourself...");
			else
				sender.sendMessage("�cIncorrect usage! /tip <player>");
		} else if (cmd.getName().equalsIgnoreCase("universalcorepreload"))
			if (sender.hasPermission(new Permission("universalrpgp.throw"))) {
				Main.plugin.reloadConfig();
				sender.sendMessage("�aConfig reloaded!");
				if (Stats.globalXPMultiplier.split("\\:")[0].contains("server"))
					Stats.globalXPMultiplier = "server:"
							+ Main.plugin.getConfig().getInt("Boosters.DefaultMultiplier.Experience");
				if (Inventories.globalShopMultiplier.split("\\:")[0].contains("server"))
					Inventories.globalShopMultiplier = "server:"
							+ Main.plugin.getConfig().getInt("Boosters.DefaultMultiplier.SellPrices");
			} else
				sender.sendMessage("�cYou don't have permission to use this command.");
		else if (cmd.getName().equalsIgnoreCase("spawncustommob"))
			if (sender.hasPermission(new Permission("universalrpgp.throw")))
				if (args.length > 0)
					if (args.length > 1)
						if (args[0].equalsIgnoreCase("orc")) {
							Orc orc = new Orc(((CraftWorld) ((Player) sender).getLocation().getWorld()).getHandle());
							orc.spawn(((Player) sender).getLocation(), Integer.parseInt(args[1]));
						} else
							sender.sendMessage("�cIncorrect args! Valid Args: \"ORC\"");
					else if (args[0].equalsIgnoreCase("orc")) {
						Orc orc = new Orc(((CraftWorld) ((Player) sender).getLocation().getWorld()).getHandle());
						orc.spawn(((Player) sender).getLocation());
					} else
						sender.sendMessage("�cIncorrect args! Valid Args: \"ORC\"");
				else
					sender.sendMessage("�cIncorrect usage! /spawncustommob <type> [level]");
			else
				sender.sendMessage("�cYou don't have permission to use this command.");
		else if (cmd.getName().equalsIgnoreCase("givelootchest"))
			if (sender.hasPermission(new Permission("universalrpgp.throw")))
				if (args.length > 2) {
					Player player = Bukkit.getPlayer(args[0]);
					ItemStack voucher = new ItemStack(Material.SKULL_ITEM, Integer.parseInt(args[2]), (byte) 3);
					SkullMeta voucherMeta = (SkullMeta) voucher.getItemMeta();
					String rank;
					if (args[1].equalsIgnoreCase("common")) {
						rank = "�aCOMMON";
						MiscellaneousUtils.setNonPlayerSkullOwner(voucherMeta,
								"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/fb1fd549a8d2d99220a747598c45d98c61def4534c70ecfcab50e49f9dee8dc3\"}}}");
					} else if (args[1].equalsIgnoreCase("rare")) {
						rank = "�5RARE";
						MiscellaneousUtils.setNonPlayerSkullOwner(voucherMeta,
								"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/2ec6d6251898355b396a7b8139980146c84e178a1967684870a1f0fb6abea2\"}}}");
					} else if (args[1].equalsIgnoreCase("legendary")) {
						rank = "�6LEGENDARY";
						MiscellaneousUtils.setNonPlayerSkullOwner(voucherMeta,
								"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/67626e587956b4be01a925e848350eaba7f324ff54c281c9aa4ae2029d2f382\"}}}");
					} else {
						rank = "�cGODLY";
						MiscellaneousUtils.setNonPlayerSkullOwner(voucherMeta,
								"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/9c96be7886eb7df75525a363e5f549626c21388f0fda988a6e8bf487a53\"}}}");
					}
					voucherMeta.setDisplayName(rank + " �eLoot Chest");
					ArrayList<String> voucherLore = new ArrayList<>();
					voucherLore.add("�7Place on ground to open");
					voucherLore.add("�7this " + rank + " �7Loot Chest!");
					voucherLore.add(" ");
					voucherLore.add("�eUnlocked by "
							+ StringUtils.substring(Main.chat.getPlayerPrefix(player).replaceAll("&", "�"), 0, 2)
							+ player.getName() + "�e!");
					voucherMeta.setLore(voucherLore);
					voucher.setItemMeta(voucherMeta);

					if (player.getInventory().firstEmpty() != -1)
						player.getInventory().addItem(voucher);
					else
						player.getWorld().dropItemNaturally(player.getLocation(), voucher);
				} else
					sender.sendMessage("�cIncorrect usage! /givelootchest <player> <type> <amount>");
			else
				sender.sendMessage("�cYou don't have permission to use this command.");
		else if (cmd.getName().equalsIgnoreCase("kit"))
			sender.sendMessage(
					"�aWe don't have kits, this is custom MMO RPG Prisons. Mine with your hand until you can afford a pickaxe!");
		else if (cmd.getName().equalsIgnoreCase("rankup"))
			sender.sendMessage(
					"�aThere are no \"A-Z\" type of ranks, only levels, you can go to any mine at any time, we do sell donation ranks at �e�nhttp://copperuniverse.enjin.com/shop�a!");
		else if (cmd.getName().equalsIgnoreCase("testfishprize") && sender instanceof Player)
			if (sender.hasPermission(new Permission("universalrpgp.throw"))) {
				Player player = (Player) sender;
				ItemStack item = player.getItemInHand();

				if (item.getType() == Material.FISHING_ROD)
					player.getInventory().addItem(OnFish.getFishPrize(player));
				else
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
							"&cHold the fishing rod you want to run the calculations on."));
			} else
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
						"&cYou don't have permission to use this command."));
		return true;
	}
}