package com.stupidbot.universalcorep.listeners;

import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.utils.Configs;
import com.stupidbot.universalcorep.utils.Stats;
import com.stupidbot.universalcorep.utils.Text;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand.EnumClientCommand;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.IOException;
import java.util.HashMap;

public class OnPlayerDeath implements Listener {
	private static final HashMap<String, String> deathMessageChoose = new HashMap<>();
	static {
		deathMessageChoose.put("default", "%killer%�e killed %dead%");
		deathMessageChoose.put("poked", "%killer%�e poked %dead%�e too hard");
		deathMessageChoose.put("bedwars", "%killer%�e destroyed %dead%�e's bed");
		deathMessageChoose.put("burnt_bacon", "%killer%�e burnt all of %dead%�e's bacon");
		deathMessageChoose.put("false_imprisonment", "%killer%�e arrested %dead%");
		deathMessageChoose.put("educated", "%killer%�e educated %dead%");
		deathMessageChoose.put("yandere", "%dead%�e fell in love with someone else, so %killer%�e killed them");
		deathMessageChoose.put("rekt", "%killer%�e rekt %dead%");
		deathMessageChoose.put("abuse", "%killer%�e abused %dead%");
		deathMessageChoose.put("broken", "%killer%�e broke %dead%");
		deathMessageChoose.put("headshot", "%killer%�e headshotted %dead%");
		deathMessageChoose.put("determination", "%killer%�e was more DETERMINED than %dead%");
		deathMessageChoose.put("phantom_theif", "%killer%�e changed %dead%�e's heart");
		deathMessageChoose.put("exprosion", "%killer%�e used EXPROSION magic on %dead%");
		deathMessageChoose.put("another_castle", "%killer%�e told %dead%�e their princess was in another castle");
		deathMessageChoose.put("fully-armed_batallion",
				"%killer%�e sent %dead%�e a fully-armed batallion to remind them of their love");
		deathMessageChoose.put("blasting_off", "%killer%�e sent %dead%�e blasting off again... *ding*");
		deathMessageChoose.put("disappointment", "%killer%�e made %dead%�e disappoint their family (more than usual)");
		deathMessageChoose.put("blocked", "%killer%�e blocked %dead%�e on Google+...");
		deathMessageChoose.put("attack_on_minecraft", "�eOn that day, %killer%�e gave %dead%�e a grim reminder...");
		deathMessageChoose.put("modding_a.p.i.", "%killer%�e is Mojang, %dead%�e is The Modding A.P.I.");
		deathMessageChoose.put("murder", "%killer%�e murdered %dead%");
		deathMessageChoose.put("admin_commands", "%killer%�e used /kill on %dead%");
		deathMessageChoose.put("money_solves_all_problems", "%killer%�e threw money at %dead%�e until they died");
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) throws IOException {
		Entity dead = e.getEntity();
		Entity killer = e.getEntity().getKiller();
		if (dead != null) {
			e.setKeepLevel(true);
			new BukkitRunnable() {
				@Override
				public void run() {
					((CraftPlayer) dead).getHandle().playerConnection
							.a(new PacketPlayInClientCommand(EnumClientCommand.PERFORM_RESPAWN));
					Text.showTitle((Player) dead, "{\"text\":\"YOU DIED\",\"bold\":\"true\",\"color\":\"red\"}", 10,
							100, 10);
					if (killer != null)
						Text.showSubtitle((Player) dead,
								"[\"\",{\"text\":\""
										+ StringUtils.substring(
												Main.chat.getPlayerPrefix((Player) killer).replaceAll("&", "�"), 0, 2)
										+ killer.getName() + "\"},{\"text\":\" killed you\",\"color\":\"yellow\"}]",
								10, 100, 10);
				}
			}.runTaskLater(Main.plugin, 1);
			FileConfiguration configDead = Configs.loadPlayerFile((Player) dead, "Player Data");
			configDead.set("Stats.Deaths", configDead.getInt("Stats.Deaths") + 1);
			configDead.save(Configs.getPlayerFile((Player) dead, "Player Data"));
			if (killer != null) {
				FileConfiguration configKiller = Configs.loadPlayerFile((Player) killer, "Player Data");
				configKiller.set("Stats.Kills", configKiller.getInt("Stats.Kills") + 1);
				configKiller.save(Configs.getPlayerFile((Player) killer, "Player Data"));
				Stats.giveEXP((Player) killer, 10 + (configKiller.getInt("Stats.Kills") * 10), true);
				e.setDeathMessage(deathMessageChoose
						.get(Configs.loadPlayerFile((Player) killer, "Player Data").getString("Cosmetics.KillMessage"))
						.replaceAll("%killer%",
								StringUtils.substring(Main.chat.getPlayerPrefix((Player) killer).replaceAll("&", "�"),
										0, 2) + killer.getName() + " �7(�f" + (float) ((Player) killer).getHealth()
										+ " �c?��7)")
						.replaceAll("%dead%",
								StringUtils.substring(Main.chat.getPlayerPrefix((Player) dead).replaceAll("&", "�"), 0,
										2) + dead.getName()));
			} else
				e.setDeathMessage(null);
		}
	}
}