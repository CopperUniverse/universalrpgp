package com.stupidbot.universalcorep.listeners;

import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.utils.Configs;
import com.stupidbot.universalcorep.utils.Inventories;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;

public class OnInventoryClose implements Listener {

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) throws IOException {
		if (e.getInventory().getName().contains("�4Player Vault �9#")) {
			new BukkitRunnable() {
				@Override
				public void run() {
					Inventories.generateGUI((Player) e.getPlayer(), "pvs0");
				}
			}.runTaskLaterAsynchronously(Main.plugin, 0);

			((Player) e.getPlayer()).playSound(e.getPlayer().getLocation(), Sound.CHEST_CLOSE, 1, 1);
			File config = Configs.getPlayerFile((Player) e.getPlayer(), "Player Vaults");
			FileConfiguration configLoaded = Configs.loadFile(config);

			configLoaded.set("Vault." + e.getInventory().getName().replace("�4Player Vault �9#", "") + ".Items",
					e.getInventory().getContents());
			configLoaded.save(config);
		}
	}
}
