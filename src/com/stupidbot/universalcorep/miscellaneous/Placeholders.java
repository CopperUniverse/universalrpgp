package com.stupidbot.universalcorep.miscellaneous;

import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.utils.Configs;
import com.stupidbot.universalcorep.utils.Text;
import me.clip.placeholderapi.external.EZPlaceholderHook;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class Placeholders extends EZPlaceholderHook {
	private static final HashMap<String, String> chatTagChoose = new HashMap<>();
	static {
		chatTagChoose.put("nothing", "");
		// chatTagChoose.put("dab", "�9[<o/] ");
		chatTagChoose.put("#potato451", "�a[#POTATO451] ");
		chatTagChoose.put("grinder", "�c[GRINDER] ");
		chatTagChoose.put("voter", "�c[VOTER] ");
		chatTagChoose.put("slayer", "�c[SLAYER] ");
		chatTagChoose.put("bad", "�c[BAD] ");
		chatTagChoose.put("breaker", "�c[BREAKER] ");
		chatTagChoose.put("advertiser", "�c[ADVERTISER] ");
		chatTagChoose.put("grand", "�6[GRAND] ");
		chatTagChoose.put("p2w", "�d[P2W] ");
		chatTagChoose.put("cool", "�b[COOL] ");
		chatTagChoose.put("e.u.l.a.", "�7[EULA] ");
	}

	public Placeholders() {
		super(Main.plugin, "universalrpgp");
	}

	@Override
	public String onPlaceholderRequest(Player player, String identifier) {
		FileConfiguration config = Configs.loadPlayerFile(player, "Player Data");
		switch (identifier) {
			case "xplevel_formatted":
				if (!(player == null))
					return Text.formatExperience(config.getInt("Level.Level"));
				else
					return "ERROR_PLAYER_IS_NULL";
			case "chatcolor":
				if (!(player == null))
					if (player.getUniqueId().toString().contains("e8584196-baf0-4b9d-8146-2d438cafa99a")) // HaydenPlus'
						// chat
						// color
						return "�a";
					else if (!(Main.chat.getPlayerPrefix(player).replaceAll("&", "�").contains("�7")))
						return "�f";
					else
						return "�7";
				else
					return "ERROR_PLAYER_IS_NULL";
			case "chat_tag":
				if (!(player == null))
					return chatTagChoose.get(config.getString("Cosmetics.ChatTag"));
				else
					return "ERROR_PLAYER_IS_NULL";
			default:
				return null;
		}
	}

}