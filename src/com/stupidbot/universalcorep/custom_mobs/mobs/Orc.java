package com.stupidbot.universalcorep.custom_mobs.mobs;

import com.stupidbot.universalcorep.utils.Text;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

public class Orc extends EntityZombie {
	public Orc(World world) {
		super(world);
	}

	@Override
	public void initAttributes() { // Don't change method name
		super.initAttributes();
		this.getAttributeInstance(GenericAttributes.ATTACK_DAMAGE).setValue(1.5);
		this.getAttributeInstance(GenericAttributes.maxHealth).setValue(15);
		this.setCustomName("§2Orc " + Text.formatExperience(1));
		this.setBaby(false);
		this.setVillager(false);
		this.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(0.25);
		// TODO random gear based on level
	}

	@Override
	protected Item getLoot() { // Don't change method name
		return Items.ROTTEN_FLESH; // TODO Add real loot system
	}

	public void spawn(Location location) {
        spawn(location, 1);
    }

	public void spawn(Location location, int lvl) {
		World mcWorld = ((CraftWorld) location.getWorld()).getHandle();
		final Orc orc = new Orc(mcWorld);

		orc.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
		// ((CraftLivingEntity)
		// orc.getBukkitEntity()).setRemoveWhenFarAway(false);
		// only for bosses
		// this.getAttributeInstance(GenericAttributes.ATTACK_DAMAGE).setValue(1.5);
		orc.getAttributeInstance(GenericAttributes.ATTACK_DAMAGE).setValue(1.5 + (lvl / 20));
		orc.getAttributeInstance(GenericAttributes.maxHealth).setValue(15 + (lvl / 4));
		orc.setCustomName("§2Orc " + Text.formatExperience(lvl));
		mcWorld.addEntity(orc, SpawnReason.CUSTOM);
		orc.getBukkitEntity();
		// Add bukkit runnable if you want particles or something for example
	}
}
