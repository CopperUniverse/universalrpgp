package com.stupidbot.universalcorep.utils;

import com.stupidbot.universalcorep.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.permissions.Permission;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Inventories {
	public static String globalShopMultiplier = "server:"
			+ Main.plugin.getConfig().getInt("Boosters.DefaultMultiplier.SellPrices");
	static ItemStack Selected;
	static ItemStack notSelected;
	static ItemStack notOwned;
	private static final ItemStack notOwnedCosmetic;
	private static final ItemStack ownedCosmetic;
	private static final ItemStack selectedCosmetic;
	public static final Inventory cosmetics0 = Bukkit.createInventory(null, 45, "�dCosmetics");
	private static final String[] KillMessagesStrings = { "Default:�9FREEBIE:�cThis message shouldn't appear",
			"Poked:�aCOMMON:�bFound in Loot Eggs", "BedWars:�aCOMMON:�bFound in Loot Eggs",
			"Burnt Bacon:�aCOMMON:�bFound in Loot Eggs", "False Imprisonment:�aCOMMON:�bFound in Loot Eggs",
			"Educated:�aCOMMON:�bFound in Loot Eggs", "Yandere:�aCOMMON:�bFound in Loot Eggs",
			"Rekt:�aCOMMON:�bFound in Loot Eggs", "ABUSE:�aCOMMON:�bFound in Loot Eggs",
			"Broken:�aCOMMON:�bFound in Loot Eggs", "Headshot:�aCOMMON:�bFound in Loot Eggs",
			"DETERMINATION:�5RARE:�bFound in Loot Eggs", "Phantom Thief:�5RARE:�bFound in Loot Eggs",
			"EXPROSION:�5RARE:�bFound in Loot Eggs", "Another Castle:�5RARE:�bFound in Loot Eggs",
			"Fully-Armed Batallion:�5RARE:�bFound in Loot Eggs", "Blasting Off:�6LEGENDARY:�bFound in Loot Eggs",
			"Disappointment:�6LEGENDARY:�bFound in Loot Eggs", "Blocked:�6LEGENDARY:�bFound in Loot Eggs",
			"Attack On Minecraft:�6LEGENDARY:�bFound in Loot Eggs", "Modding API:�6LEGENDARY:�bFound in Loot Eggs",
			"Murder:�2PAY2WIN:�dRequires �aPLANET �drank", "Admin Commands:�2PAY2WIN:�dRequires �bGALAXY �drank",
			"Money Solves All Problems:�2PAY2WIN:�dRequires �5UNIVERSE �drank" };
	private static final ItemStack[] KillMessagesItemStacks = { new ItemStack(Material.BARRIER, 1), new ItemStack(Material.STICK, 1),
			new ItemStack(Material.BED, 1), new ItemStack(Material.GRILLED_PORK, 1),
			new ItemStack(Material.IRON_FENCE, 1), new ItemStack(Material.APPLE, 1),
			new ItemStack(Material.BOOK_AND_QUILL, 1), new ItemStack(Material.ROTTEN_FLESH, 1),
			new ItemStack(Material.LEASH, 1), new ItemStack(Material.RECORD_11, 1), new ItemStack(Material.BOW, 1),
			new ItemStack(Material.YELLOW_FLOWER, 1), new ItemStack(Material.PUMPKIN, 1),
			new ItemStack(Material.TNT, 1), new ItemStack(Material.RED_MUSHROOM, 1), new ItemStack(Material.BOAT, 1),
			new ItemStack(Material.FIREWORK_CHARGE, 1), new ItemStack(Material.STONE, 1, (byte) 3),
			new ItemStack(Material.OBSIDIAN, 1), new ItemStack(Material.FEATHER, 1), new ItemStack(Material.ANVIL, 1),
			new ItemStack(Material.IRON_SWORD, 1), new ItemStack(Material.COMMAND, 1),
			new ItemStack(Material.EMERALD, 1) };
	private static final String[] ChatTagsStrings = { "Nothing:�9FREEBIE:�cThis message shouldn't appear",
			"#POTATO451:�5RARE:�bFound in Loot Eggs", "Grinder:�6SPECIAL:�9Make it onto the levels leaderboard",
			"Voter:�6SPECIAL:�9Make it onto the votes leaderboard",
			"Slayer:�6SPECIAL:�9Make it onto the kills leaderboard",
			"Bad:�6SPECIAL:�9Make it onto the deaths leaderboard",
			"Breaker:�6SPECIAL:�9Make it onto the blocks leaderboard",
			"Advertiser:�6SPECIAL:�9Make it onto the referrals leaderboard",
			"Grand:�2PAY2WIN �3LIMITED TIME:�9Use our 'Grand Opening' sale (CURRENTLY ACTIVE)",
			"P2W:�2PAY2WIN:�dRequires �aPLANET �drank", "Cool:�2PAY2WIN:�dRequires �bGALAXY �drank",
			"E.U.L.A.:�2PAY2WIN:�dRequires �5UNIVERSE �drank" };
	private static final ItemStack[] ChatTagsItemStacks = { new ItemStack(Material.BARRIER, 1),
			new ItemStack(Material.POTATO_ITEM, 1), new ItemStack(Material.DIAMOND_PICKAXE, 1),
			new ItemStack(Material.ANVIL, 1, (byte) 2), new ItemStack(Material.DIAMOND_SWORD, 1),
			new ItemStack(Material.BONE, 1), new ItemStack(Material.DIAMOND_ORE, 1), new ItemStack(Material.PAPER, 1),
			new ItemStack(Material.CAKE, 1), new ItemStack(Material.EMERALD_BLOCK, 1), new ItemStack(Material.ICE, 1),
			new ItemStack(Material.BEDROCK, 1) };
	private static final String[] ProjectileTrailsStrings = { "Default:�9FREEBIE:�cThis message shouldn't appear",
			"Snow:�aCOMMON:�bFound in Loot Eggs", "Splash:�aCOMMON:�bFound in Loot Eggs",
			"Slimey:�aCOMMON:�bFound in Loot Eggs", "Black Magic:�aCOMMON:�bFound in Loot Eggs",
			"Dirty:�aCOMMON:�bFound in Loot Eggs", "Electrocution:�aCOMMON:�bFound in Loot Eggs",
			"Enchanted:�aCOMMON:�bFound in Loot Eggs", "God's Dust:�5RARE:�bFound in Loot Eggs",
			"Grinded Emerald:�5RARE:�bFound in Loot Eggs", "P.F.U.D.O.R.:�5RARE:�bFound in Loot Eggs",
			"Curse:�5RARE:�bFound in Loot Eggs", "Blood:�5RARE:�bFound in Loot Eggs",
			"Compressed Air:�5RARE:�bFound in Loot Eggs", "Default v2:�5RARE:�bFound in Loot Eggs",
			"Smokey:�5RARE:�bFound in Loot Eggs", "Twinkle:�6LEGENDARY:�bFound in Loot Eggs",
			"Rage:�6LEGENDARY:�bFound in Loot Eggs", "Magma Drop:�6LEGENDARY:�bFound in Loot Eggs",
			"Water Leak:�6LEGENDARY:�bFound in Loot Eggs", "Love:�6LEGENDARY:�bFound in Loot Eggs",
			"Real Talent:�6LEGENDARY:�bFound in Loot Eggs", "Crayon:�6LEGENDARY:�bFound in Loot Eggs",
			"Spicy:�2PAY2WIN:�dRequires �aPLANET �drank", "Bling:�2PAY2WIN:�dRequires �bGALAXY �drank",
			"Chipotle:�2PAY2WIN:�dRequires �5UNIVERSE �drank" };
	private static final ItemStack[] ProjectileTrailsItemStacks = { new ItemStack(Material.BARRIER, 1),
			new ItemStack(Material.SNOW_BALL, 1), new ItemStack(Material.GHAST_TEAR, 1),
			new ItemStack(Material.SLIME_BLOCK, 1), new ItemStack(Material.BREWING_STAND_ITEM, 1),
			new ItemStack(Material.DIRT, 1), new ItemStack(Material.GLOWSTONE_DUST, 1),
			new ItemStack(Material.ENCHANTMENT_TABLE, 1), new ItemStack(Material.SUGAR, 1),
			new ItemStack(Material.EMERALD, 1), new ItemStack(Material.WOOL, 1, (byte) 6),
			new ItemStack(Material.BLAZE_ROD, 1), new ItemStack(Material.REDSTONE_BLOCK, 1),
			new ItemStack(Material.GLASS_BOTTLE, 1), new ItemStack(Material.DIAMOND_AXE, 1),
			new ItemStack(Material.TORCH, 1), new ItemStack(Material.FIREWORK, 1), new ItemStack(Material.DEAD_BUSH, 1),
			new ItemStack(Material.LAVA_BUCKET, 1), new ItemStack(Material.WATER_BUCKET, 1),
			new ItemStack(Material.RED_ROSE, 1), new ItemStack(Material.JUKEBOX, 1),
			new ItemStack(Material.INK_SACK, 1, (byte) 11), new ItemStack(Material.BLAZE_POWDER, 1),
			new ItemStack(Material.GOLD_BLOCK, 1), new ItemStack(Material.INK_SACK, 1, (byte) 1) };
	private static final String[] ShipsStrings = { "Default:�9FREEBIE:�cThis message shouldn't appear",
			"Hipster:�6LEGENDARY:�bFound in Loot Eggs", "Planetary:�2PAY2WIN:�dRequires �aPLANET �drank",
			"Galaxious:�2PAY2WIN:�dRequires �bGALAXY �drank", "Universal:�2PAY2WIN:�dRequires �5UNIVERSE �drank" };
	private static final ItemStack[] ShipsItemStacks = { new ItemStack(Material.BARRIER, 1), new ItemStack(Material.GLASS, 1),
			new ItemStack(Material.STAINED_CLAY, 1, (byte) 5), new ItemStack(Material.ICE, 1),
			new ItemStack(Material.BEACON, 1) };
	private static final Double[] shopPrices = { 0.06, 0.12, 0.24, 0.36, 0.48, 0.62, 0.84, 1.02 };

	static {
		notOwnedCosmetic = new ItemStack(Material.INK_SACK, 1, (short) 8);
		ownedCosmetic = new ItemStack(Material.INK_SACK, 1, (short) 8);
		selectedCosmetic = new ItemStack(Material.INK_SACK, 1, (short) 8);

		ItemMeta notOwnedCosmeticMeta = notOwnedCosmetic.getItemMeta();
		notOwnedCosmeticMeta.setDisplayName("�c%name% %catagory%");
		ArrayList<String> notOwnedCosmeticLore = new ArrayList<>();
		notOwnedCosmeticLore.add("�7Get the %name% %catagory%");
		notOwnedCosmeticLore.add("");
		notOwnedCosmeticLore.add("�7Rarity: %rarity%");
		notOwnedCosmeticLore.add("");
		notOwnedCosmeticLore.add("%howto%");
		notOwnedCosmeticMeta.setLore(notOwnedCosmeticLore);
		notOwnedCosmetic.setItemMeta(notOwnedCosmeticMeta);

		ItemMeta ownedCosmeticMeta = ownedCosmetic.getItemMeta();
		ownedCosmeticMeta.setDisplayName("�e%name% %catagory%");
		ArrayList<String> ownedCosmeticLore = new ArrayList<>();
		ownedCosmeticLore.add("�7Select the %name% %catagory%");
		ownedCosmeticLore.add("");
		ownedCosmeticLore.add("�7Rarity: %rarity%");
		ownedCosmeticLore.add("");
		ownedCosmeticLore.add("�eClick to select");
		ownedCosmeticMeta.setLore(ownedCosmeticLore);
		ownedCosmetic.setItemMeta(ownedCosmeticMeta);

		ItemMeta selectedMeta = selectedCosmetic.getItemMeta();
		selectedMeta.setDisplayName("�a%name% %catagory%");
		ArrayList<String> selectedLore = new ArrayList<>();
		selectedLore.add("�7%catagory%s are purely cosmetic");
		selectedLore.add("");
		selectedLore.add("�7Rarity: %rarity%");
		selectedLore.add("");
		selectedLore.add("�aSELECTED");
		selectedMeta.setLore(selectedLore);
		selectedCosmetic.setItemMeta(selectedMeta);

		ItemStack border = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemMeta borderMeta = border.getItemMeta();
		borderMeta.setDisplayName(" ");
		border.setItemMeta(borderMeta);

		/*
		 * ItemStack jobs = new ItemStack(Material.WOOD_HOE, 1); ItemMeta
		 * jobsMeta = jobs.getItemMeta(); jobsMeta.setDisplayName("�bJobs");
		 * jobsMeta.addItemFlags(ItemFlag.values()); jobs.setItemMeta(jobsMeta);
		 * spaceship2.setItem(15, jobs);
		 */

		for (int i = 0; i < cosmetics0.getSize(); i++)
			cosmetics0.setItem(i, border);

		ItemStack killMessages = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		SkullMeta killMessagesMeta = (SkullMeta) killMessages.getItemMeta();
		MiscellaneousUtils.setNonPlayerSkullOwner(killMessagesMeta,
				"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/e9e750f3f5ad255710cad6ec20f28489f0fb2873f35fb469a8162a6d465e07b\"}}}");
		killMessagesMeta.setDisplayName("�6Kill Messages");
		killMessagesMeta.addItemFlags(ItemFlag.values());
		killMessages.setItemMeta(killMessagesMeta);
		cosmetics0.setItem(12, killMessages);

		ItemStack chatTitle = new ItemStack(Material.NAME_TAG, 1);
		ItemMeta chatTitleMeta = chatTitle.getItemMeta();
		chatTitleMeta.setDisplayName("�6Chat Tags");
		chatTitle.setItemMeta(chatTitleMeta);
		cosmetics0.setItem(14, chatTitle);

		ItemStack projectileTrails = new ItemStack(Material.BOW, 1);
		ItemMeta projectileTrailsMeta = projectileTrails.getItemMeta();
		projectileTrailsMeta.setDisplayName("�6Projectile Trails");
		projectileTrails.setItemMeta(projectileTrailsMeta);
		cosmetics0.setItem(30, projectileTrails);

		ItemStack shipSkin = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		SkullMeta shipSkinMeta = (SkullMeta) shipSkin.getItemMeta();
		MiscellaneousUtils.setNonPlayerSkullOwner(shipSkinMeta,
				"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/e8345eec1bcb3425881ea9ea7157b12f1bd7c646230f8b1c5e5362794cf838a\"}}}");
		shipSkinMeta.setDisplayName("�6Ship Skins");
		shipSkinMeta.addItemFlags(ItemFlag.values());
		shipSkin.setItemMeta(shipSkinMeta);
		cosmetics0.setItem(32, shipSkin);

		ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
		SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
		skullMeta.setDisplayName("�2How To Get Cosmetics");
		MiscellaneousUtils.setNonPlayerSkullOwner(skullMeta,
				"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/e2a850feabb07349cfe245b26a264ea36df73338f84cd2ee3833b185e1e2e2d8\"}}}");
		skull.setItemMeta(skullMeta);
		cosmetics0.setItem(26, skull);
	}

	public static void generateGUI(final Player player, final String inventory) {
		if (Objects.equals(inventory, "pvs0")) {
			Inventory pvs0 = Bukkit.createInventory(null, InventoryType.HOPPER, "�4Player Vaults");
			FileConfiguration config = Configs.loadPlayerFile(player, "Player Data");

			for (int i = 0; i < pvs0.getSize(); i++)
				if (config.getInt("Level.Level") >= i * 25) {
					ItemStack access = new ItemStack(Material.ENDER_CHEST, i + 1);
					ItemMeta accessMeta = access.getItemMeta();
					accessMeta.setDisplayName("�bPlayer Vault �e#" + (i + 1));
					access.setItemMeta(accessMeta);
					pvs0.setItem(i, access);
				} else {
					ItemStack accessFalse = new ItemStack(Material.STAINED_GLASS_PANE, i + 1, (byte) 14);
					ItemMeta accessFalseMeta = accessFalse.getItemMeta();
					accessFalseMeta.setDisplayName("�cPlayer Vault �e#" + (i + 1));
					ArrayList<String> accessFalseLore = new ArrayList<>();
					accessFalseLore.add("�e�lRequired Level: �f�l" + i * 25);
					accessFalseMeta.setLore(accessFalseLore);
					accessFalse.setItemMeta(accessFalseMeta);
					pvs0.setItem(i, accessFalse);
				}
			player.openInventory(pvs0);
		} else if (Objects.equals(inventory, "dailyrewards0")) {
			Inventory dailyrewards0 = Bukkit.createInventory(null, 27, "�5Rewards");

			ItemStack border = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
			ItemMeta borderMeta = border.getItemMeta();
			borderMeta.setDisplayName(" ");
			border.setItemMeta(borderMeta);
			for (int i = 0; i < dailyrewards0.getSize(); i++)
				dailyrewards0.setItem(i, border);

			ItemStack referral = new ItemStack(Material.RED_ROSE, 1);
			ItemMeta referralMeta = referral.getItemMeta();
			referralMeta.setDisplayName("�bReferrals");
			referral.setItemMeta(referralMeta);
			dailyrewards0.setItem(12, referral);

			ItemStack vote = new ItemStack(Material.BOOK_AND_QUILL, 1);
			ItemMeta voteMeta = vote.getItemMeta();
			voteMeta.setDisplayName("�bVote");
			vote.setItemMeta(voteMeta);
			dailyrewards0.setItem(14, vote);

			player.openInventory(dailyrewards0);
			// TODO Finish this
		} else if (Objects.equals(inventory, "stats0")) {
			Inventory stats0 = Bukkit.createInventory(null, 45, "�4Stats");

			ItemStack border = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
			ItemMeta borderMeta = border.getItemMeta();
			borderMeta.setDisplayName(" ");
			border.setItemMeta(borderMeta);
			for (int i = 0; i < stats0.getSize(); i++)
				stats0.setItem(i, border);

			ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
			SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
			skullMeta.setDisplayName(Text.formatExperience(player.getLevel()) + " "
					+ Main.chat.getPlayerPrefix(player).replace("&", "�") + player.getName());
			skullMeta.setOwner(player.getName());
			skull.setItemMeta(skullMeta);
			stats0.setItem(4, skull);

			FileConfiguration config = Configs.loadPlayerFile(player, "Player Data");

			ItemStack knowlege = new ItemStack(Material.BOOK, 1);
			ItemMeta knowlegeMeta = knowlege.getItemMeta();
			knowlegeMeta.setDisplayName("�eKnowledge");
			ArrayList<String> knowlegeLore = new ArrayList<>();
			knowlegeLore.add("�a" + NumberFormat.getNumberInstance(Locale.US).format(config.getInt("Stats.Knowledge")));
			knowlegeLore.add(" ");
			knowlegeLore.add("�7�oIncreases chance of finding good items whilst mining!");
			knowlegeMeta.setLore(knowlegeLore);
			knowlege.setItemMeta(knowlegeMeta);
			stats0.setItem(11, knowlege);

			ItemStack proficiency = new ItemStack(Material.SHEARS, 1);
			ItemMeta proficiencyMeta = proficiency.getItemMeta();
			proficiencyMeta.setDisplayName("�eProficiency");
			ArrayList<String> proficiencyLore = new ArrayList<>();
			proficiencyLore
					.add("�a" + NumberFormat.getNumberInstance(Locale.US).format(config.getInt("Stats.Proficiency")));
			proficiencyLore.add(" ");
			proficiencyLore.add("�7�oDirectly increases stamina!");
			proficiencyMeta.setLore(proficiencyLore);
			proficiency.setItemMeta(proficiencyMeta);
			stats0.setItem(20, proficiency);

			ItemStack guts = new ItemStack(Material.EYE_OF_ENDER, 1);
			ItemMeta gutsMeta = guts.getItemMeta();
			gutsMeta.setDisplayName("�eGuts");
			ArrayList<String> gutsLore = new ArrayList<>();
			gutsLore.add("�a" + NumberFormat.getNumberInstance(Locale.US).format(config.getInt("Stats.Guts")));
			gutsLore.add(" ");
			gutsLore.add("�7�oComing soon (P.V.P. related)!");
			gutsMeta.setLore(gutsLore);
			guts.setItemMeta(gutsMeta);
			stats0.setItem(29, guts);

			player.openInventory(stats0);
		} else if (Objects.equals(inventory, "spaceship0")) {
			Inventory spaceship0 = Bukkit.createInventory(null, 54, "�3Spaceship");
			File config = Configs.getPlayerFile(player, "Player Data");
			FileConfiguration configLoaded = Configs.loadFile(config);

			ItemStack border = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
			ItemMeta borderMeta = border.getItemMeta();
			borderMeta.setDisplayName(" ");
			border.setItemMeta(borderMeta);
			for (int i = 0; i < spaceship0.getSize(); i++)
				spaceship0.setItem(i, border);

			ItemStack shipSkin = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
			SkullMeta shipSkinMeta = (SkullMeta) shipSkin.getItemMeta();
			MiscellaneousUtils.setNonPlayerSkullOwner(shipSkinMeta,
					"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/e8345eec1bcb3425881ea9ea7157b12f1bd7c646230f8b1c5e5362794cf838a\"}}}");
			shipSkinMeta.setDisplayName("�6Ship Skins");
			shipSkin.setItemMeta(shipSkinMeta);
			spaceship0.setItem(49, shipSkin);

			ItemStack warp = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
			SkullMeta warpMeta = (SkullMeta) warp.getItemMeta();
			try {
				if (new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
						.parse(configLoaded.getString("Timers.Rewards.InstaFlightTickets")).before(new Date())
						&& configLoaded.getInt("Ships.InstaFlightTickets") < 1) {
					configLoaded.set("Ships.InstaFlightTickets", 1);
					configLoaded.save(config);
				}
			} catch (ParseException | IOException e1) {
				e1.printStackTrace();
			}
			if (configLoaded.getInt("Ships.InstaFlightTickets") > 0) {
				warpMeta.setDisplayName("�eInstant Flight Tickets: �a" + NumberFormat.getNumberInstance(Locale.US)
						.format(configLoaded.getInt("Ships.InstaFlightTickets")));
				MiscellaneousUtils.setNonPlayerSkullOwner(warpMeta,
						"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/7a2569415c14e31c98ec993a2f99e6d64846db367a13b199965ad99c438c86c\"}}}");
			} else {
				try {
					Date date1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
							.parse(configLoaded.getString("Timers.Rewards.InstaFlightTickets"));
					long millis = date1.getTime() - new Date().getTime();
					warpMeta.setDisplayName("�cFree Instant Flight Ticket in �7"
							+ String.format("%d hr, %d min, %d sec", TimeUnit.MILLISECONDS.toHours(millis),
									TimeUnit.MILLISECONDS.toMinutes(millis)
											- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
									TimeUnit.MILLISECONDS.toSeconds(millis)
											- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));
					MiscellaneousUtils.setNonPlayerSkullOwner(warpMeta,
							"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/91361e576b493cbfdfae328661cedd1add55fab4e5eb418b92cebf6275f8bb4\"}}}");
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			ArrayList<String> warpLore = new ArrayList<>();
			warpLore.add("�7Instant Flight Tickets are given to you");
			warpLore.add("�7for free every 18 hours and allow you");
			warpLore.add("�7to instantly fly too wherever you want!");
			warpMeta.setLore(warpLore);
			warpMeta.addItemFlags(ItemFlag.values());
			warp.setItemMeta(warpMeta);
			spaceship0.setItem(4, warp);

			if (configLoaded.getInt("Ships.InstaFlightTickets") < 1)
				new BukkitRunnable() {
					@Override
					public void run() {
						Date date2 = new Date();

						try {
							if (new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
									.parse(configLoaded.getString("Timers.Rewards.InstaFlightTickets")).before(date2)
									&& configLoaded.getInt("Ships.InstaFlightTickets") < 1) {
								configLoaded.set("Ships.InstaFlightTickets", 1);
								Calendar cal = Calendar.getInstance();
								cal.setTime(new Date());
								cal.add(Calendar.HOUR, 18);
								configLoaded.set("Timers.Rewards.InstaFlightTickets",
										new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(cal.getTime()));
								configLoaded.save(config);
								cancel();
							}
						} catch (ParseException | IOException e) {
							e.printStackTrace();
						}
						if (configLoaded.getInt("Ships.InstaFlightTickets") > 0) {
							warpMeta.setDisplayName(
									"�eInstant Flight Tickets: �a" + NumberFormat.getNumberInstance(Locale.US)
											.format(configLoaded.getInt("Ships.InstaFlightTickets")));
							MiscellaneousUtils.setNonPlayerSkullOwner(warpMeta,
									"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/7a2569415c14e31c98ec993a2f99e6d64846db367a13b199965ad99c438c86c\"}}}");
						} else {
							try {
								Date date1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
										.parse(configLoaded.getString("Timers.Rewards.InstaFlightTickets"));
								long millis = date1.getTime() - date2.getTime();
								warpMeta.setDisplayName("�cFree Instant Flight in �7" + String.format(
										"%d hr, %d min, %d sec", TimeUnit.MILLISECONDS.toHours(millis),
										TimeUnit.MILLISECONDS.toMinutes(millis)
												- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
										TimeUnit.MILLISECONDS.toSeconds(millis)
												- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));
								Calendar cal = Calendar.getInstance();
								cal.setTime(date2);
								cal.add(Calendar.SECOND, 1);
								date2 = cal.getTime();
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
						warp.setItemMeta(warpMeta);
						spaceship0.setItem(4, warp);

						if (player.getOpenInventory() == null || !Objects.equals(player.getOpenInventory().getTitle(), "�3Spaceship")
								|| !(player.isOnline()))
							cancel();
					}
				}.runTaskTimerAsynchronously(Main.plugin,
						(1000 - Calendar.getInstance().get(Calendar.MILLISECOND)) / 50, 20);

			ItemStack spawn = new ItemStack(Material.BED, 1);
			ItemMeta spawnMeta = spawn.getItemMeta();
			spawnMeta.setDisplayName("�bSpawn �c�lINSTANT FLIGHT");
			spawn.setItemMeta(spawnMeta);
			spaceship0.setItem(9, spawn);

			ItemStack droria = new ItemStack(Material.BRICK, 1);
			ItemMeta droriaMeta = droria.getItemMeta();
			droriaMeta.setDisplayName("�7Droria 632 Spawn");
			droria.setItemMeta(droriaMeta);
			spaceship0.setItem(18, droria);
			ArrayList<String> NoMinePermLore = new ArrayList<>();
			NoMinePermLore.add("�cYou don't have access to fast-travel for this");
			NoMinePermLore.add("�cmine! Find this mine and talk to Bob!");
			if (player.hasPermission(new Permission("ezranks.rank.AIR"))) {
				ItemStack AIR = new ItemStack(Material.CLAY_BRICK, 1);
				ItemMeta AIRMeta = AIR.getItemMeta();
				AIRMeta.setDisplayName("�7AIR Mine");
				AIR.setItemMeta(AIRMeta);
				spaceship0.setItem(27, AIR);
			} else {
				ItemStack AIR = new ItemStack(Material.NETHER_BRICK_ITEM, 1);
				ItemMeta AIRMeta = AIR.getItemMeta();
				AIRMeta.setDisplayName("�cAIR Mine");
				AIRMeta.setLore(NoMinePermLore);
				AIR.setItemMeta(AIRMeta);
				spaceship0.setItem(27, AIR);
			}
			if (player.hasPermission(new Permission("ezranks.rank.DIRT"))) {
				ItemStack DIRT = new ItemStack(Material.CLAY_BRICK, 1);
				ItemMeta DIRTMeta = DIRT.getItemMeta();
				DIRTMeta.setDisplayName("�7DIRT Mine");
				DIRT.setItemMeta(DIRTMeta);
				spaceship0.setItem(36, DIRT);
			} else {
				ItemStack DIRT = new ItemStack(Material.NETHER_BRICK_ITEM, 1);
				ItemMeta DIRTMeta = DIRT.getItemMeta();
				DIRTMeta.setDisplayName("�cDIRT Mine");
				DIRTMeta.setLore(NoMinePermLore);
				DIRT.setItemMeta(DIRTMeta);
				spaceship0.setItem(36, DIRT);
			}
			if (player.hasPermission(new Permission("ezranks.rank.GRASS"))) {
				ItemStack GRASS = new ItemStack(Material.CLAY_BRICK, 1);
				ItemMeta GRASSMeta = GRASS.getItemMeta();
				GRASSMeta.setDisplayName("�7GRASS Mine");
				GRASS.setItemMeta(GRASSMeta);
				spaceship0.setItem(37, GRASS);
			} else {
				ItemStack GRASS = new ItemStack(Material.NETHER_BRICK_ITEM, 1);
				ItemMeta GRASSMeta = GRASS.getItemMeta();
				GRASSMeta.setDisplayName("�cGRASS Mine");
				GRASSMeta.setLore(NoMinePermLore);
				GRASS.setItemMeta(GRASSMeta);
				spaceship0.setItem(37, GRASS);
			}
			if (configLoaded.getInt("Level.Level") >= 50) {
				ItemStack glosie = new ItemStack(Material.BRICK, 1);
				ItemMeta glosieMeta = glosie.getItemMeta();
				glosieMeta.setDisplayName("�7Glosie Spawn");
				glosie.setItemMeta(glosieMeta);
				spaceship0.setItem(29, glosie);
			} else {
				ItemStack glosie = new ItemStack(Material.REDSTONE_BLOCK, 1);
				ItemMeta glosieMeta = glosie.getItemMeta();
				glosieMeta.setDisplayName("�cGlosie Spawn");
				ArrayList<String> noPlanetPermLore = new ArrayList<>();
				noPlanetPermLore.add("�cYou don't have access to fast-travel for this");
				noPlanetPermLore.add("�cplanet! Reach level �f�l50�c!");
				glosieMeta.setLore(noPlanetPermLore);
				glosie.setItemMeta(glosieMeta);
				spaceship0.setItem(29, glosie);
			}
			if (player.hasPermission(new Permission("ezranks.rank.LOG"))) {
				ItemStack LOG = new ItemStack(Material.CLAY_BRICK, 1);
				ItemMeta LOGMeta = LOG.getItemMeta();
				LOGMeta.setDisplayName("�7LOG Mine");
				LOG.setItemMeta(LOGMeta);
				spaceship0.setItem(20, LOG);
			} else {
				ItemStack LOG = new ItemStack(Material.NETHER_BRICK_ITEM, 1);
				ItemMeta LOGMeta = LOG.getItemMeta();
				LOGMeta.setDisplayName("�cLOG Mine");
				LOGMeta.setLore(NoMinePermLore);
				LOG.setItemMeta(LOGMeta);
				spaceship0.setItem(20, LOG);
			}

			ItemStack pvp = new ItemStack(Material.DIAMOND_SWORD, 1);
			ItemMeta pvpMeta = pvp.getItemMeta();
			pvpMeta.setDisplayName("�7Danger Zone �c�lPVP");
			pvpMeta.addItemFlags(ItemFlag.values());
			pvp.setItemMeta(pvpMeta);
			spaceship0.setItem(38, pvp);

			ItemStack soon = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 7);
			ItemMeta soonMeta = soon.getItemMeta();
			soonMeta.setDisplayName("�cComing Soon�");
			soon.setItemMeta(soonMeta);
			spaceship0.setItem(11, soon);
			spaceship0.setItem(12, soon);
			spaceship0.setItem(13, soon);
			spaceship0.setItem(22, soon);
			spaceship0.setItem(31, soon);
			spaceship0.setItem(40, soon);
			spaceship0.setItem(41, soon);
			spaceship0.setItem(42, soon);
			spaceship0.setItem(33, soon);
			spaceship0.setItem(24, soon);
			spaceship0.setItem(15, soon);
			spaceship0.setItem(16, soon);
			spaceship0.setItem(17, soon);
			spaceship0.setItem(26, soon);
			spaceship0.setItem(35, soon);
			spaceship0.setItem(44, soon);

			player.openInventory(spaceship0);
		}
	}

	public static void generateCosmeticGUI(String cosmeticType, Player player, int page) {
		Inventory cosmetic = Bukkit.createInventory(null, 54, "�6" + cosmeticType + "s Page " + page);
		FileConfiguration config = Configs.loadPlayerFile(player, "Player Data");

		ItemStack border = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemMeta borderMeta = border.getItemMeta();
		borderMeta.setDisplayName(" ");
		border.setItemMeta(borderMeta);
		for (int i = 0; i < cosmetic.getSize(); i++)
			cosmetic.setItem(i, border);

		ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
		skullMeta.setDisplayName("�bEggshells: �a"
				+ NumberFormat.getNumberInstance(Locale.US).format(config.getInt("LootEggs.Eggshells")));
		List<String> skullLore = new ArrayList<>();
		skullLore.add("�7Eggshells are gained upon recieving a");
		skullLore.add("�7cosmetic you already own from a Loot Egg,");
		skullLore.add("�7they are used to buy cosmetics directly!");
		skullMeta.setLore(skullLore);
		skullMeta.setOwner(player.getName());
		skull.setItemMeta(skullMeta);
		cosmetic.setItem(40, skull);

		ItemStack back = new ItemStack(Material.PAINTING, 1);
		ItemMeta backMeta = back.getItemMeta();
		backMeta.setDisplayName("�4Back");
		back.setItemMeta(backMeta);
		cosmetic.setItem(49, back);

		String[] stringString = null;
		ItemStack[] itemString = null;
		if (Objects.equals(cosmeticType, "Kill Message")) {
			stringString = KillMessagesStrings;
			itemString = KillMessagesItemStacks;
		} else if (Objects.equals(cosmeticType, "Chat Tag")) {
			stringString = ChatTagsStrings;
			itemString = ChatTagsItemStacks;
		} else if (Objects.equals(cosmeticType, "Projectile Trail")) {
			stringString = ProjectileTrailsStrings;
			itemString = ProjectileTrailsItemStacks;
		} else if (Objects.equals(cosmeticType, "Ship Skin")) {
			stringString = ShipsStrings;
			itemString = ShipsItemStacks;
		}
		if (Objects.requireNonNull(stringString).length > 21 * page) {
			ItemStack next = new ItemStack(Material.ARROW, 1);
			ItemMeta nextMeta = back.getItemMeta();
			nextMeta.setDisplayName("�ePage " + (page + 1));
			next.setItemMeta(nextMeta);
			cosmetic.setItem(50, next);
		}
		if (page > 1) {
			ItemStack back2 = new ItemStack(Material.ARROW, 1);
			ItemMeta back2Meta = back2.getItemMeta();
			back2Meta.setDisplayName("�ePage " + (page - 1));
			back2.setItemMeta(back2Meta);
			cosmetic.setItem(48, back2);
		}
		int step = 21 * (page - 1);
		boolean ignoreSelected = false;
		for (int i = 10; i < 35; i++)
			if (i < 17 || i > 18 && i < 26 || i > 27) {
				if (step < stringString.length) {
					String[] stringSplit = stringString[step].split("\\:");
					ItemStack item;
					if (!(player.hasPermission(
							new Permission("universalrpgp." + cosmeticType.toLowerCase().replaceAll(" ", "_") + "."
									+ stringSplit[0].toLowerCase().replaceAll(" ", "_"))))) {
						item = new ItemStack(Material.INK_SACK, 1, (byte) 8);
						ItemMeta itemMeta = item.getItemMeta();
						itemMeta.setDisplayName(notOwnedCosmetic.getItemMeta().getDisplayName());
						List<String> lore = notOwnedCosmetic.getItemMeta().getLore();
						if (stringSplit[1].contains("�aCOMMON")) {
							lore.add(" ");
							if (config.getInt("LootEggs.Eggshells") >= 25)
								lore.add("�eClick to buy for �b25 �eEggshells");
							else
								lore.add("�cCost �b25 �cEggshells");
						} else if (stringSplit[1].contains("�5RARE")) {
							lore.add(" ");
							if (config.getInt("LootEggs.Eggshells") >= 50)
								lore.add("�eClick to buy for �b50 �eEggshells");
							else
								lore.add("�cCost �b50 �cEggshells");
						} else if (stringSplit[1].contains("�6LEGENDARY")) {
							lore.add(" ");
							if (config.getInt("LootEggs.Eggshells") >= 100)
								lore.add("�eClick to buy for �b100 �eEggshells");
							else
								lore.add("�cCost �b100 �cEggshells");
						}
						itemMeta.setLore(lore);
						item.setItemMeta(itemMeta);
					} else if (!ignoreSelected
							&& config.getString("Cosmetics." + cosmeticType.replaceAll(" ", ""))
									.contains(stringSplit[0].toLowerCase().replaceAll(" ", "_"))) {
						ignoreSelected = true;
						item = itemString[step];
						ItemMeta itemMeta = item.getItemMeta();
						itemMeta.setDisplayName(selectedCosmetic.getItemMeta().getDisplayName());
						itemMeta.setLore(selectedCosmetic.getItemMeta().getLore());
						itemMeta.addEnchant(Enchantment.ARROW_DAMAGE, 0, true);
						itemMeta.addItemFlags(ItemFlag.values());
						item.setItemMeta(itemMeta);
					} else {
						item = itemString[step];
						ItemMeta itemMeta = item.getItemMeta();
						itemMeta.setDisplayName(ownedCosmetic.getItemMeta().getDisplayName());
						itemMeta.setLore(ownedCosmetic.getItemMeta().getLore());
						itemMeta.removeEnchant(Enchantment.ARROW_DAMAGE);
						itemMeta.addItemFlags(ItemFlag.values());
						item.setItemMeta(itemMeta);
					}
					ItemMeta itemMeta = item.getItemMeta();
					itemMeta.setDisplayName(itemMeta.getDisplayName().replaceAll("%name%", stringSplit[0])
							.replaceAll("%catagory%", cosmeticType));
					ArrayList<String> itemLore = new ArrayList<>();
					for (int index = 0; index < itemMeta.getLore().size(); index++)
						itemLore.add(itemMeta.getLore().get(index).replaceAll("%name%", stringSplit[0])
								.replaceAll("%catagory%", cosmeticType).replaceAll("%rarity%", stringSplit[1])
								.replaceAll("%howto%", stringSplit[2]));
					itemMeta.setLore(itemLore);
					item.setItemMeta(itemMeta);
					cosmetic.setItem(i, item);
					step++;
				} else
					cosmetic.setItem(i, null);
			}
		player.openInventory(cosmetic);
	}

	public static void generateShopGUI(String shopType, Player player, double multiplier) {
		Inventory shopType0 = Bukkit.createInventory(null, 54, "�8�l" + shopType);
		ItemStack border = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemMeta borderMeta = border.getItemMeta();
		borderMeta.setDisplayName(" ");
		border.setItemMeta(borderMeta);
		for (int i = 0; i < shopType0.getSize(); i++)
			shopType0.setItem(i, border);

		ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
		skullMeta.setOwner(player.getName());
		skullMeta
				.setDisplayName("�6$" + NumberFormat.getNumberInstance(Locale.US).format(Main.econ.getBalance(player)));
		skull.setItemMeta(skullMeta);
		shopType0.setItem(49, skull);

		if (shopType.contains("Sell")) {
			List<Double> shopPricesR = new ArrayList<>();
			double multiplierReal = multiplier * Double.parseDouble(globalShopMultiplier.split("\\:")[1]);
			for (Double shopPrice : shopPrices) shopPricesR.add(shopPrice * multiplierReal);
			ItemStack wRSand = new ItemStack(Material.RED_SANDSTONE, 1);
			ItemMeta wRSandMeta = wRSand.getItemMeta();
			wRSandMeta.setDisplayName("�b�lRed Sandstone");
			ArrayList<String> wRSandLore = new ArrayList<>();
			wRSandLore.add("�7Price: �6$" + NumberFormat.getNumberInstance(Locale.US).format(shopPricesR.get(0)));
			wRSandMeta.setLore(wRSandLore);
			wRSand.setItemMeta(wRSandMeta);
			shopType0.setItem(20, wRSand);

			ItemStack wSand = new ItemStack(Material.SANDSTONE, 1);
			ItemMeta wSandMeta = wSand.getItemMeta();
			wSandMeta.setDisplayName("�b�lSandstone");
			ArrayList<String> wSandLore = new ArrayList<>();
			wSandLore.add("�7Price: �6$" + NumberFormat.getNumberInstance(Locale.US).format(shopPricesR.get(1)));
			wSandMeta.setLore(wSandLore);
			wSand.setItemMeta(wSandMeta);
			shopType0.setItem(29, wSand);

			ItemStack wOCoal = new ItemStack(Material.COAL_ORE, 1);
			ItemMeta wOCoalMeta = wOCoal.getItemMeta();
			wOCoalMeta.setDisplayName("�b�lCoal Ore");
			ArrayList<String> wOCoalLore = new ArrayList<>();
			wOCoalLore.add("�7Price: �6$" + NumberFormat.getNumberInstance(Locale.US).format(shopPricesR.get(2)));
			wOCoalMeta.setLore(wOCoalLore);
			wOCoal.setItemMeta(wOCoalMeta);
			shopType0.setItem(21, wOCoal);

			ItemStack wBCoal = new ItemStack(Material.COAL, 1);
			ItemMeta wBCoalMeta = wBCoal.getItemMeta();
			wBCoalMeta.setDisplayName("�b�lCoal");
			ArrayList<String> wBCoalLore = new ArrayList<>();
			wBCoalLore.add("�7Price: �6$" + NumberFormat.getNumberInstance(Locale.US).format(shopPricesR.get(3)));
			wBCoalMeta.setLore(wBCoalLore);
			wBCoal.setItemMeta(wBCoalMeta);
			shopType0.setItem(30, wBCoal);

			ItemStack wOIron = new ItemStack(Material.IRON_ORE, 1);
			ItemMeta wOIronMeta = wOIron.getItemMeta();
			wOIronMeta.setDisplayName("�b�lIron Ore");
			ArrayList<String> wOIronLore = new ArrayList<>();
			wOIronLore.add("�7Price: �6$" + NumberFormat.getNumberInstance(Locale.US).format(shopPricesR.get(4)));
			wOIronMeta.setLore(wOIronLore);
			wOIron.setItemMeta(wOIronMeta);
			shopType0.setItem(23, wOIron);

			ItemStack wBIron = new ItemStack(Material.IRON_INGOT, 1);
			ItemMeta wBIronMeta = wBIron.getItemMeta();
			wBIronMeta.setDisplayName("�b�lIron Ingot");
			ArrayList<String> wBIronLore = new ArrayList<>();
			wBIronLore.add("�7Price: �6$" + NumberFormat.getNumberInstance(Locale.US).format(shopPricesR.get(5)));
			wBIronMeta.setLore(wBIronLore);
			wBIron.setItemMeta(wBIronMeta);
			shopType0.setItem(32, wBIron);

			ItemStack wORedstone = new ItemStack(Material.GOLD_ORE, 1);
			ItemMeta wORedstoneMeta = wORedstone.getItemMeta();
			wORedstoneMeta.setDisplayName("�b�lGold Ore");
			ArrayList<String> wORedstoneLore = new ArrayList<>();
			wORedstoneLore.add("�7Price: �6$" + NumberFormat.getNumberInstance(Locale.US).format(shopPricesR.get(6)));
			wORedstoneMeta.setLore(wORedstoneLore);
			wORedstone.setItemMeta(wORedstoneMeta);
			shopType0.setItem(24, wORedstone);

			ItemStack wBRedstone = new ItemStack(Material.GOLD_INGOT, 1);
			ItemMeta wBRedstoneMeta = wBRedstone.getItemMeta();
			wBRedstoneMeta.setDisplayName("�b�lGold Ingot");
			ArrayList<String> wBRedstoneLore = new ArrayList<>();
			wBRedstoneLore.add("�7Price: �6$" + NumberFormat.getNumberInstance(Locale.US).format(shopPricesR.get(7)));
			wBRedstoneMeta.setLore(wBRedstoneLore);
			wBRedstone.setItemMeta(wBRedstoneMeta);
			shopType0.setItem(33, wBRedstone);
		} else if (shopType.contains("Food")) {
			ItemStack wBread = new ItemStack(Material.BREAD, 1);
			ItemMeta wBreadMeta = wBread.getItemMeta();
			wBreadMeta.setDisplayName("�b�lJunie Cake");
			ArrayList<String> wBreadLore = new ArrayList<>();
			wBreadLore.add("�a+5 �3Stamina");
			wBreadLore.add("");
			wBreadLore.add("�7Cost: �6$" + NumberFormat.getNumberInstance(Locale.US).format(0.09 * multiplier));
			wBreadMeta.setLore(wBreadLore);
			wBread.setItemMeta(wBreadMeta);
			shopType0.setItem(20, wBread);

			ItemStack wCookie = new ItemStack(Material.MELON, 1);
			ItemMeta wCookieMeta = wCookie.getItemMeta();
			wCookieMeta.setDisplayName("�b�lWaterfelon");
			ArrayList<String> wCookieLore = new ArrayList<>();
			wCookieLore.add("�a+15 �3Stamina");
			wCookieLore.add("");
			wCookieLore.add("�7Cost: �6$" + NumberFormat.getNumberInstance(Locale.US).format(0.26 * multiplier));
			wCookieMeta.setLore(wCookieLore);
			wCookie.setItemMeta(wCookieMeta);
			shopType0.setItem(21, wCookie);

			ItemStack wApple = new ItemStack(Material.APPLE, 1);
			ItemMeta wAppleMeta = wApple.getItemMeta();
			wAppleMeta.setDisplayName("�b�lThe Cure To Literally Everything");
			ArrayList<String> wAppleLore = new ArrayList<>();
			wAppleLore.add("�a+50 �3Stamina");
			wAppleLore.add("");
			wAppleLore.add("�7Cost: �6$" + NumberFormat.getNumberInstance(Locale.US).format(0.88 * multiplier));
			wAppleMeta.setLore(wAppleLore);
			wApple.setItemMeta(wAppleMeta);
			shopType0.setItem(22, wApple);

			ItemStack wCarrot = new ItemStack(Material.CARROT_ITEM, 1);
			ItemMeta wCarrotMeta = wCarrot.getItemMeta();
			wCarrotMeta.setDisplayName("�b�lEye Betterer");
			ArrayList<String> wCarrotLore = new ArrayList<>();
			wCarrotLore.add("�a+125 �3Stamina");
			wCarrotLore.add("");
			wCarrotLore.add("�7Cost: �6$" + NumberFormat.getNumberInstance(Locale.US).format(2.22 * multiplier));
			wCarrotMeta.setLore(wCarrotLore);
			wCarrot.setItemMeta(wCarrotMeta);
			shopType0.setItem(23, wCarrot);

			ItemStack wCookedFish = new ItemStack(Material.POTATO_ITEM, 1);
			ItemMeta wCookedFishMeta = wCookedFish.getItemMeta();
			wCookedFishMeta.setDisplayName("�b�lDarth Tater");
			ArrayList<String> wCookedFishLore = new ArrayList<>();
			wCookedFishLore.add("�a+200 �3Stamina");
			wCookedFishLore.add("");
			wCookedFishLore.add("�7Cost: �6$" + NumberFormat.getNumberInstance(Locale.US).format(3.56 * multiplier));
			wCookedFishMeta.setLore(wCookedFishLore);
			wCookedFish.setItemMeta(wCookedFishMeta);
			shopType0.setItem(24, wCookedFish);
		} else if (shopType.contains("Equipment")) {

			ItemStack wPick = new ItemStack(Material.WOOD_PICKAXE, 1);
			ItemMeta wPickMeta = wPick.getItemMeta();
			wPickMeta.setDisplayName("�dWooden Pickaxe");
			ArrayList<String> wPickLore = new ArrayList<>();
			wPickLore.add("�e�lRequired Level: �f�l0");
			wPickLore.add("");
			wPickLore.add("�7Cost: �6$" + NumberFormat.getNumberInstance(Locale.US).format(75));
			wPickMeta.setLore(wPickLore);
			wPickMeta.addItemFlags(ItemFlag.values());
			wPick.setItemMeta(wPickMeta);
			shopType0.setItem(20, wPick);

			ItemStack sPick = new ItemStack(Material.STONE_PICKAXE, 1);
			ItemMeta sPickMeta = sPick.getItemMeta();
			sPickMeta.setDisplayName("�dStone Pickaxe");
			ArrayList<String> sPickLore = new ArrayList<>();
			sPickLore.add("�e�lRequired Level: �f�l25");
			sPickLore.add("");
			sPickLore.add("�7Cost: �6$" + NumberFormat.getNumberInstance(Locale.US).format(250));
			sPickMeta.setLore(sPickLore);
			sPickMeta.addItemFlags(ItemFlag.values());
			sPick.setItemMeta(sPickMeta);
			shopType0.setItem(21, sPick);

			ItemStack iPick = new ItemStack(Material.IRON_PICKAXE, 1);
			ItemMeta iPickMeta = iPick.getItemMeta();
			iPickMeta.setDisplayName("�dIron Pickaxe");
			ArrayList<String> iPickLore = new ArrayList<>();
			iPickLore.add("�e�lRequired Level: �f�l75");
			iPickLore.add("");
			iPickLore.add("�7Cost: �6$" + NumberFormat.getNumberInstance(Locale.US).format(50000));
			iPickMeta.setLore(iPickLore);
			iPickMeta.addItemFlags(ItemFlag.values());
			iPick.setItemMeta(iPickMeta);
			shopType0.setItem(22, iPick);

			ItemStack dPick = new ItemStack(Material.DIAMOND_PICKAXE, 1);
			ItemMeta dPickMeta = dPick.getItemMeta();
			dPickMeta.setDisplayName("�dDiamond Pickaxe");
			ArrayList<String> dPickLore = new ArrayList<>();
			dPickLore.add("�e�lRequired Level: �f�l150");
			dPickLore.add("");
			dPickLore.add("�7Cost: �6$" + NumberFormat.getNumberInstance(Locale.US).format(750000));
			dPickMeta.setLore(dPickLore);
			dPickMeta.addItemFlags(ItemFlag.values());
			dPick.setItemMeta(dPickMeta);
			shopType0.setItem(23, dPick);

			ItemStack gPick = new ItemStack(Material.GOLD_PICKAXE, 1);
			ItemMeta gPickMeta = wPick.getItemMeta();
			gPickMeta.setDisplayName("�dGolden Pickaxe");
			ArrayList<String> gPickLore = new ArrayList<>();
			gPickLore.add("�e�lRequired Level: �f�l250");
			gPickLore.add("");
			gPickLore.add("�7Cost: �6$" + NumberFormat.getNumberInstance(Locale.US).format(2000000));
			gPickMeta.setLore(gPickLore);
			gPickMeta.addItemFlags(ItemFlag.values());
			gPick.setItemMeta(gPickMeta);
			shopType0.setItem(24, gPick);

			ItemStack fRod = new ItemStack(Material.FISHING_ROD, 1);
			ItemMeta fRodMeta = fRod.getItemMeta();
			fRodMeta.setDisplayName("�dFishing Rod");
			ArrayList<String> fRodLore = new ArrayList<>();
			fRodLore.add("�e�lRequired Level: �f�l50");
			fRodLore.add("");
			fRodLore.add("�7Cost: �6$" +  NumberFormat.getNumberInstance(Locale.US).format(15000));
			fRodMeta.setLore(fRodLore);
			fRod.setItemMeta(fRodMeta);
			shopType0.setItem(31, fRod);

			/*
			 * ItemStack wSword = new ItemStack(Material.WOOD_SWORD, 1);
			 * ItemMeta wSwordMeta = wSword.getItemMeta();
			 * wSwordMeta.setDisplayName("�dWooden Sword"); ArrayList<String>
			 * wSwordLore = new ArrayList<>();
			 * wSwordLore.add("�e�lRequired Level: �f�l15");
			 * wSwordLore.add("�c�l(BETA)"); wSwordLore.add("�7Cost: �6$" +
			 * NumberFormat.getNumberInstance(Locale.US).format(100));
			 * wSwordMeta.setLore(wSwordLore);
			 * wSwordMeta.addItemFlags(ItemFlag.values());
			 * wSword.setItemMeta(wSwordMeta); shopType0.setItem(29, wSword);
			 */

		}
		player.openInventory(shopType0);
	}

	public static void generateBanGUI(Player staff, OfflinePlayer punished, String reason) throws ParseException {
		Inventory banGUI = Bukkit.createInventory(null, 54, "�lPunish �� �4" + punished.getName());
		FileConfiguration configLoaded = Configs.loadFile(Configs.getPlayerFile(punished.getUniqueId(), "Player Data"));

		ItemStack border = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemMeta borderMeta = border.getItemMeta();
		borderMeta.setDisplayName(" ");
		border.setItemMeta(borderMeta);
		for (int i = 0; i < banGUI.getSize(); i++)
			banGUI.setItem(i, border);

		ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
		SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
		skullMeta.setDisplayName(configLoaded.getString("Name.Prefix").replaceAll("&", "�") + punished.getName());
		skullMeta.setOwner(punished.getName());
		ArrayList<String> skullLore = new ArrayList<>();
		skullLore.add("�cPunish Reason: �f" + reason);
		skullLore.add("�cCurrent Time: �f" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
		skullLore.add("�cUUID: �f" + punished.getUniqueId());
		skullLore.add(staff.hasPermission(new Permission("punishgui.mod"))
				? "�cLast Known IP: �f" + configLoaded.getString("Punishments.IP")
				: "�cLast Known IP: �fRequires �2MOD�f rank to view");
		skullMeta.setLore(skullLore);
		skull.setItemMeta(skullMeta);
		banGUI.setItem(4, skull);

		ItemStack warn = new ItemStack(Material.REDSTONE_TORCH_ON, 1);
		ItemMeta warnMeta = warn.getItemMeta();
		warnMeta.setDisplayName("�2Warn Player");
		warn.setItemMeta(warnMeta);
		banGUI.setItem(22, warn);

		ItemStack kick = new ItemStack(Material.INK_SACK, 1);
		ItemMeta kickMeta = kick.getItemMeta();
		kickMeta.setDisplayName("�2Kick Player");
		kick.setItemMeta(kickMeta);
		banGUI.setItem(31, kick);

		ItemStack history = new ItemStack(Material.ENCHANTED_BOOK, 1);
		ItemMeta historyMeta = kick.getItemMeta();
		historyMeta.setDisplayName("�6View Player Punishment History");
		historyMeta.addItemFlags(ItemFlag.values());
		history.setItemMeta(historyMeta);
		banGUI.setItem(49, history);

		String mute = Punishments.getMute(punished);
		if (mute == null) {
			ItemStack mute0 = new ItemStack(Material.INK_SACK, 1, (byte) 15);
			ItemMeta mute0Meta = mute0.getItemMeta();
			mute0Meta.setDisplayName("�2Mute Player �e1 Hour");
			mute0.setItemMeta(mute0Meta);
			banGUI.setItem(11, mute0);

			ItemStack mute1 = new ItemStack(Material.INK_SACK, 1, (byte) 4);
			ItemMeta mute1Meta = mute1.getItemMeta();
			mute1Meta.setDisplayName("�2Mute Player �e1 Day");
			mute1.setItemMeta(mute1Meta);
			banGUI.setItem(20, mute1);

			ItemStack mute2 = new ItemStack(Material.INK_SACK, 1, (byte) 14);
			ItemMeta mute2Meta = mute2.getItemMeta();
			mute2Meta.setDisplayName("�2Mute Player �e3 Days");
			mute2.setItemMeta(mute2Meta);
			banGUI.setItem(29, mute2);

			ItemStack mute3 = new ItemStack(Material.INK_SACK, 1, (byte) 1);
			ItemMeta mute3Meta = mute3.getItemMeta();
			mute3Meta.setDisplayName("�2Mute Player �e7 Days");
			mute3.setItemMeta(mute3Meta);
			banGUI.setItem(38, mute3);

			if (staff.hasPermission(new Permission("punishgui.mod"))) {
				ItemStack mute4 = new ItemStack(Material.INK_SACK, 1, (byte) 8);
				ItemMeta mute4Meta = mute4.getItemMeta();
				mute4Meta.setDisplayName("�2Mute Player �ePERMANENT");
				mute4.setItemMeta(mute4Meta);
				banGUI.setItem(47, mute4);
			}
		} else {
			String[] muteSplit = mute.split("#");
			FileConfiguration configPunisherLoaded = Configs
					.loadFile(Configs.getPlayerFile(UUID.fromString(muteSplit[1]), "Player Data"));

			ItemStack muteUn = new ItemStack(Material.BARRIER, 1);
			ItemMeta muteUnMeta = muteUn.getItemMeta();
			muteUnMeta.setDisplayName("�2Unmute Player");
			muteUnMeta.addEnchant(Enchantment.ARROW_DAMAGE, 0, true);
			muteUnMeta.addItemFlags(ItemFlag.values());
			ArrayList<String> muteUnLore = new ArrayList<>();
			muteUnLore.add("�ePunish Reason: �f" + muteSplit[2]);
			muteUnLore.add("�ePunish Time: �f" + muteSplit[3]);
			muteUnLore.add(
					muteSplit[0].contains("tempmute") ? "�ePunish End: �f" + muteSplit[4] : "�ePunish End: �fNever");
			muteUnLore.add("�ePunisher: �f" + configPunisherLoaded.getString("Name.Prefix").replaceAll("&", "�")
					+ configPunisherLoaded.getString("Name.Name"));
			muteUnMeta.setLore(muteUnLore);
			muteUn.setItemMeta(muteUnMeta);
			banGUI.setItem(20, muteUn);

		}
		String ban = Punishments.getBan(punished);
		if (ban == null) {
			ItemStack ban0 = new ItemStack(Material.IRON_BLOCK, 1);
			ItemMeta ban0Meta = ban0.getItemMeta();
			ban0Meta.setDisplayName("�2Ban Player �e1 Day");
			ban0.setItemMeta(ban0Meta);
			banGUI.setItem(15, ban0);

			if (staff.hasPermission(new Permission("punishgui.mod"))) {
				ItemStack ban1 = new ItemStack(Material.LAPIS_BLOCK, 1);
				ItemMeta ban1Meta = ban1.getItemMeta();
				ban1Meta.setDisplayName("�2Ban Player �e7 Days");
				ban1.setItemMeta(ban1Meta);
				banGUI.setItem(24, ban1);

				ItemStack ban2 = new ItemStack(Material.GOLD_BLOCK, 1);
				ItemMeta ban2Meta = ban2.getItemMeta();
				ban2Meta.setDisplayName("�2Ban Player �e14 Days");
				ban2.setItemMeta(ban2Meta);
				banGUI.setItem(33, ban2);

				ItemStack ban3 = new ItemStack(Material.REDSTONE_BLOCK, 1);
				ItemMeta ban3Meta = ban3.getItemMeta();
				ban3Meta.setDisplayName("�2Ban Player �e30 Days");
				ban3.setItemMeta(ban3Meta);
				banGUI.setItem(42, ban3);

				ItemStack ban4 = new ItemStack(Material.BEDROCK, 1);
				ItemMeta ban4Meta = ban4.getItemMeta();
				ban4Meta.setDisplayName("�2Ban Player �ePERMANENT");
				ban4.setItemMeta(ban4Meta);
				banGUI.setItem(51, ban4);
			}
		} else {
			String[] banSplit = ban.split("#");
			FileConfiguration configPunisherLoaded = Configs
					.loadFile(Configs.getPlayerFile(UUID.fromString(banSplit[1]), "Player Data"));

			ItemStack banUn = new ItemStack(Material.BARRIER, 1);
			ItemMeta banUnMeta = banUn.getItemMeta();
			banUnMeta.setDisplayName("�2Unban Player");
			banUnMeta.addEnchant(Enchantment.ARROW_DAMAGE, 0, true);
			banUnMeta.addItemFlags(ItemFlag.values());
			ArrayList<String> banUnLore = new ArrayList<>();
			banUnLore.add("�ePunish Reason: �f" + banSplit[2]);
			banUnLore.add("�ePunish Time: �f" + banSplit[3]);
			banUnLore.add(banSplit[0].contains("tempban") ? "�ePunish End: �f" + banSplit[4] : "�ePunish End: �fNever");
			banUnLore.add("�ePunisher: �f" + configPunisherLoaded.getString("Name.Prefix").replaceAll("&", "�")
					+ configPunisherLoaded.getString("Name.Name"));
			banUnMeta.setLore(banUnLore);
			banUn.setItemMeta(banUnMeta);
			banGUI.setItem(24, banUn);
		}
		if (staff.hasPermission(new Permission("punishgui.mod"))) {

		} else {
			ItemStack noPerm = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14);
			ItemMeta noPermMeta = noPerm.getItemMeta();
			noPermMeta.setDisplayName("�rRequires �2MOD �rrank to use");
			noPerm.setItemMeta(noPermMeta);
			if (mute == null)
				banGUI.setItem(47, noPerm);
			banGUI.setItem(24, noPerm);
			if (ban == null) {
				banGUI.setItem(33, noPerm);
				banGUI.setItem(42, noPerm);
				banGUI.setItem(51, noPerm);
			}
		}
		staff.openInventory(banGUI);
	}
}
