package com.stupidbot.universalcorep.utils;

import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Punishments {
	public static String getMute(OfflinePlayer player) throws ParseException {
		FileConfiguration config = Configs.loadFile(Configs.getPlayerFile(player.getUniqueId(), "Player Data"));
		String returnString = null;
		if (!(config.getStringList("Punishments.History").isEmpty())) {
			List<String> history = config.getStringList("Punishments.History");
			for (String punishment : history) {
				String[] punishmentSplit = punishment.split("#");
				if (punishmentSplit[0].contains("tempmute")) {
					if (punishmentSplit.length < 6) {
						SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
						Date date1 = sdf.parse(punishmentSplit[4]);
						Date date2 = sdf.parse(sdf.format(new Date()));
						if (date1.after(date2)) {
							returnString = punishment;
							break;
						}
					}
				} else if (punishmentSplit[0].contains("mute"))
					if (punishmentSplit.length < 5) {
						returnString = punishment;
						break;
					}
			}
		}
		return returnString;
	}

	public static String getBan(OfflinePlayer player) throws ParseException {
		FileConfiguration config = Configs.loadFile(Configs.getPlayerFile(player.getUniqueId(), "Player Data"));
		String returnString = null;
		if (!(config.getStringList("Punishments.History").isEmpty())) {
			List<String> history = config.getStringList("Punishments.History");
			for (String punishment : history) {
				String[] punishmentSplit = punishment.split("#");
				if (punishmentSplit[0].contains("tempban")) {
					if (punishmentSplit.length < 6) {
						SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
						Date date1 = sdf.parse(punishmentSplit[4]);
						Date date2 = sdf.parse(sdf.format(new Date()));
						if (date1.after(date2)) {
							returnString = punishment;
							break;
						}
					}
				} else if (punishmentSplit[0].contains("ban"))
					if (punishmentSplit.length < 5) {
						returnString = punishment;
						break;
					}
			}
		}
		return returnString;
	}
}
