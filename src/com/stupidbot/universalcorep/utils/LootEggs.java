package com.stupidbot.universalcorep.utils;

import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.listeners.OnEntityInteract;
import com.stupidbot.universalcorep.listeners.OnItemDespawn;
import com.stupidbot.universalcorep.listeners.OnItemPickup;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R3.EntityLightning;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityWeather;
import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.permissions.Permission;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.EulerAngle;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class LootEggs {
	public static final ItemStack howToObtainLootEggs = Text.newBook("How To Get Loot Eggs", "StupidBot",
			"[\"\",{\"text\":\"  GET LOOT EGGS\",\"bold\":\"true\",\"color\":\"dark_green\"},{\"text\":\"\n\n\nYou can obtain Loot Eggs from levelling up or you can buy them at our store. Loot Eggs give a variety of \",\"bold\":\"false\",\"color\":\"black\"},{\"text\":\"COMMON\",\"color\":\"green\"},{\"text\":\",\",\"color\":\"black\"},{\"text\":\" RARE\",\"color\":\"dark_purple\"},{\"text\":\", and\n\",\"color\":\"black\"},{\"text\":\"LEGENDARY \",\"color\":\"gold\"},{\"text\":\"COSMETIC prizes!\n\n\n  \",\"color\":\"black\"},{\"text\":\"VISIT OUR STORE\",\"bold\":\"true\",\"color\":\"light_purple\",\"underlined\":\"true\",\"clickEvent\":{\"action\":\"open_url\",\"value\":\"http://copperuniverse.enjin.com/shop\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"http://copperuniverse.enjin.com/shop\",\"color\":\"yellow\"}]}}}]");

	public static final Map<String, Integer> hatchEggTimer = new HashMap<>();
	static Random random = new Random();
	private static final String[] prizes0 = { "�b�nProjectile Trail:�aSnow", "�b�nProjectile Trail:�aSplash",
			"�b�nProjectile Trail:�aSlimey", "�b�nProjectile Trail:�aBlack Magic", "�b�nProjectile Trail:�aDirty",
			"�b�nProjectile Trail:�aElectrocution", "�b�nProjectile Trail:�aEnchanted", "�b�nKill Message:�aPoked",
			"�b�nKill Message:�aBedWars", "�b�nKill Message:�aBurnt Bacon", "�b�nKill Message:�aFalse Imprisonment",
			"�b�nKill Message:�aEducated", "�b�nKill Message:�aYandere", "�b�nKill Message:�aRekt",
			"�b�nKill Message:�aAbuse", "�b�nKill Message:�aBroken", "�b�nKill Message:�aHeadshot" };
	private static final ItemStack[] items0 = { new ItemStack(Material.SNOW_BALL, 1), new ItemStack(Material.GHAST_TEAR, 1),
			new ItemStack(Material.SLIME_BLOCK, 1), new ItemStack(Material.BREWING_STAND_ITEM, 1),
			new ItemStack(Material.DIRT, 1), new ItemStack(Material.GLOWSTONE_DUST, 1),
			new ItemStack(Material.ENCHANTMENT_TABLE, 1), new ItemStack(Material.STICK, 1),
			new ItemStack(Material.BED, 1), new ItemStack(Material.GRILLED_PORK, 1),
			new ItemStack(Material.IRON_FENCE, 1), new ItemStack(Material.APPLE, 1),
			new ItemStack(Material.BOOK_AND_QUILL, 1), new ItemStack(Material.ROTTEN_FLESH, 1),
			new ItemStack(Material.LEASH, 1), new ItemStack(Material.RECORD_11, 1), new ItemStack(Material.BOW, 1) };

	private static final String[] prizes1 = { "�b�nProjectile Trail:�5God's Dust", "�b�nProjectile Trail:�5Grinded Emerald",
			"�b�nProjectile Trail:�5P.F.U.D.O.R.", "�b�nProjectile Trail:�5Curse", "�b�nProjectile Trail:�5Blood",
			"�b�nProjectile Trail:�5Compressed Air", "�b�nProjectile Trail:�5Default v2",
			"�b�nProjectile Trail:�5Smokey", "�b�nKill Message:�5DETERMINATION", "�b�nKill Message:�5Phantom Thief",
			"�b�nKill Message:�5EXPROSION", "�b�nKill Message:�5Another Castle",
			"�b�nKill Message:�5Fully-Armed Batallion", "�b�nChat Tag:�5#POTATO451" };
	private static final ItemStack[] items1 = { new ItemStack(Material.SUGAR, 1), new ItemStack(Material.EMERALD, 1),
			new ItemStack(Material.WOOL, 1, (byte) 6), new ItemStack(Material.BLAZE_ROD, 1),
			new ItemStack(Material.REDSTONE_BLOCK, 1), new ItemStack(Material.GLASS_BOTTLE, 1),
			new ItemStack(Material.DIAMOND_AXE, 1), new ItemStack(Material.TORCH, 1),
			new ItemStack(Material.YELLOW_FLOWER, 1), new ItemStack(Material.PUMPKIN, 1),
			new ItemStack(Material.TNT, 1), new ItemStack(Material.RED_MUSHROOM, 1), new ItemStack(Material.BOAT, 1),
			new ItemStack(Material.POTATO_ITEM, 1) };

	private static final String[] prizes2 = { "�b�nProjectile Trail:�6Twinkle", "�b�nProjectile Trail:�6Rage",
			"�b�nProjectile Trail:�6Magma Drop", "�b�nProjectile Trail:�6Water Leak", "�b�nProjectile Trail:�6Love",
			"�b�nProjectile Trail:�6Real Talent", "�b�nProjectile Trail:�6Crayon", "�b�nKill Message:�6Blasting Off",
			"�b�nKill Message:�6Disappointment", "�b�nKill Message:�6Blocked", "�b�nKill Message:�6Attack On Minecraft",
			"�b�nKill Message:�6Modding A.P.I.", "�b�nShip Skin:�6Hipster" };
	private static final ItemStack[] items2 = { new ItemStack(Material.FIREWORK, 1), new ItemStack(Material.DEAD_BUSH, 1),
			new ItemStack(Material.LAVA_BUCKET, 1), new ItemStack(Material.WATER_BUCKET, 1),
			new ItemStack(Material.RED_ROSE, 1), new ItemStack(Material.JUKEBOX, 1),
			new ItemStack(Material.INK_SACK, 1, (byte) 11), new ItemStack(Material.FIREWORK_CHARGE, 1),
			new ItemStack(Material.STONE, 1, (byte) 3), new ItemStack(Material.OBSIDIAN, 1),
			new ItemStack(Material.FEATHER, 1), new ItemStack(Material.ANVIL, 1), new ItemStack(Material.GLASS, 1) };

	public static void hatchLootEgg(final Player player) {
		File playerFile = Configs.getPlayerFile(player, "Player Data");
		FileConfiguration config = Configs.loadFile(playerFile);

		if (config.getInt("LootEggs.Amount") > 0) {
			Location locBack = player.getLocation();

			ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
			SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
			MiscellaneousUtils.setNonPlayerSkullOwner(skullMeta,
					"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/e2a850feabb07349cfe245b26a264ea36df73338f84cd2ee3833b185e1e2e2d8\"}}}");
			skull.setItemMeta(skullMeta);

			ItemStack skull1 = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
			SkullMeta skull1Meta = (SkullMeta) skull1.getItemMeta();
			MiscellaneousUtils.setNonPlayerSkullOwner(skull1Meta,
					"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/5a6787ba32564e7c2f3a0ce64498ecbb23b89845e5a66b5cec7736f729ed37\"}}}");
			skull1.setItemMeta(skull1Meta);

			ItemStack skull2 = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
			SkullMeta skull2Meta = (SkullMeta) skull2.getItemMeta();
			MiscellaneousUtils.setNonPlayerSkullOwner(skull2Meta,
					"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/e36e94f6c34a35465fce4a90f2e25976389eb9709a12273574ff70fd4daa6852\"}}}");
			skull2.setItemMeta(skull2Meta);

			ArmorStand stand = Bukkit.getWorld("world")
					.spawn(new Location(Bukkit.getWorld("world"), 75.5, 113, -0.5, 30, 0), ArmorStand.class);
			stand.setVisible(false);
			stand.setGravity(false);
			stand.setHelmet(skull);
			stand.setSmall(true);

			ArmorStand stand1 = Bukkit.getWorld("world")
					.spawn(new Location(Bukkit.getWorld("world"), 77, 107.75, -1.25, 135, 0), ArmorStand.class);
			stand1.setVisible(false);
			stand1.setGravity(false);
			stand1.setSmall(true);

			ArmorStand stand2 = Bukkit.getWorld("world")
					.spawn(new Location(Bukkit.getWorld("world"), 74, 107.75, -1.25, -135, 0), ArmorStand.class);
			stand2.setVisible(false);
			stand2.setGravity(false);
			stand2.setSmall(true);

			ArmorStand stand3 = Bukkit.getWorld("world")
					.spawn(new Location(Bukkit.getWorld("world"), 75.5, 109.5, 0.0, 0, 0), ArmorStand.class);
			stand3.setVisible(false);
			stand3.setGravity(false);
			stand3.setMarker(true);

			ArmorStand stand4 = Bukkit.getWorld("world")
					.spawn(new Location(Bukkit.getWorld("world"), 75.5, 110, 0.0, 0, 0), ArmorStand.class);
			stand4.setVisible(false);
			stand4.setGravity(false);
			stand4.setMarker(true);

			ItemStack barrier = new ItemStack(Material.BARRIER, 1);
			ItemMeta barrierMeta = barrier.getItemMeta();
			barrierMeta.setDisplayName(UUID.randomUUID().toString());
			barrier.setItemMeta(barrierMeta);

			Item item = stand4.getWorld()
					.dropItemNaturally(new Location(Bukkit.getWorld("world"), 75.5, 113, -0.5, 0, 0), barrier);

			player.teleport(new Location(Bukkit.getWorld("world"), 75.5, 108, -2.5, 0, 0));

			player.playSound(player.getLocation(), Sound.BAT_TAKEOFF, 2, 1);
			player.setGameMode(GameMode.ADVENTURE);
			player.setWalkSpeed(0);
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 1000000, 128, true, false));

			Main.removeList.add(stand);
			Main.removeList.add(stand1);
			Main.removeList.add(stand2);
			Main.removeList.add(stand3);
			Main.removeList.add(stand4);
			Main.removeList.add(item);
			OnEntityInteract.stopInteractList.add(stand);
			OnEntityInteract.stopInteractList.add(stand1);
			OnEntityInteract.stopInteractList.add(stand2);
			OnItemDespawn.stopDespawnList.add(item);
			OnItemPickup.stopPickupList.add(item);
			hatchEggTimer.put(player.getUniqueId().toString(), 0);
			new BukkitRunnable() {
				String eggs = NumberFormat.getInstance(Locale.US)
						.format(Configs.loadPlayerFile(player, "Player Data").getInt("LootEggs.Amount"));

				@Override
				public void run() {
					int t = hatchEggTimer.getOrDefault(player.getUniqueId().toString(), 0);
					if (t < 110) {
						if (t % 4 == 0) {
							Location fallLoc = stand.getLocation();
							fallLoc.setY(stand.getLocation().getY() + 0.75);
							ParticleEffect.FIREWORKS_SPARK.display(0, 0, 0, 0.5F, 2, fallLoc, player);
						}

						if (t % 20 == 0 && t != 0)
							player.playSound(player.getLocation(), Sound.FIREWORK_TWINKLE, 2, 1);

						hatchEggTimer.put(player.getUniqueId().toString(),
								hatchEggTimer.get(player.getUniqueId().toString()) + 1);
						if (t <= 100)
							stand.teleport(new Location(Bukkit.getWorld("world"), 75.5, 113 - (t * 0.05),
									-0.5, (float) (t * 10.8), 0));
					}
					if (t == 110) {
						((CraftPlayer) player).getHandle().playerConnection
								.sendPacket(new PacketPlayOutSpawnEntityWeather(new EntityLightning(
										((CraftPlayer) player).getHandle().getWorld(), 75, 107, 0, false, false)));
						player.playSound(player.getLocation(), Sound.GHAST_FIREBALL, 2, 2);
						Location boomLoc = stand.getLocation();
						boomLoc.setY(stand.getLocation().getY() + 0.75);

						ParticleEffect.FLAME.display(0, 0, 0, 0.25F, 25, boomLoc, player);

						player.sendMessage(Text.sendCenteredMessage("�e�lPUNCH�r�a an option", 131));
						player.sendMessage(Text.sendCenteredMessage("�cOR", 131));
						player.sendMessage(Text.sendCenteredMessage("�e�lSNEAK�r�a to view your cosmetics", 131));

						stand.setCustomName("�e�lHatch Egg");
						stand.setCustomNameVisible(true);

						stand1.setHelmet(skull1);
						stand1.setCustomName("�c�lExit");
						stand1.setCustomNameVisible(true);

						stand2.setHelmet(skull2);
						stand2.setCustomName("�6�lBuy Eggs");
						stand2.setCustomNameVisible(true);

						stand3.setCustomName("�4No Previous Prize");
						stand3.setCustomNameVisible(true);

						item.setCustomNameVisible(true);
						stand4.setPassenger(item);

						stand.teleport(new Location(Bukkit.getWorld("world"), 75.5, 108, -0.5, 0, 0));
						stand1.teleport(new Location(Bukkit.getWorld("world"), 77, 108, -1.25, 135, 0));
						stand2.teleport(new Location(Bukkit.getWorld("world"), 74, 108, -1.25, -135, 0));

						hatchEggTimer.put(player.getUniqueId().toString(), 200);
					}
					if (t == 200 || t == 201) {
						double dot = stand.getEyeLocation().toVector().subtract(player.getEyeLocation().toVector())
								.normalize().dot(player.getEyeLocation().getDirection());
						double dot1 = stand1.getEyeLocation().toVector().subtract(player.getEyeLocation().toVector())
								.normalize().dot(player.getEyeLocation().getDirection());
						double dot2 = stand2.getEyeLocation().toVector().subtract(player.getEyeLocation().toVector())
								.normalize().dot(player.getEyeLocation().getDirection());

						if (dot > 0.99D) {
							stand.teleport(new Location(Bukkit.getWorld("world"), 75.5, 108.15, -0.5, 0, 0));
							Text.showActionBar(player,
									"{\"text\":\"�bHatch a Loot Egg, �6�l" + eggs + " �bavailable!\"}");
							if (t == 201) {
								if (Configs.loadPlayerFile(player, "Player Data").getInt("LootEggs.Amount") > 0) {
									FileConfiguration config = Configs.loadPlayerFile(player, "Player Data");
									config.set("LootEggs.Amount", config.getInt("LootEggs.Amount") - 1);
									config.set("LootEggs.Opened", config.getInt("LootEggs.Opened") + 1);
									try {
										config.save(Configs.getPlayerFile(player, "Player Data"));
									} catch (IOException e) {
										e.printStackTrace();
									}
									hatchEggTimer.put(player.getUniqueId().toString(), 300);
								} else {
									Text.openBook(howToObtainLootEggs, player);
									player.playSound(player.getLocation(), Sound.LEVEL_UP, 2F, 1F);
									hatchEggTimer.put(player.getUniqueId().toString(), 200);
								}
							}
						} else if (dot1 > 0.99D) {
							stand1.teleport(new Location(Bukkit.getWorld("world"), 77, 108.15, -1.25, 135, 0));
							Text.showActionBar(player, "{\"text\":\"�cExit menu!\"}");
							if (t == 201)
								hatchEggTimer.remove(player.getUniqueId().toString());
						} else if (dot2 > 0.99D) {
							stand2.teleport(new Location(Bukkit.getWorld("world"), 74, 108.15, -1.25, -135, 0));
							Text.showActionBar(player, "{\"text\":\"�6Buy Loot Eggs with �4�lREAL�6 money!\"}");
							if (t == 201) {
								Text.openBook(howToObtainLootEggs, player);
								player.playSound(player.getLocation(), Sound.LEVEL_UP, 2F, 1F);
								hatchEggTimer.put(player.getUniqueId().toString(), 200);
							}
						} else {
							stand.teleport(new Location(Bukkit.getWorld("world"), 75.5, 108, -0.5, 0, 0));
							stand1.teleport(new Location(Bukkit.getWorld("world"), 77, 108, -1.25, 135, 0));
							stand2.teleport(new Location(Bukkit.getWorld("world"), 74, 108, -1.25, -135, 0));
							Text.showActionBar(player, "{\"text\":\"�aChoose an option!\"}");
							if (t == 201)
								hatchEggTimer.put(player.getUniqueId().toString(), 200);
						}
					}

					if (t >= 300 && t <= 422) {
						hatchEggTimer.put(player.getUniqueId().toString(),
								hatchEggTimer.get(player.getUniqueId().toString()) + 1);
						EulerAngle shake;
						if (t < 350)
							shake = new EulerAngle(ThreadLocalRandom.current().nextDouble(-0.05, 0.05),
									ThreadLocalRandom.current().nextDouble(-0.05, 0.05),
									ThreadLocalRandom.current().nextDouble(-0.05, 0.05));
						else if (t < 400)
							shake = new EulerAngle(ThreadLocalRandom.current().nextDouble(-0.05, 0.05),
									ThreadLocalRandom.current().nextDouble(-0.1, 0.1),
									ThreadLocalRandom.current().nextDouble(-0.1, 0.1));
						else
							shake = new EulerAngle(ThreadLocalRandom.current().nextDouble(-0.05, 0.05),
									ThreadLocalRandom.current().nextDouble(-0.15, 0.15),
									ThreadLocalRandom.current().nextDouble(-0.15, 0.15));
						ParticleEffect.SMOKE_NORMAL.display(0, 0, 0, 1F, 2, stand.getLocation(), player);

						if (t == 325 || t == 350 || t == 375 || t == 400) {
							Location boomLoc = stand.getLocation();
							boomLoc.setY(boomLoc.getY() + 0.8);
							ParticleEffect.LAVA.display(0.5F, 0, 0.5F, 1F, 15, boomLoc, player);
							player.playSound(player.getLocation(), Sound.GHAST_FIREBALL, 2, 1.5F);
						}
						stand.setHeadPose(shake);

						if (t == 301) {
							stand.teleport(new Location(Bukkit.getWorld("world"), 75.5, 108, -0.5, 0, 0));
							stand.setCustomNameVisible(false);
							stand1.setCustomNameVisible(false);
							stand2.setCustomNameVisible(false);
							stand1.setHelmet(new ItemStack(Material.AIR, 1));
							stand2.setHelmet(new ItemStack(Material.AIR, 1));
							Text.showActionBar(player, "{\"text\":\"\"}");
							player.playSound(player.getLocation(), Sound.EXPLODE, 2, 2);
						}
						if (t == 372) {
							ParticleEffect.ENCHANTMENT_TABLE.display(0, 0, 0, 4F, 250,
									stand.getLocation().add(0, 2.1, 0), player);
							player.playSound(player.getLocation(), Sound.LEVEL_UP, 2F, 0.5F);
						}
						if (t == 389) {
							ItemStack crack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
							SkullMeta crackMeta = (SkullMeta) crack.getItemMeta();
							MiscellaneousUtils.setNonPlayerSkullOwner(crackMeta,
									"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/8c4ff69ffd18fb63d3e195a02881871425a4b084e4ec3e9c537b5a6eb052\"}}}");
							crack.setItemMeta(crackMeta);
							stand.setHelmet(crack);
							player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 2F, 0.8F);
						}
						if (t == 392) {
							ItemStack crack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
							SkullMeta crackMeta = (SkullMeta) crack.getItemMeta();
							MiscellaneousUtils.setNonPlayerSkullOwner(crackMeta,
									"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/a4c57bc1a9fcb9e3c7593687b0c238ff8dc92d932d0644190a7fbd3b6e4aa21\"}}}");
							crack.setItemMeta(crackMeta);
							stand.setHelmet(crack);
							player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 2F, 0.9F);
						}
						if (t == 395) {
							ItemStack crack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
							SkullMeta crackMeta = (SkullMeta) crack.getItemMeta();
							MiscellaneousUtils.setNonPlayerSkullOwner(crackMeta,
									"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/3c631d8a23a274a33bee6f9c3874591deb9dc905c5ea3aa59c8ee9789426\"}}}");
							crack.setItemMeta(crackMeta);
							stand.setHelmet(crack);
							player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 2F, 1F);
						}
						if (t == 398) {
							ItemStack crack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
							SkullMeta crackMeta = (SkullMeta) crack.getItemMeta();
							MiscellaneousUtils.setNonPlayerSkullOwner(crackMeta,
									"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/416c02a7c3ece432d90ccc331c2849cd979f4fff1aef3d413b34a9c2c415269\"}}}");
							crack.setItemMeta(crackMeta);
							stand.setHelmet(crack);
							player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 2F, 1.1F);
						}
						if (t == 401) {
							ItemStack crack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
							SkullMeta crackMeta = (SkullMeta) crack.getItemMeta();
							MiscellaneousUtils.setNonPlayerSkullOwner(crackMeta,
									"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/4f68d12c703e13a23a9e447fa28b6a89355bffb8766b1069c6c8fcb259874315\"}}}");
							crack.setItemMeta(crackMeta);
							stand.setHelmet(crack);
							player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 2F, 1.2F);
						}
						if (t == 404) {
							ItemStack crack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
							SkullMeta crackMeta = (SkullMeta) crack.getItemMeta();
							MiscellaneousUtils.setNonPlayerSkullOwner(crackMeta,
									"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/5ee4218e3e2bd1d22097da7bab2fbbfcb68cbbd39d314f7891c20f873dde460\"}}}");
							crack.setItemMeta(crackMeta);
							stand.setHelmet(crack);
							player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 2F, 1.3F);
						}
						if (t == 407) {
							ItemStack crack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
							SkullMeta crackMeta = (SkullMeta) crack.getItemMeta();
							MiscellaneousUtils.setNonPlayerSkullOwner(crackMeta,
									"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/9954a6dcc2e98e633f883fd6ff3bb2d6bda87b3df9d94cc626f3331a816e7b2\"}}}");
							crack.setItemMeta(crackMeta);
							stand.setHelmet(crack);
							player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 2F, 1.4F);
						}
						if (t == 410) {
							ItemStack crack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
							SkullMeta crackMeta = (SkullMeta) crack.getItemMeta();
							MiscellaneousUtils.setNonPlayerSkullOwner(crackMeta,
									"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/6e4af6c9ab383855244ba3adc1232cc67aea980bb63fe47cf5d6839d0b816c7\"}}}");
							crack.setItemMeta(crackMeta);
							stand.setHelmet(crack);
							player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 2F, 1.5F);
						}
						if (t == 413) {
							ItemStack crack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
							SkullMeta crackMeta = (SkullMeta) crack.getItemMeta();
							MiscellaneousUtils.setNonPlayerSkullOwner(crackMeta,
									"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/75298cb54b18a9c2d0ac3fabdec513a8d06a12d2f8a5183f113fddbf7eee77d1\"}}}");
							crack.setItemMeta(crackMeta);
							stand.setHelmet(crack);
							player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 2F, 1.6F);
						}
						if (t == 416) {
							ItemStack crack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
							SkullMeta crackMeta = (SkullMeta) crack.getItemMeta();
							MiscellaneousUtils.setNonPlayerSkullOwner(crackMeta,
									"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/7c86329bd3e79b397c7b2c466e34ce17efbbee1b7c8c31917564d327efce35a6\"}}}");
							crack.setItemMeta(crackMeta);
							stand.setHelmet(crack);
							player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 2F, 1.7F);
						}
						if (t == 419) {
							ItemStack crack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
							SkullMeta crackMeta = (SkullMeta) crack.getItemMeta();
							MiscellaneousUtils.setNonPlayerSkullOwner(crackMeta,
									"{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/74f794e079c9179eb4921240a6f3aed1314f5f62247cf9c91838ca43c2c89da\"}}}");
							crack.setItemMeta(crackMeta);
							stand.setHelmet(crack);
							player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 2F, 1.8F);
						}
						if (t == 421) {
							Location sLoc = stand.getLocation().add(0, 2, 0);
							ParticleEffect.EXPLOSION_LARGE.display(2, 2, 2, 0, 250, sLoc, player);
							ParticleEffect.EXPLOSION_NORMAL.display(1, 1, 1, 1F, 50, sLoc, player);
							player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 2F, 1.9F);
						}
						if (t == 422) {
							player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 2F, 2F);
							player.playSound(player.getLocation(), Sound.CAT_HIT, 3F, 1F);
							stand.setHeadPose(new EulerAngle(0, 0, 0));
							stand.setHelmet(skull);
							stand1.setHelmet(skull1);
							stand2.setHelmet(skull2);
							stand.setCustomNameVisible(true);
							stand1.setCustomNameVisible(true);
							stand2.setCustomNameVisible(true);

							HashMap<String[], ItemStack> prize = getLootEggPrize(player);
							String[] prizeInfo = prize.keySet().iterator().next();
							ItemStack prizeItem = prize.get(prizeInfo);

							stand3.setCustomName(prizeInfo[0]);
							item.setCustomName(prizeInfo[1]);

							ItemMeta prizeItemMeta = prizeItem.getItemMeta();
							prizeItemMeta.setDisplayName(prizeItemMeta.getDisplayName());
							prizeItem.setItemMeta(prizeItemMeta);
							item.setItemStack(prizeItem);

							eggs = NumberFormat.getInstance(Locale.US)
									.format(Configs.loadPlayerFile(player, "Player Data").getInt("LootEggs.Amount"));
							hatchEggTimer.put(player.getUniqueId().toString(), 200);
						}
					}
					for (Player all : Bukkit.getOnlinePlayers())
						if (all != player) {
							player.hidePlayer(all);
							PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(stand.getEntityId());
							((CraftPlayer) all).getHandle().playerConnection.sendPacket(packet);
							PacketPlayOutEntityDestroy packet1 = new PacketPlayOutEntityDestroy(stand1.getEntityId());
							((CraftPlayer) all).getHandle().playerConnection.sendPacket(packet1);
							PacketPlayOutEntityDestroy packet2 = new PacketPlayOutEntityDestroy(stand2.getEntityId());
							((CraftPlayer) all).getHandle().playerConnection.sendPacket(packet2);
							PacketPlayOutEntityDestroy packet3 = new PacketPlayOutEntityDestroy(stand3.getEntityId());
							((CraftPlayer) all).getHandle().playerConnection.sendPacket(packet3);
							PacketPlayOutEntityDestroy packet4 = new PacketPlayOutEntityDestroy(item.getEntityId());
							((CraftPlayer) all).getHandle().playerConnection.sendPacket(packet4);
						}
					if (!(hatchEggTimer.containsKey(player.getUniqueId().toString())) || !(player.isOnline())) {
						for (Player all : Bukkit.getOnlinePlayers())
							player.showPlayer(all);
						hatchEggTimer.remove(player.getUniqueId().toString());
						player.setWalkSpeed(0.2f);
						player.removePotionEffect(PotionEffectType.JUMP);
						player.teleport(locBack);
						player.setGameMode(GameMode.SURVIVAL);
						stand.remove();
						stand1.remove();
						stand2.remove();
						stand3.remove();
						stand4.remove();
						item.remove();
						Main.removeList.remove(stand);
						Main.removeList.remove(stand1);
						Main.removeList.remove(stand2);
						Main.removeList.remove(stand3);
						Main.removeList.remove(stand4);
						Main.removeList.remove(item);
						OnEntityInteract.stopInteractList.remove(stand);
						OnEntityInteract.stopInteractList.remove(stand1);
						OnEntityInteract.stopInteractList.remove(stand2);
						OnItemDespawn.stopDespawnList.remove(item);
						OnItemPickup.stopPickupList.remove(item);
						Text.showActionBar(player, "{\"text\":\"\"}");
						player.playSound(player.getLocation(), Sound.GLASS, 2F, 1F);
						LootEggs.hatchEggTimer.remove(player);
						cancel();
					}
				}
			}.runTaskTimer(Main.plugin, 0, 1);
		} else {
			Text.openBook(howToObtainLootEggs, player);
			player.playSound(player.getLocation(), Sound.LEVEL_UP, 2F, 1F);
		}
	}

	private static HashMap<String[], ItemStack> getLootEggPrize(final Player player) {
		HashMap<String[], ItemStack> returnMap = new HashMap<>();
		Double randomNum = ThreadLocalRandom.current().nextDouble(0, 100);
		String[] prizesChoose;
		ItemStack[] itemsChoose;
		FileConfiguration config = Configs.loadPlayerFile(player, "Player Data");
		int eggshells;
		if (randomNum >= 0 && randomNum < 62.5) {
			eggshells = ThreadLocalRandom.current().nextInt(1, 4);
			prizesChoose = prizes0;
			itemsChoose = items0;
			Text.showTitle(player, "{\"text\":\"\"}", 10, 15, 10);
			Text.showSubtitle(player, "{\"text\":\"COMMON\",\"bold\":\"true\",\"color\":\"green\"}", 10, 15, 10);
			config.set("LootEggs.Prizes.Commons", config.getInt("LootEggs.Prizes.Commons") + 1);
		} else if (randomNum >= 62.5 && randomNum < 92.5) {
			eggshells = ThreadLocalRandom.current().nextInt(4, 7);
			prizesChoose = prizes1;
			itemsChoose = items1;
			Text.showTitle(player, "{\"text\":\"\"}", 10, 15, 10);
			Text.showSubtitle(player, "{\"text\":\"RARE\",\"bold\":\"true\",\"color\":\"dark_purple\"}", 10, 15, 10);
			config.set("LootEggs.Prizes.Rares", config.getInt("LootEggs.Prizes.Rares") + 1);
		} else {
			eggshells = ThreadLocalRandom.current().nextInt(7, 10);
			prizesChoose = prizes2;
			itemsChoose = items2;
			Text.showTitle(player, "{\"text\":\"\"}", 10, 15, 10);
			Text.showSubtitle(player, "{\"text\":\"LEGENDARY\",\"bold\":\"true\",\"color\":\"gold\"}", 10, 15, 10);
			config.set("LootEggs.Prizes.Legendaries", config.getInt("LootEggs.Prizes.Legendaries") + 1);
		}
		int index = ThreadLocalRandom.current().nextInt(0, prizesChoose.length);
		String[] prizeSplit = prizesChoose[index].split("\\:");
		returnMap.put(prizeSplit, itemsChoose[index]);

		String prizePerm = "universalrpgp." + ChatColor.stripColor(prizeSplit[0].toLowerCase().replaceAll(" ", "_"))
				+ "." + ChatColor.stripColor(prizeSplit[1].toLowerCase().replaceAll(" ", "_"));
		if (!(player.hasPermission(new Permission(prizePerm)))) {
			Main.perms.playerAdd(player, prizePerm);
			player.sendMessage("�dYou got the " + prizeSplit[1] + " " + ChatColor.stripColor(prizeSplit[0]) + "�d!");
			config.set("LootEggs.Prizes.New", config.getInt("LootEggs.Prizes.New") + 1);
		} else {
			player.sendMessage("�cYou already have the " + prizeSplit[1] + " " + ChatColor.stripColor(prizeSplit[0])
					+ "�c! �bEggshells: �a+" + eggshells + "�c.");
			config.set("LootEggs.Prizes.Dupes", config.getInt("LootEggs.Prizes.Dupes") + 1);
			config.set("LootEggs.Eggshells", config.getInt("LootEggs.Eggshells") + eggshells);
			config.set("LootEggs.TotalEggshells", config.getInt("LootEggs.TotalEggshells") + eggshells);
		}
		try {
			config.save(Configs.getPlayerFile(player, "Player Data"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return returnMap;
	}
}
