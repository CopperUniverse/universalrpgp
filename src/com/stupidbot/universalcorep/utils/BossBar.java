package com.stupidbot.universalcorep.utils;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

class BossBar {
	private static final Map<String, EntityWither> withers = new ConcurrentHashMap<>();

	public static void sendBar(Player p, String text, float healthPercent) {
		Location loc = p.getLocation();
		WorldServer world = ((CraftWorld) p.getLocation().getWorld()).getHandle();

		EntityWither wither = new EntityWither(world);
		wither.setLocation(loc.getX(), loc.getY() - 100, loc.getZ(), 0, 0);

		PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(wither);

		DataWatcher watcher = new DataWatcher(null);
		watcher.a(0, (byte) 0x20); // Flags, 0x20 = invisible
		watcher.a(6, healthPercent * 300 / 100);
		watcher.a(10, text); // Entity name
		watcher.a(11, (byte) 1); // Show name, 1 = show, 0 = don't show
		watcher.a(16, (int) (healthPercent * 300) / 100); //Wither health, 300 = full
		// health

		try {
			Field t = PacketPlayOutSpawnEntityLiving.class.getDeclaredField("l");
			t.setAccessible(true);
			t.set(packet, watcher);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		withers.put(p.getName(), wither);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
	}

	public static void removeBar(Player p) {
		if (withers.containsKey(p.getName())) {
			PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(withers.get(p.getName()).getId());
			withers.remove(p.getName());
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
		}
	}

	public static void teleportBar(Player p) {
		if (withers.containsKey(p.getName())) {
			Location loc = p.getLocation().add(p.getLocation().getDirection().normalize().multiply(40));
			PacketPlayOutEntityTeleport packet = new PacketPlayOutEntityTeleport(withers.get(p.getName()).getId(),
					(int) loc.getX() * 32, (int) loc.getY() * 32, (int) loc.getZ() * 32,
					(byte) ((int) loc.getYaw() * 256 / 360), (byte) ((int) loc.getPitch() * 256 / 360), false);
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
		}
	}

	public static void updateText(Player p, String text) {
		updateBar(p, text, -1);
	}

	public static void updateHealth(Player p, float healthPercent) {
		updateBar(p, null, healthPercent);
	}

	public static void updateBar(Player p, String text, float healthPercent) {
		if (withers.containsKey(p.getName())) {
			DataWatcher watcher = new DataWatcher(null);
			watcher.a(0, (byte) 0x20);
			if (healthPercent != -1)
				watcher.a(6, (healthPercent * 300) / 100);
			if (text != null) {
				watcher.a(10, text);
				watcher.a(2, text);
			}
			watcher.a(11, (byte) 1);
			watcher.a(3, (byte) 1);

			PacketPlayOutEntityMetadata packet = new PacketPlayOutEntityMetadata(withers.get(p.getName()).getId(),
					watcher, true);
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
		}
	}

	public static Set<String> getPlayers() {
		Set<String> set = new HashSet<>();

		for (Map.Entry<String, EntityWither> entry : withers.entrySet())
			set.add(entry.getKey());

		return set;
	}
}
