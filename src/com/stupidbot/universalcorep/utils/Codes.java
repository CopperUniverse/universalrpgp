package com.stupidbot.universalcorep.utils;

import java.security.SecureRandom;
import java.util.Random;

class Codes {
	public static String genCode(String prefix, int codeLength) {
		char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new SecureRandom();
		sb.append(prefix);
		for (int i = 0; i < codeLength; i++) {
			if (i != 0 && i % 4 == 0)
				sb.append("-");
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		String output = sb.toString();
		System.out.println(output);

		return output;
	}
}
