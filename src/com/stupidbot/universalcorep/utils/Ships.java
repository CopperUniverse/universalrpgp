package com.stupidbot.universalcorep.utils;

import com.stupidbot.universalcorep.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class Ships {
	public static final HashMap<String, Integer> shipWait = new HashMap<>();
	private static final HashMap<String, Location> skinChoose = new HashMap<>();
	static {
		skinChoose.put("default",
				new Location(Bukkit.getServer().getWorld("world"), 9006.5D, 24.25D, 89996.5D, 180.0F, 0.0F));
		skinChoose.put("hipster",
				new Location(Bukkit.getServer().getWorld("world"), 9103.5D, 104.25D, 89972.5D, 135.0F, 0.0F));
		skinChoose.put("planetary",
				new Location(Bukkit.getServer().getWorld("world"), 8981.5D, 24.25D, 89956.5D, 180.0F, 0.0F));
		skinChoose.put("galaxious",
				new Location(Bukkit.getServer().getWorld("world"), 9008.5D, 191.25D, 89939.5D, 180.0F, 0.0F));
		skinChoose.put("universal",
				new Location(Bukkit.getServer().getWorld("world"), 8935.5D, 96.25D, 89927.5D, -90F, 0F));
	}

	public static void enterSpaceship(final Player player, final Location destination, final int time)
			throws IOException {
		player.closeInventory();

		File config = Configs.getPlayerFile(player, "Player Data");
		FileConfiguration configLoaded = Configs.loadFile(config);
		boolean instaFlight = time > 0 && configLoaded.getInt("Ships.InstaFlightTickets") > 0;
		int waitT = (int) (instaFlight ? 0
				: ((MiscellaneousUtils.locIsInRegion(destination, "droria_632")
						&& MiscellaneousUtils.locIsInRegion(player.getLocation(), "droria_632")) ? time / 2.5 : time));
		configLoaded.set("Ships.Flights", configLoaded.getInt("Ships.Flights") + 1);

		if (instaFlight) {
			configLoaded.set("Ships.InstaFlightTickets", configLoaded.getInt("Ships.InstaFlightTickets") - 1);
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.HOUR, 18);
			configLoaded.set("Timers.Rewards.InstaFlightTickets",
					new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(cal.getTime()));
		}
		configLoaded.save(config);
		if (configLoaded.getInt("Ships.Flights") == 3) {
			player.sendMessage(Text.sendCenteredMessage("�m+-----------------|-----------------+", 131));
			player.sendMessage(Text.sendCenteredMessage("�6�lTUTORIAL", 131));
			player.sendMessage(Text.sendCenteredMessage("�eEvery �a18 hours�e you get �a1 instant flight�e,", 131));
			player.sendMessage(Text.sendCenteredMessage("�eyou have to wait out every other flight!", 131));
			player.sendMessage(Text.sendCenteredMessage("�m+-----------------|-----------------+", 131));
		}
		if (waitT > 0) {
			player.teleport(skinChoose.get(configLoaded.getString("Cosmetics.ShipSkin")));
			BossBar.sendBar(player, "This message shouldn't appear", 100);
			player.sendMessage("�aYou'll arrive at your destination in �6" + waitT + " �aseconds.");
			if (player.hasPermission(new Permission("universalmiscp.fly")) && !player.getAllowFlight())
				player.performCommand("fly");
		}
		player.playSound(player.getLocation(), Sound.BAT_TAKEOFF, 2.0F, 1.0F);
		new BukkitRunnable() {
			@Override
			public void run() {
				Ships.shipWait.put(player.getUniqueId().toString(),
						Ships.shipWait.getOrDefault(player.getUniqueId().toString(), -1) + 1);
				int wait = (Ships.shipWait.get(player.getUniqueId().toString()));

				if (wait < waitT) {
					for (Player all : Bukkit.getOnlinePlayers())
						player.hidePlayer(all);
					BossBar.updateBar(player,
							"�b� �a" + (waitT - wait)
									+ ((waitT - wait == 1) ? " �dSecond Remaining �b�" : " �dSeconds Remaining �b�"),
							(100f / waitT) * (waitT - wait));
				} else if (wait == waitT) {
					BossBar.updateBar(player, "�b� �aArriving At Destination �b�", 0);
					player.teleport(destination);
					player.playSound(player.getLocation(), Sound.ANVIL_LAND, 2.0F, 1.0F);
					for (Player all : Bukkit.getOnlinePlayers())
						player.showPlayer(all);
					if (MiscellaneousUtils.locIsInRegion(player.getLocation(), "droria_632")) {
						if (player.getAllowFlight()) {
							player.setAllowFlight(false);
							player.sendMessage("�cFlight disabled!");
						}
					} else if (player.hasPermission(new Permission("universalmiscp.fly"))
							&& !player.getAllowFlight())
						player.performCommand("fly");
				} else if (wait > waitT) {
					Ships.shipWait.remove(player.getUniqueId().toString());
					BossBar.removeBar(player);
					cancel();
				}
				if (!(player.isOnline())) {
					cancel();
					BossBar.removeBar(player);
					Ships.shipWait.remove(player.getUniqueId().toString());
				}
				BossBar.teleportBar(player);
			}
		}.runTaskTimer(Main.plugin, 0L, 20L);
	}
}
