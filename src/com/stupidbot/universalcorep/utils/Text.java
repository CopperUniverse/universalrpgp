package com.stupidbot.universalcorep.utils;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.minecraft.server.v1_8_R3.*;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.text.NumberFormat;
import java.util.Locale;

public class Text {
	public static String sendCenteredMessage(String message, int CENTER_PX) {
		// CENTER_PX 131 for chat
		if (message == null || message.equals(""))
			return null;
		message = ChatColor.translateAlternateColorCodes('&', message);

		int messagePxSize = 0;
		boolean previousCode = false;
		boolean isBold = false;

		for (char c : message.toCharArray()) {
			if (c == '§')
				previousCode = true;
			else if (previousCode) {
				previousCode = false;
				isBold = c == 'l' || c == 'L';
			} else {
				DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
				messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
				messagePxSize++;
			}
		}
		int halvedMessageSize = messagePxSize / 2;
		int toCompensate = CENTER_PX - halvedMessageSize;
		int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
		int compensated = 0;
		StringBuilder sb = new StringBuilder();
		while (compensated < toCompensate) {
			sb.append(" ");
			compensated += spaceLength;
		}
		return (sb.toString() + message);
	}

	public static void sendJSONMessage(final Player player, final String jsonText) {
		PacketPlayOutChat packet = new PacketPlayOutChat(ChatSerializer.a(jsonText));
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
	}

	public static void showTitle(final Player player, final String jsonText, final int fadeIn, final int stay,
			final int fadeOut) {
		IChatBaseComponent chatTitle = ChatSerializer.a(jsonText);
		PacketPlayOutTitle title = new PacketPlayOutTitle(EnumTitleAction.TITLE, chatTitle);
		PacketPlayOutTitle length = new PacketPlayOutTitle(fadeIn, stay, fadeOut);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(title);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(length);
	}

	public static void showSubtitle(final Player player, final String jsonText, final int fadeIn, final int stay,
			final int fadeOut) {
		IChatBaseComponent chatSubtitle = ChatSerializer.a(jsonText);
		PacketPlayOutTitle subtitle = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, chatSubtitle);
		PacketPlayOutTitle length = new PacketPlayOutTitle(fadeIn, stay, fadeOut);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(subtitle);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(length);
	}

	public static void showActionBar(final Player player, final String jsonText) {
		IChatBaseComponent cbc = ChatSerializer.a(jsonText);
		PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte) 2);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(ppoc);
	}

	public static String toRoman(int mInt) {
		String[] rnChars = { "M", "CM", "D", "C", "XC", "L", "X", "IX", "V", "I" };
		int[] rnVals = { 1000, 900, 500, 100, 90, 50, 10, 9, 5, 1 };
		StringBuilder retVal = new StringBuilder();
		for (int i = 0; i < rnVals.length; i++) {
			int numberInPlace = mInt / rnVals[i];
			if (numberInPlace != 0) {
				retVal.append((numberInPlace == 4) && (i > 0) ? rnChars[i] + rnChars[(i - 1)]
                        : new String(new char[numberInPlace]).replace("\000", rnChars[i]));
				mInt %= rnVals[i];
			}
		}
		return retVal.toString();
	}

	public static ItemStack newBook(final String title, final String author, final String... pages) {
		ItemStack is = new ItemStack(Material.WRITTEN_BOOK, 1);
		net.minecraft.server.v1_8_R3.ItemStack nmsis = CraftItemStack.asNMSCopy(is);

		NBTTagCompound bd = new NBTTagCompound();
		bd.setString("title", title);
		bd.setString("author", author);
		NBTTagList bp = new NBTTagList();
		String[] arrayOfString;
		int j = (arrayOfString = pages).length;
		for (int i = 0; i < j; i++) {
			String text = arrayOfString[i];
			bp.add(new NBTTagString(text));
		}
		bd.set("pages", bp);
		nmsis.setTag(bd);
		is = CraftItemStack.asBukkitCopy(nmsis);
		return is;
	}

	public static void openBook(final ItemStack book, final Player p) {
		int slot = p.getInventory().getHeldItemSlot();
		ItemStack old = p.getInventory().getItem(slot);
		p.getInventory().setItem(slot, book);

		ByteBuf buf = Unpooled.buffer(256);
		buf.setByte(0, 0);
		buf.writerIndex(1);

		PacketPlayOutCustomPayload packet = new PacketPlayOutCustomPayload("MC|BOpen",
				new PacketDataSerializer(Unpooled.EMPTY_BUFFER));
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
		p.getInventory().setItem(slot, old);
	}

	public static String formatExperience(final int lvl) {
		/*
		 * String col;
		 * 
		 * if (lvl < 10) col = ""; else if (lvl >= 10 && lvl < 25) col = "§f";
		 * else if (lvl >= 25 && lvl < 50) col = "§3"; else if (lvl >= 50 && lvl
		 * < 75) col = "§e"; else if (lvl >= 75 && lvl < 100) col = "§d"; else
		 * if (lvl >= 100 && lvl < 150) col = "§a"; else if (lvl >= 150 && lvl <
		 * 200) col = "§b"; else if (lvl >= 200 && lvl < 250) col = "§5"; else
		 * if (lvl >= 250 && lvl < 300) col = "§6"; else if (lvl >= 300 && lvl <
		 * 500) col = "§c"; else if (lvl >= 500 && lvl < 1000) col = "§2"; else
		 * col = "§4";
		 * 
		 * return "§7[" + NumberFormat.getNumberInstance(Locale.US).format(lvl)
		 * + col + "✯§7]";
		 */
		
		return "§7[" + NumberFormat.getNumberInstance(Locale.US).format(lvl) + "✯§7]";
	}

	public static String numberWithSuffix(final long count) {
		if (count < 1000)
			return "" + count;
		int exp = (int) (Math.log(count) / Math.log(1000));
		return String.format("%.1f%c", count / Math.pow(1000, exp), "kMGTPE".charAt(exp - 1));
	}

}