package com.stupidbot.universalcorep.utils;

import com.stupidbot.universalcorep.utils.versions.VersionWrapper;
import com.stupidbot.universalcorep.utils.versions.Wrapper1_8_R3;
import org.apache.commons.lang3.Validate;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.function.BiFunction;

/**
 * An anvil gui, used for gathering a user's input
 * 
 * @author Wesley Smith
 * @since 1.0
 */
public class AnvilGUI {
	/**
	 * Give this item if player closes GUI
	 */
	private ItemStack refund;
	/**
	 * The player who has the GUI open
	 */
	private final Player holder;
    /**
	 * Called when the player clicks the {@link Slot#OUTPUT} slot
	 */
	private final BiFunction<Player, String, String> biFunction;

	/**
	 * The {@link VersionWrapper} for this server
	 */
	private final VersionWrapper wrapper;
	/**
	 * The container id of the inventory, used for NMS methods
	 */
	private final int containerId;
	/**
	 * The inventory that is used on the Bukkit side of things
	 */
	private final Inventory inventory;
	/**
	 * The listener holder class
	 */
	private final ListenUp listener = new ListenUp();

	/**
	 * Represents the state of the inventory being open
	 */
	private boolean open = false;

	/**
	 * Create an AnvilGUI and open it for the player
	 * 
	 * @param plugin
	 *            A {@link org.bukkit.plugin.java.JavaPlugin} instance
	 * @param holder
	 *            The {@link Player} to open the inventory for
	 * @param insert
	 *            What to have the text already set to
	 * @param clickHandler
	 *            A {@link ClickHandler} that is called when the player clicks
	 *            the {@link Slot#OUTPUT} slot
	 * @throws NullPointerException
	 *             If the server version isn't supported
	 *
	 * @deprecated As of version 1.1, use {@link AnvilGUI(Plugin, Player,
	 *             String, BiFunction)}
	 */
	@Deprecated
	public AnvilGUI(Plugin plugin, Player holder, Material paperType, ItemStack refundItem, String insert,
			ClickHandler clickHandler) {
		this(plugin, holder, paperType, refundItem, insert, clickHandler::onClick);
	}

	/**
	 * Create an AnvilGUI and open it for the player.
	 * 
	 * @param plugin
	 *            A {@link org.bukkit.plugin.java.JavaPlugin} instance
	 * @param holder
	 *            The {@link Player} to open the inventory for
	 * @param insert
	 *            What to have the text already set to
	 * @param biFunction
	 *            A {@link BiFunction} that is called when the player clicks the
	 *            {@link Slot#OUTPUT} slot
	 * @throws NullPointerException
	 *             If the server version isn't supported
	 */
	public AnvilGUI(Plugin plugin, Player holder, Material paperType, ItemStack refundItem, String insert,
			BiFunction<Player, String, String> biFunction) {
		this.holder = holder;
		this.biFunction = biFunction;
		this.refund = refundItem;

		final ItemStack paper = new ItemStack(paperType);
		final ItemMeta paperMeta = paper.getItemMeta();
		paperMeta.setDisplayName(insert);
		paperMeta.addItemFlags(ItemFlag.values());
		ArrayList<String> paperLore = new ArrayList<>();
		paperLore.add("§d● §eAll rules still apply");
		paperLore.add("§d● §eUse the '&' symbol for colors");
		paperLore.add("§d● §eYou may exit this GUI at any time");
		paperMeta.setLore(paperLore);
		paper.setItemMeta(paperMeta);
		/*
	  The ItemStack that is in the {@link Slot#INPUT_LEFT} slot.
	 */ /*
          The ItemStack that is in the {@link Slot#INPUT_LEFT} slot.
         */

		wrapper = new Wrapper1_8_R3();

		wrapper.handleInventoryCloseEvent(holder);
		wrapper.setActiveContainerDefault(holder);

		Bukkit.getPluginManager().registerEvents(listener, plugin);

		final Object container = wrapper.newContainerAnvil(holder);

		inventory = wrapper.toBukkitInventory(container);
		inventory.setItem(Slot.INPUT_LEFT, paper);

		containerId = wrapper.getNextContainerId(holder);
		wrapper.sendPacketOpenWindow(holder, containerId);
		wrapper.setActiveContainer(holder, container);
		wrapper.setActiveContainerId(container, containerId);
		wrapper.addActiveContainerSlotListener(container, holder);

		open = true;
	}

	/**
	 * Closes the inventory if it's open.
	 * 
	 * @throws IllegalArgumentException
	 *             If the inventory isn't open
	 */
	private void closeInventory() {
		if (refund != null)
			if (holder.getInventory().firstEmpty() != -1)
				holder.getInventory().addItem(refund);
			else
				holder.getWorld().dropItemNaturally(holder.getLocation(), refund);
		Validate.isTrue(open, "You can't close an inventory that isn't open!");
		open = false;

		wrapper.handleInventoryCloseEvent(holder);
		wrapper.setActiveContainerDefault(holder);
		wrapper.sendPacketCloseWindow(holder, containerId);

		HandlerList.unregisterAll(listener);
	}

	/**
	 * Simply holds the listeners for the GUI
	 */
	private class ListenUp implements Listener {

		@EventHandler
		public void onInventoryClick(InventoryClickEvent e) {
			final Player clicker = (Player) e.getWhoClicked();
			if (e.getInventory().equals(inventory) && clicker == holder) {
				e.setCancelled(true);
				final ItemStack clicked = inventory.getItem(e.getRawSlot());
				if (e.getRawSlot() == Slot.OUTPUT && clicked != null) {
					refund = null;
					closeInventory();
					final String ret = biFunction.apply(clicker, clicked.hasItemMeta()
							? clicked.getItemMeta().getDisplayName() : clicked.getType().toString());
					if (ret != null) {
						final ItemMeta meta = clicked.getItemMeta();
						meta.setDisplayName(ret);
						clicked.setItemMeta(meta);
						inventory.setItem(e.getRawSlot(), clicked);
					}
				}
			}
		}

		@EventHandler
		public void onInventoryClose(InventoryCloseEvent e) {
			final Player player = (Player) e.getPlayer();
			if (open && e.getInventory().equals(inventory) && player == holder)
				closeInventory();
		}
	}

	/**
	 * Handles the click of the output slot
	 *
	 * @deprecated Since version 1.1, use {@link AnvilGUI(Plugin, Player,
	 *             String, BiFunction)} instead
	 */
	@Deprecated
	public static abstract class ClickHandler {

		/**
		 * Is called when a {@link Player} clicks on the output in the GUI
		 * 
		 * @param clicker
		 *            The {@link Player} who clicked the output
		 * @param input
		 *            What the item was renamed to
		 * @return What to replace the text with, or null to close the inventory
		 */
		public abstract String onClick(Player clicker, String input);

	}

	/**
	 * Class wrapping the magic constants of slot numbers in an anvil GUI
	 */
	static class Slot {

		/**
		 * The slot on the far left, where the first input is inserted. An
		 * {@link ItemStack} is always inserted here to be renamed
		 */
		static final int INPUT_LEFT = 0;
		/**
		 * Not used, but in a real anvil you are able to put the second item you
		 * want to combine here
		 */
		public static final int INPUT_RIGHT = 1;
		/**
		 * The output slot, where an item is put when two items are combined
		 * from {@link #INPUT_LEFT} and {@link #INPUT_RIGHT} or
		 * {@link #INPUT_LEFT} is renamed
		 */
		static final int OUTPUT = 2;

	}
}