package com.stupidbot.universalcorep.utils;

import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.listeners.OnBlockBreak;
import com.stupidbot.universalcorep.listeners.OnEntityInteract;
import com.stupidbot.universalcorep.listeners.OnItemDespawn;
import com.stupidbot.universalcorep.listeners.OnItemPickup;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.PacketPlayOutBlockAction;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.util.CraftMagicNumbers;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class LootChests {
	private static ItemStack[] lootChestCommonItemStacks = { new ItemStack(Material.RED_SANDSTONE, 32),
			new ItemStack(Material.RED_SANDSTONE, 64), new ItemStack(Material.RED_SANDSTONE, 64),
			new ItemStack(Material.SANDSTONE, 32), new ItemStack(Material.SANDSTONE, 64),
			new ItemStack(Material.SANDSTONE, 64), new ItemStack(Material.PAPER, 1), new ItemStack(Material.PAPER, 1),
			new ItemStack(Material.PAPER, 1), new ItemStack(Material.PAPER, 1), new ItemStack(Material.PAPER, 1) };
	private static String[] lootChestCommonStrings = { "", "", "", "", "", "", "", "", "", "", "" };
	private static ItemStack[] lootChestRareItemStacks = { new ItemStack(Material.DIAMOND),
			new ItemStack(Material.DIRT) };
	private static String[] lootChestRareStrings = { "" };
	private static ItemStack[] lootChestLegendaryItemStacks = { new ItemStack(Material.DIAMOND),
			new ItemStack(Material.DIRT) };
	private static String[] lootChestLegendaryStrings = { "" };
	private static ItemStack[] lootChestGodlyItemStacks = { new ItemStack(Material.DIAMOND),
			new ItemStack(Material.DIRT) };
	private static String[] lootChestGodlyStrings = { "" };
	static {
		ArrayList<String> newLootChestCommonStrings = new ArrayList<>();
		ArrayList<ItemStack> newLootChestCommonItemStacks = new ArrayList<>();
		for (int i = 0; i < lootChestCommonStrings.length; i++) {
			String[] stringSplit = lootChestCommonStrings[i].split("\\:");
			for (int t = 0; t < Integer.parseInt(stringSplit[2]); t++) {
				newLootChestCommonStrings.add(lootChestCommonStrings[i]);
				newLootChestCommonItemStacks.add(lootChestCommonItemStacks[i]);
			}
		}
		lootChestCommonStrings = newLootChestCommonStrings.toArray(new String[0]);
		lootChestCommonItemStacks = newLootChestCommonItemStacks.toArray(new ItemStack[0]);

		ArrayList<String> newLootChestRareStrings = new ArrayList<>();
		ArrayList<ItemStack> newLootChestRareItemStacks = new ArrayList<>();
		for (int i = 0; i < lootChestRareStrings.length; i++) {
			String[] stringSplit = lootChestRareStrings[i].split("\\:");
			for (int t = 0; t < Integer.parseInt(stringSplit[2]); t++) {
				newLootChestRareStrings.add(lootChestRareStrings[i]);
				newLootChestRareItemStacks.add(lootChestRareItemStacks[i]);
			}
		}
		lootChestRareStrings = newLootChestRareStrings.toArray(new String[0]);
		lootChestRareItemStacks = newLootChestRareItemStacks.toArray(new ItemStack[0]);

		ArrayList<String> newLootChestLegendaryStrings = new ArrayList<>();
		ArrayList<ItemStack> newLootChestLegendaryItemStacks = new ArrayList<>();
		for (int i = 0; i < lootChestLegendaryStrings.length; i++) {
			String[] stringSplit = lootChestLegendaryStrings[i].split("\\:");
			for (int t = 0; t < Integer.parseInt(stringSplit[2]); t++) {
				newLootChestLegendaryStrings.add(lootChestLegendaryStrings[i]);
				newLootChestLegendaryItemStacks.add(lootChestLegendaryItemStacks[i]);
			}
		}
		lootChestLegendaryStrings = newLootChestLegendaryStrings.toArray(new String[0]);
		lootChestLegendaryItemStacks = newLootChestLegendaryItemStacks.toArray(new ItemStack[0]);

		ArrayList<String> newLootChestGodlyStrings = new ArrayList<>();
		ArrayList<ItemStack> newLootChestGodlyItemStacks = new ArrayList<>();
		for (int i = 0; i < lootChestGodlyStrings.length; i++) {
			String[] stringSplit = lootChestGodlyStrings[i].split("\\:");
			for (int t = 0; t < Integer.parseInt(stringSplit[2]); t++) {
				newLootChestGodlyStrings.add(lootChestGodlyStrings[i]);
				newLootChestGodlyItemStacks.add(lootChestGodlyItemStacks[i]);
			}
		}
		lootChestGodlyStrings = newLootChestGodlyStrings.toArray(new String[0]);
		lootChestGodlyItemStacks = newLootChestGodlyItemStacks.toArray(new ItemStack[0]);
	}

	@SuppressWarnings("deprecation")
	public static void openLootChest(final String type, final Location location, final Player player) {
		String[] lootString;
		ItemStack[] lootItems;
		Location location4 = location.add(0, 2, 0);
		ArmorStand stand = Bukkit.getWorld("world").spawn(new Location(Bukkit.getWorld("world"), location.getX() + 0.5,
				location.getY() + 1.5, location.getZ() + 0.5), ArmorStand.class);
		stand.setVisible(false);
		stand.setGravity(false);
		stand.setMarker(true);
		ArmorStand stand1 = Bukkit.getWorld("world").spawn(new Location(Bukkit.getWorld("world"), location.getX() + 0.5,
				location.getY() + 0.6, location.getZ() + 0.5), ArmorStand.class);
		stand1.setVisible(false);
		stand1.setGravity(false);
		stand1.setMarker(true);

		ItemStack itemItemStack = new ItemStack(Material.BARRIER, 1);
		ItemMeta itemItemStackMeta = itemItemStack.getItemMeta();
		itemItemStackMeta.setDisplayName(UUID.randomUUID().toString());
		itemItemStack.setItemMeta(itemItemStackMeta);
		Item item = stand1.getWorld().dropItemNaturally(stand1.getLocation().add(0, 100000000, 0), itemItemStack);

		Main.removeList.add(stand);
		OnEntityInteract.stopInteractList.add(stand);
		Main.removeList.add(stand1);
		OnEntityInteract.stopInteractList.add(stand1);
		Main.removeList.add(item);
		OnItemDespawn.stopDespawnList.add(item);
		OnItemPickup.stopPickupList.add(item);

		if (Objects.equals(type, "common")) {
			lootString = lootChestCommonStrings;
			lootItems = lootChestCommonItemStacks;
			stand.setCustomName("�aCOMMON �eLoot Chest");
		} else if (Objects.equals(type, "rare")) {
			lootString = lootChestRareStrings;
			lootItems = lootChestRareItemStacks;
			stand.setCustomName("�5RARE �eLoot Chest");
		} else if (Objects.equals(type, "legendary")) {
			lootString = lootChestLegendaryStrings;
			lootItems = lootChestLegendaryItemStacks;
			stand.setCustomName("�6LEGENDARY �eLoot Chest");
		} else {
			lootString = lootChestGodlyStrings;
			lootItems = lootChestGodlyItemStacks;
			stand.setCustomName("�cGODLY �eLoot Chest");
		}
		String direction = MiscellaneousUtils.getCardinalDirection4(player);
		location4.getBlock().setType(Material.TRAPPED_CHEST);
		Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "blockdata " + location4.getX() + " "
				+ location4.getY() + " " + location4.getZ() + " {Lock:\"poop\",CustomName:\"Loot Chest\"}");
		if (direction == null || Objects.equals(direction, "N"))
			location4.getBlock().setData((byte) 5);
		else if (Objects.equals(direction, "S"))
			location4.getBlock().setData((byte) 4);
		else if (Objects.equals(direction, "W"))
			location4.getBlock().setData((byte) 2);
		else
			location4.getBlock().setData((byte) 3);
		OnBlockBreak.blockReset.put(location4, "AIR:0");

		new BukkitRunnable() { // TODO Add sound + particles
			int i = 0;

			@Override
			public void run() {
				i++;
				if (i > 4 && i < 161) {
					for (Player all : Bukkit.getOnlinePlayers()) {
						PacketPlayOutBlockAction packet = new PacketPlayOutBlockAction(
								new BlockPosition(location.getX(), location.getY(), location.getZ()),
								CraftMagicNumbers.getBlock(location.getBlock()), 1, 1);
						((CraftPlayer) all).getHandle().playerConnection.sendPacket(packet);
					}
					if (i == 15) {
						stand.setCustomNameVisible(true);
						HashMap<String[], ItemStack> prize = getLootChestPrize(player, lootString, lootItems);
						String[] prizeInfo = prize.keySet().iterator().next();
						ItemStack prizeItem = prize.get(prizeInfo);
						ItemMeta prizeItemMeta = prizeItem.getItemMeta();
						prizeItemMeta.setDisplayName(item.getItemStack().getItemMeta().getDisplayName());
						prizeItem.setItemMeta(prizeItemMeta);
						item.setCustomName(prizeInfo[0]);
						item.setItemStack(prizeItem);
						stand1.setPassenger(item);
						item.setCustomNameVisible(true);
					} else if (i > 15 && i < 36) {
						HashMap<String[], ItemStack> prize = getLootChestPrize(player, lootString, lootItems);
						String[] prizeInfo = prize.keySet().iterator().next();
						ItemStack prizeItem = prize.get(prizeInfo);
						ItemMeta prizeItemMeta = prizeItem.getItemMeta();
						prizeItemMeta.setDisplayName(item.getItemStack().getItemMeta().getDisplayName());
						prizeItem.setItemMeta(prizeItemMeta);
						item.setCustomName(prizeInfo[0]);
						item.setItemStack(prizeItem);
					} else if (i > 36 && i < 47 && i % 2 == 0) {
						HashMap<String[], ItemStack> prize = getLootChestPrize(player, lootString, lootItems);
						String[] prizeInfo = prize.keySet().iterator().next();
						ItemStack prizeItem = prize.get(prizeInfo);
						ItemMeta prizeItemMeta = prizeItem.getItemMeta();
						prizeItemMeta.setDisplayName(item.getItemStack().getItemMeta().getDisplayName());
						prizeItem.setItemMeta(prizeItemMeta);
						item.setCustomName(prizeInfo[0]);
						item.setItemStack(prizeItem);
					} else if (i > 47 && i < 63 && i % 3 == 0) {
						HashMap<String[], ItemStack> prize = getLootChestPrize(player, lootString, lootItems);
						String[] prizeInfo = prize.keySet().iterator().next();
						ItemStack prizeItem = prize.get(prizeInfo);
						ItemMeta prizeItemMeta = prizeItem.getItemMeta();
						prizeItemMeta.setDisplayName(item.getItemStack().getItemMeta().getDisplayName());
						prizeItem.setItemMeta(prizeItemMeta);
						item.setCustomName(prizeInfo[0]);
						item.setItemStack(prizeItem);
					} else if (i > 63 && i < 79 && i % 4 == 0) {
						HashMap<String[], ItemStack> prize = getLootChestPrize(player, lootString, lootItems);
						String[] prizeInfo = prize.keySet().iterator().next();
						ItemStack prizeItem = prize.get(prizeInfo);
						ItemMeta prizeItemMeta = prizeItem.getItemMeta();
						prizeItemMeta.setDisplayName(item.getItemStack().getItemMeta().getDisplayName());
						prizeItem.setItemMeta(prizeItemMeta);
						item.setCustomName(prizeInfo[0]);
						item.setItemStack(prizeItem);
					} else if (i > 79 && i < 90 && i % 5 == 0) {
						HashMap<String[], ItemStack> prize = getLootChestPrize(player, lootString, lootItems);
						String[] prizeInfo = prize.keySet().iterator().next();
						ItemStack prizeItem = prize.get(prizeInfo);
						ItemMeta prizeItemMeta = prizeItem.getItemMeta();
						prizeItemMeta.setDisplayName(item.getItemStack().getItemMeta().getDisplayName());
						prizeItem.setItemMeta(prizeItemMeta);
						item.setCustomName(prizeInfo[0]);
						item.setItemStack(prizeItem);
					} else if (i == 100) {
						HashMap<String[], ItemStack> prize = getLootChestPrize(player, lootString, lootItems);
						String[] prizeInfo = prize.keySet().iterator().next();
						ItemStack prizeItem = prize.get(prizeInfo);
						ItemMeta prizeItemMeta = prizeItem.getItemMeta();
						prizeItemMeta.setDisplayName(item.getItemStack().getItemMeta().getDisplayName());
						prizeItem.setItemMeta(prizeItemMeta);
						item.setCustomName(prizeInfo[0]);
						item.setItemStack(prizeItem);
					} else if (i == 120) {
						HashMap<String[], ItemStack> prize = getLootChestPrize(player, lootString, lootItems);
						String[] prizeInfo = prize.keySet().iterator().next();
						ItemStack prizeItem = prize.get(prizeInfo);
						ItemMeta prizeItemMeta = prizeItem.getItemMeta();
						prizeItemMeta.setDisplayName(item.getItemStack().getItemMeta().getDisplayName());
						prizeItem.setItemMeta(prizeItemMeta);
						item.setCustomName(prizeInfo[0]);
						item.setItemStack(prizeItem);
					} else if (i == 135) {
						HashMap<String[], ItemStack> prize = getLootChestPrize(player, lootString, lootItems);
						String[] prizeInfo = prize.keySet().iterator().next();
						ItemStack prizeItem = prize.get(prizeInfo);
						ItemMeta prizeItemMeta = prizeItem.getItemMeta();
						prizeItemMeta.setDisplayName(item.getItemStack().getItemMeta().getDisplayName());
						prizeItem.setItemMeta(prizeItemMeta);
						item.setCustomName(prizeInfo[0]);
						item.setItemStack(prizeItem);
						for (String cmds : prizeInfo[1].split(";"))
							Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(),
									cmds.replace("%player%", player.getName()));
						// TODO Play particle
					}
				} else if (i == 165) {
					stand.remove();
					item.remove();
					stand1.remove();
					Main.removeList.remove(stand);
					OnEntityInteract.stopInteractList.remove(stand);
					Main.removeList.remove(stand1);
					OnEntityInteract.stopInteractList.remove(stand1);
					Main.removeList.remove(item);
					OnItemDespawn.stopDespawnList.remove(item);
					OnItemPickup.stopPickupList.remove(item);
				} else if (i > 169) { // Giggidy giggidy
					for (Player all : Bukkit.getOnlinePlayers()) {
						PacketPlayOutBlockAction packet = new PacketPlayOutBlockAction(
								new BlockPosition(location.getX(), location.getY(), location.getZ()),
								CraftMagicNumbers.getBlock(location.getBlock()), 1, 0);
						((CraftPlayer) all).getHandle().playerConnection.sendPacket(packet);
					}
				}
				if (i == 185 || !(player.isOnline())) {
					if (!(player.isOnline())) {
						stand.remove();
						item.remove();
						stand1.remove();
						Main.removeList.remove(stand);
						OnEntityInteract.stopInteractList.remove(stand);
						Main.removeList.remove(stand1);
						OnEntityInteract.stopInteractList.remove(stand1);
						Main.removeList.remove(item);
						OnItemDespawn.stopDespawnList.remove(item);
						OnItemPickup.stopPickupList.remove(item);
					}
					location4.getChunk().load(true);
					location4.getBlock().setType(Material.AIR);
					OnBlockBreak.blockReset.remove(location4);
					cancel();
				}
			}
		}.runTaskTimer(Main.plugin, 0, 1);
	}

	private static HashMap<String[], ItemStack> getLootChestPrize(final Player player, final String[] prizesChoose,
			final ItemStack[] itemsChoose) {
		HashMap<String[], ItemStack> returnMap = new HashMap<>();

		int index = ThreadLocalRandom.current().nextInt(0, prizesChoose.length);
		String[] prizeSplit = prizesChoose[index].split("\\:");
		returnMap.put(prizeSplit, itemsChoose[index]);

		return returnMap;
	}
}
