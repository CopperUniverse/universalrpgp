package com.stupidbot.universalcorep.utils;

import com.stupidbot.universalcorep.Main;
import com.stupidbot.universalcorep.mutations.Mutations;
import io.netty.util.internal.ThreadLocalRandom;
import org.apache.commons.lang.StringUtils;
import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.*;

@SuppressWarnings({ "all", "ConstantConditions" })
public class Stats {
	static Random random = new Random();
	public static HashMap<String, Integer> statsRollTimer = new HashMap<>();
	public static String globalXPMultiplier = "server:"
			+ Main.plugin.getConfig().getInt("Boosters.DefaultMultiplier.Experience");
	public static final Map<String, Integer> staminaLevels = new HashMap<>();
	public static final List<Material> miningItems = new ArrayList<>();
	static {
		miningItems.add(Material.WOOD_PICKAXE);
		miningItems.add(Material.STONE_PICKAXE);
		miningItems.add(Material.IRON_PICKAXE);
		miningItems.add(Material.DIAMOND_PICKAXE);
		miningItems.add(Material.GOLD_PICKAXE);
	}
	public static final List<Material> pvpItems = new ArrayList<>();
	static {
		pvpItems.add(Material.WOOD_SWORD);
	}

	public static int getMaxStamina(final Player player) {
		FileConfiguration configLoaded = Configs.loadPlayerFile(player, "Player Data");
		return (configLoaded.getInt("Stats.Proficiency") * 4) + 20;
	}

	public static void setStamina(final Player player, final int value) {
		double totalStamina = getMaxStamina(player);

		if (value > 0) {
			staminaLevels.put(player.getUniqueId().toString(), (int) Math.min(value, totalStamina));
			player.setFoodLevel((int) (20 / (totalStamina / staminaLevels.get(player.getUniqueId().toString()))));
			player.setSaturation(staminaLevels.get(player.getUniqueId().toString()));
		} else {
			staminaLevels.put(player.getUniqueId().toString(), 0);
			player.setFoodLevel(0);
			player.setSaturation(0);
		}
	}

	@SuppressWarnings("ConstantConditions")
	public static void giveEXP(final Player player, Integer amount, final boolean useMultiplier) throws IOException {
		if (player.hasPermission("universalrpgp.ytboost"))
			amount *= 2;
		File config = Configs.getPlayerFile(player.getUniqueId(), "Player Data");
		FileConfiguration configLoaded = Configs.loadFile(config);
		int lvl = configLoaded.getInt("Level.Level");
		int toNextLvl = configLoaded.getInt("Level.EXPNeeded");
		boolean holdingMineItem = player.getItemInHand() != null
				&& (miningItems.contains(player.getItemInHand().getType())
						|| pvpItems.contains(player.getItemInHand().getType()));
		String[] nameP = holdingMineItem
				? player.getItemInHand().getItemMeta().getDisplayName().replaceAll(",", "").split(" §f§l") : null;
		int lvlP = holdingMineItem ? Integer.parseInt(nameP[1]) : -1;
		String[] xpP = holdingMineItem ? ChatColor.stripColor(
				player.getItemInHand().getItemMeta().getLore().get(1).replace("Progress: ", "").replaceAll(",", ""))
				.split("/") : null;
		int toNextLvlP = holdingMineItem ? Integer.parseInt(xpP[1]) - Integer.parseInt(xpP[0]) : -1;
		ItemStack mineItem = holdingMineItem ? player.getItemInHand() : null;
		ItemMeta mineItemMeta = holdingMineItem ? mineItem.getItemMeta() : null;
		List<String> mineItemLore = holdingMineItem ? mineItemMeta.getLore() : null;
		Map<Enchantment, Integer> giveEnchs = new HashMap<>();
		for (int i = 0; i < (int) (useMultiplier ? amount * Double.parseDouble(globalXPMultiplier.split("\\:")[1])
				: amount); i++) {
			if (holdingMineItem) {
				toNextLvlP--;
				if (toNextLvlP < 1) {
					player.getWorld().playSound(player.getLocation(), Sound.FIREWORK_TWINKLE, 2, 0.8f);
					ParticleEffect.FIREWORKS_SPARK.display(1f, 1f, 1f, 1f, 25, player.getLocation().add(0, 1, 0), 256D);
					lvlP++;
					toNextLvlP = (int) (Math.pow(lvlP, 1.25) * 10);
					player.sendMessage("§eYour " + nameP[0] + " §eis now level §f§l"
							+ NumberFormat.getNumberInstance(Locale.US).format(lvlP));
					mineItemLore.set(4,
							"§6Familiarity: §a" + NumberFormat.getNumberInstance(Locale.US)
									.format(Integer.parseInt(
											mineItemLore.get(4).replace("§6Familiarity: §a", "").replaceAll(",", ""))
											+ ThreadLocalRandom.current().nextInt(0, 3 + 1)));
					switch (mineItem.getType()) {
					case WOOD_PICKAXE:
						mineItemLore.set(5,
								"§6Luck: §a" + NumberFormat.getNumberInstance(Locale.US)
										.format(Integer.parseInt(
												mineItemLore.get(5).replace("§6Luck: §a", "").replaceAll(",", ""))
												+ ThreadLocalRandom.current().nextInt(1, 5 + 1)));
						break;
					case STONE_PICKAXE:
						mineItemLore.set(5,
								"§6Luck: §a" + NumberFormat.getNumberInstance(Locale.US)
										.format(Integer.parseInt(
												mineItemLore.get(5).replace("§6Luck: §a", "").replaceAll(",", ""))
												+ ThreadLocalRandom.current().nextInt(1, 10 + 1)));
						break;
					case IRON_PICKAXE:
						mineItemLore.set(5,
								"§6Luck: §a" + NumberFormat.getNumberInstance(Locale.US)
										.format(Integer.parseInt(
												mineItemLore.get(5).replace("§6Luck: §a", "").replaceAll(",", ""))
												+ ThreadLocalRandom.current().nextInt(1, 15 + 1)));
						break;
					case DIAMOND_PICKAXE:
						mineItemLore.set(5,
								"§6Luck: §a" + NumberFormat.getNumberInstance(Locale.US)
										.format(Integer.parseInt(
												mineItemLore.get(5).replace("§6Luck: §a", "").replaceAll(",", ""))
												+ ThreadLocalRandom.current().nextInt(1, 20 + 1)));
						break;
					case GOLD_PICKAXE:
						mineItemLore.set(5,
								"§6Luck: §a" + NumberFormat.getNumberInstance(Locale.US)
										.format(Integer.parseInt(
												mineItemLore.get(5).replace("§6Luck: §a", "").replaceAll(",", ""))
												+ ThreadLocalRandom.current().nextInt(1, 25 + 1)));
						break;
					default:
						break;
					}
					if (configLoaded.getInt("Level.ItemsLevelled") < 1) {
						player.sendMessage(Text.sendCenteredMessage("§m+-----------------|-----------------+", 131));
						player.sendMessage(Text.sendCenteredMessage("§6§lTUTORIAL", 131));
						player.sendMessage(Text
								.sendCenteredMessage("§eWhen levelling up your equipment it may §c§lMUTATE§e!", 131));
						player.sendMessage("\n");
						player.sendMessage(Text.sendCenteredMessage("§eMutations are like enchantments,", 131));
						player.sendMessage(
								Text.sendCenteredMessage("§ebut these can be §a§lGOOD§e or §4§lBAD§e!", 131));
						player.sendMessage("\n");
						player.sendMessage(
								Text.sendCenteredMessage("§eYour item's §bfamiliarity§e stat lowers the", 131));
						player.sendMessage(Text.sendCenteredMessage("§echance of getting a bad mutation!", 131));
						player.sendMessage(Text.sendCenteredMessage("§m+-----------------|-----------------+", 131));
					}
					if (ThreadLocalRandom.current().nextInt(0, 101) <= 20) {
						player.getWorld().playSound(player.getLocation(), Sound.ORB_PICKUP, 2, 0.5f);
						ParticleEffect.ENCHANTMENT_TABLE.display(1f, 1f, 1f, 1f, 25, player.getLocation().add(0, 1, 0),
								256D);
						boolean good = ThreadLocalRandom.current().nextInt(0,
								(Integer.parseInt(
										mineItemLore.get(4).replace("§6Familiarity: §a", "").replaceAll(",", "")) / 15)
										+ 1) != 0;
						Enchantment giveEnchant;
						if (miningItems.contains(mineItem.getType()))
							giveEnchant = good
									? Mutations.GOOD_PICKAXES
											.get(ThreadLocalRandom.current().nextInt(0, Mutations.GOOD_PICKAXES.size()))
									: Mutations.BAD_PICKAXES
											.get(ThreadLocalRandom.current().nextInt(0, Mutations.BAD_PICKAXES.size()));
						else
							giveEnchant = good
									? Mutations.GOOD_SWORDS
											.get(ThreadLocalRandom.current().nextInt(0, Mutations.GOOD_SWORDS.size()))
									: Mutations.BAD_SWORDS
											.get(ThreadLocalRandom.current().nextInt(0, Mutations.BAD_SWORDS.size()));
						int giveEnchantLvl = mineItem.getEnchantments() != null
								&& mineItem.getEnchantments().containsKey(giveEnchant)
										? mineItem.getEnchantmentLevel(giveEnchant) + 1 : 1;
						if (giveEnchantLvl <= giveEnchant.getMaxLevel()) {
							giveEnchs.put(giveEnchant, giveEnchantLvl);
							mineItem.addUnsafeEnchantment(giveEnchant, giveEnchantLvl);

							if (mineItemLore.get(8).contains("§7Level me up and I may mutate!"))
								mineItemLore.set(8, (good ? "§a" : "§4") + giveEnchant.getName() + " I");
							else if (giveEnchantLvl == 1)
								mineItemLore.add((good ? "§a" : "§4") + giveEnchant.getName() + " I");
							else
								for (int l = 8; l < mineItemLore.size(); l++)
									if (ChatColor.stripColor(mineItemLore.get(l)).contains(giveEnchant.getName())) {
										mineItemLore.set(l, (good ? "§a" : "§4") + giveEnchant.getName() + " "
												+ Text.toRoman(giveEnchantLvl));
										break;
									}
							Text.sendJSONMessage(player,
									"[\"\",{\"text\":\"" + giveEnchant.getName() + " " + Text.toRoman(giveEnchantLvl)
											+ " (" + NumberFormat.getNumberInstance(Locale.US).format(giveEnchantLvl)
											+ ")" + " §7(hover)\",\"color\":\"" + (good ? "green" : "dark_red")
											+ "\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\""
											+ giveEnchant.getName() + "\n\",\"color\":\""
											+ (good ? "green" : "dark_red")
											+ "\",\"bold\":\"true\"},{\"text\":\"Description: \",\"color\":\"gray\"},{\"text\":\""
											+ ((Mutations) giveEnchant).getDescription()
											+ "\n\",\"color\":\"light_purple\"},{\"text\":\"Max Level: \",\"color\":\"gray\"},{\"text\":\""
											+ giveEnchant.getMaxLevel()
											+ "\",\"color\":\"light_purple\"}]}}},{\"text\":\" has been added to your \",\"color\":\"yellow\"},{\"text\":\""
											+ nameP[0] + "\"}]");
						}
					}
					configLoaded.set("Level.ItemsLevelled", configLoaded.getInt("Level.ItemsLevelled") + 1);
				}
			}
			toNextLvl--;
			if (toNextLvl < 1) {
				lvl++;
				toNextLvl = (int) (Math.pow(lvl, 2.5) * 5);
				if (lvl > 1) {
					for (Player all : Bukkit.getOnlinePlayers())
						if (all != player)
							all.sendMessage(
									StringUtils.substring(Main.chat.getPlayerPrefix(player).replaceAll("&", "§"), 0, 2)
											+ player.getName() + "§e is now a §a" + Text.formatExperience(lvl));
					player.getWorld().playSound(player.getLocation(), Sound.ENDERDRAGON_GROWL, 2, 1.5f);
					if (lvl == 2) {
						player.sendMessage(Text.sendCenteredMessage("§m+-----------------|-----------------+", 131));
						player.sendMessage(Text.sendCenteredMessage("§6§lTUTORIAL", 131));
						player.sendMessage(
								Text.sendCenteredMessage("§eWhen you level up you'll get a §dLoot Egg§e!", 131));
						player.sendMessage(
								Text.sendCenteredMessage("§eCrack them open on the §aleft-hand side of spawn§e.", 131));
						player.sendMessage("\n");
						player.sendMessage(
								Text.sendCenteredMessage("§eWhen levelling up your stats will also increase§e!", 131));
						player.sendMessage(Text.sendCenteredMessage("§m+-----------------|-----------------+", 131));
					}
					int add = ThreadLocalRandom.current().nextInt(1, 5);
					int add1 = ThreadLocalRandom.current().nextInt(1, 5);
					int add2 = ThreadLocalRandom.current().nextInt(1, 5);
					player.sendMessage(Text.sendCenteredMessage("§m+-----------------|-----------------+", 131));
					player.sendMessage(Text.sendCenteredMessage("§e§lLEVEL UP§r " + Text.formatExperience(lvl), 131));
					Text.sendJSONMessage(player,
							"{\"text\":\""
									+ Text.sendCenteredMessage("§aKnowledge §7(hover)§f: " + "§3"
											+ NumberFormat.getNumberInstance(Locale.US)
													.format(configLoaded.getInt("Stats.Knowledge"))
											+ " §e+ §3" + add + " §e= §b"
											+ NumberFormat.getNumberInstance(Locale.US)
													.format(configLoaded.getInt("Stats.Knowledge") + add),
											131)
									+ "\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"§7§oIncreases chance of finding good items whilst mining!\"}]}}}");
					Text.sendJSONMessage(
							player, "{\"text\":\""
									+ Text.sendCenteredMessage("§aProficiency §7(hover)§f: " + "§3"
											+ NumberFormat.getNumberInstance(Locale.US)
													.format(configLoaded.getInt("Stats.Proficiency"))
											+ " §e+ §3" + add1 + " §e= §b"
											+ NumberFormat.getNumberInstance(Locale.US)
													.format(configLoaded.getInt("Stats.Proficiency") + add1),
											131)
									+ "\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"§7§oDirectly increases stamina!\"}]}}}");
					Text.sendJSONMessage(player,
							"{\"text\":\""
									+ Text.sendCenteredMessage("§aGuts §7(hover)§f: " + "§3"
											+ NumberFormat.getNumberInstance(Locale.US)
													.format(configLoaded.getInt("Stats.Guts"))
											+ " §e+ §3" + add2 + " §e= §b"
											+ NumberFormat.getNumberInstance(Locale.US)
													.format(configLoaded.getInt("Stats.Guts") + add2),
											131)
									+ "\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"§7§oComing soon (P.V.P. related)!\"}]}}}");
					player.sendMessage(Text.sendCenteredMessage("§m+-----------------|-----------------+", 131));

					configLoaded.set("Stats.Knowledge", configLoaded.getInt("Stats.Knowledge") + add);
					configLoaded.set("Stats.Proficiency", configLoaded.getInt("Stats.Proficiency") + add1);
					configLoaded.set("Stats.Guts", configLoaded.getInt("Stats.Guts") + add2);
					configLoaded.set("LootEggs.Amount", configLoaded.getInt("LootEggs.Amount") + 1);

					Text.showTitle(player, "{\"text\":\"LEVEL UP\",\"color\":\"yellow\",\"bold\":\"true\"}", 10, 60,
							10);
					Text.showSubtitle(player,
							"[\"\",{\"text\":\"" + Text.toRoman(lvl - 1)
									+ "\",\"color\":\"green\"},{\"text\":\" ➩ \",\"color\":\"gold\"},{\"text\":\""
									+ Text.toRoman(lvl) + "\",\"color\":\"green\"}]",
							10, 60, 10);

					Text.showActionBar(player, "{\"text\":\"§6Loot Egg §a+1\"}");

					new BukkitRunnable() {
						double t = 0;

						@Override
						public void run() {
							for (double i = 0; i < 3; i += 1) {
								double dy = 1 + Math.sin(0.1 * (t - (i % 3))) * Math.cos(0.25 * (t - (i * 3)));
								double dx = Math.cos(t - i) * ((dy + 1.5) / 3);
								double dz = Math.sin(t - i) * ((dy + 1.5) / 3);

								Location pLoc = new Location(player.getLocation().getWorld(),
										player.getLocation().getX() + dx, player.getLocation().getY() + dy,
										player.getLocation().getZ() + dz);
								ParticleEffect.REDSTONE.display(
										new ParticleEffect.OrdinaryColor(ThreadLocalRandom.current().nextInt(0, 256),
												ThreadLocalRandom.current().nextInt(0, 256),
												ThreadLocalRandom.current().nextInt(0, 256)),
										pLoc, 256);
							}
							if (t > 125 || !(player.isOnline()))
								cancel();
							else
								t += 0.25;
						}
					}.runTaskTimerAsynchronously(Main.plugin, 0, 1);
				}
			}
		}
		if (holdingMineItem) {
			mineItemMeta.setDisplayName(nameP[0] + " §f§l" + NumberFormat.getNumberInstance(Locale.US).format(lvlP));
			mineItemLore.set(1,
					"§fProgress: §b"
							+ NumberFormat.getNumberInstance(Locale.US)
									.format((int) (Math.pow(lvlP, 1.25) * 10) - toNextLvlP)
							+ "§7/§b"
							+ NumberFormat.getNumberInstance(Locale.US).format((int) (Math.pow(lvlP, 1.25) * 10)));
			mineItemMeta.setLore(mineItemLore);
			mineItem.setItemMeta(mineItemMeta);
			mineItem.addUnsafeEnchantments(giveEnchs);

			player.setItemInHand(mineItem);
			player.updateInventory();
		}
		configLoaded.set("Level.EXPNeeded", toNextLvl);
		configLoaded.set("Level.Level", lvl);
		configLoaded.save(config);
		updateEXP(player);
	}

	public static void updateEXP(final Player player) {
		FileConfiguration configLoaded = Configs.loadPlayerFile(player, "Player Data");
		int lvl = configLoaded.getInt("Level.Level");
		int toNextLvl = configLoaded.getInt("Level.EXPNeeded");
		int expNeeded = (int) (Math.pow(lvl, 2.5) * 5);
		player.setExp((1F / expNeeded) * (expNeeded - toNextLvl));
		player.setLevel(lvl);
	}
}
