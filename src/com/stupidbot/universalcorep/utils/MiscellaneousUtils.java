package com.stupidbot.universalcorep.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.SkullMeta;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import io.netty.util.internal.ThreadLocalRandom;

public class MiscellaneousUtils {
	public static <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map) {
		Comparator<K> valueComparator = (k1, k2) -> {
			int compare = map.get(k1).compareTo(map.get(k2));
			if (compare == 0)
				return 1;
			else
				return compare;
		};
		Map<K, V> sortedByValues = new TreeMap<>(valueComparator);
		sortedByValues.putAll(map);
		return sortedByValues;
	}

	public static boolean locIsInRegion(Location location, String regionName) {
		RegionManager regionManager = WGBukkit.getRegionManager(location.getWorld());
		ApplicableRegionSet regions = regionManager.getApplicableRegions(location);
		for (ProtectedRegion region : regions)
			if (region.getId().equalsIgnoreCase(regionName))
				return true;
		return false;
	}

	public static void setNonPlayerSkullOwner(final SkullMeta skull, final String url) {
		GameProfile profile = new GameProfile(UUID.randomUUID(), null);
		byte[] encodedData = Base64.getEncoder().encode(url.getBytes());
		profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
		Field profileField;
		try {
			profileField = skull.getClass().getDeclaredField("profile");
			profileField.setAccessible(true);
			profileField.set(skull, profile);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
			e1.printStackTrace();
		}
	}

	public static String getCardinalDirection8(Player player) {
		double rotation = (player.getLocation().getYaw() - 90) % 360;
		if (rotation < 0)
			rotation += 360.0;
		if (0 <= rotation && rotation < 22.5)
			return "N";
		else if (22.5 <= rotation && rotation < 67.5)
			return "NE";
		else if (67.5 <= rotation && rotation < 112.5)
			return "E";
		else if (112.5 <= rotation && rotation < 157.5)
			return "SE";
		else if (157.5 <= rotation && rotation < 202.5)
			return "S";
		else if (202.5 <= rotation && rotation < 247.5)
			return "SW";
		else if (247.5 <= rotation && rotation < 292.5)
			return "W";
		else if (292.5 <= rotation && rotation < 337.5)
			return "NW";
		else if (337.5 <= rotation && rotation < 360.0)
			return "N";
		else
			return null;
	}

	public static String getCardinalDirection4(Player player) {
		double rotation = (player.getLocation().getYaw() - 90) % 360;
		if (rotation < 0)
			rotation += 360.0;
		if (0 <= rotation && rotation < 22.5)
			return "N";
		else if (22.5 <= rotation && rotation < 112.5)
			return "E";
		else if (112.5 <= rotation && rotation < 202.5)
			return "S";
		else if (202.5 <= rotation && rotation < 292.5)
			return "W";
		else if (292.5 <= rotation && rotation < 360.0)
			return "N";
		else
			return null;
	}

	public static double getBiasedRandom(final double min, final double max, final double scale) {
		Double d = max;
		int i = 1;
		ArrayList<Double> numList = new ArrayList<>();

		while (d >= min) {
			for (int t = i; t > 0; t--)
				numList.add(d);

			i++;
			d = d - scale;
		}

		return numList.get(ThreadLocalRandom.current().nextInt(numList.size()));
	}
}
