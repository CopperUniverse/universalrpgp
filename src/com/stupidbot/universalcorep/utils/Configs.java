package com.stupidbot.universalcorep.utils;

import com.stupidbot.universalcorep.Main;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class Configs {
	public static void setupPlayer(final Player player) throws IOException {
		File playerFile = getPlayerFile(player, "Player Data");
		if (!(playerFile.exists()))
			playerFile.createNewFile();
		FileConfiguration config = loadFile(playerFile);
		config.set("Name.Name", player.getName());
		config.set("Name.Prefix", Main.chat.getPlayerPrefix(player));
		config.set("Name.Suffix", Main.chat.getPlayerSuffix(player));
		if (!(config.isSet("Stats.Knowledge")))
			config.set("Stats.Knowledge", 0);
		if (!(config.isSet("Stats.Proficiency")))
			config.set("Stats.Proficiency", 0);
		if (!(config.isSet("Stats.Guts")))
			config.set("Stats.Guts", 0);
		if (!(config.isSet("Stats.Reputation")))
			config.set("Stats.Reputation", 0);
		if (!(config.isSet("Stats.Votes")))
			config.set("Stats.Votes", 0);
		if (!(config.isSet("Stats.Kills")))
			config.set("Stats.Kills", 0);
		if (!(config.isSet("Stats.Deaths")))
			config.set("Stats.Deaths", 0);
		if (!(config.isSet("Stats.FirstJoin")))
			config.set("Stats.FirstJoin", new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
		if (!(config.isSet("Stats.LastPlayed")))
			config.set("Stats.LastPlayed", new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
		if (!(config.isSet("Stats.BlocksMined")))
			config.set("Stats.BlocksMined", 0);
		if (!(config.isSet("Stats.Stamina")))
			config.set("Stats.Stamina", 25);
		if (!(config.isSet("LootEggs.Amount")))
			config.set("LootEggs.Amount", 0);
		if (!(config.isSet("LootEggs.Eggshells")))
			config.set("LootEggs.Eggshells",
					config.isSet("LootEggs.Prizes.Dupes") ? config.getInt("LootEggs.Prizes.Dupes") * 10 : 0);
		if (!(config.isSet("LootEggs.TotalEggshells")))
			config.set("LootEggs.TotalEggshells", config.getInt("LootEggs.Eggshells"));
		if (!(config.isSet("LootEggs.Opened")))
			config.set("LootEggs.Opened", 0);
		if (!(config.isSet("LootEggs.Prizes.New")))
			config.set("LootEggs.Prizes.New", 0);
		if (!(config.isSet("LootEggs.Prizes.Dupes")))
			config.set("LootEggs.Prizes.Dupes", 0);
		if (!(config.isSet("LootEggs.Prizes.Legendaries")))
			config.set("LootEggs.Prizes.Legendaries", 0);
		if (!(config.isSet("LootEggs.Prizes.Rares")))
			config.set("LootEggs.Prizes.Rares", 0);
		if (!(config.isSet("LootEggs.Prizes.Commons")))
			config.set("LootEggs.Prizes.Commons", 0);
		if (!(config.isSet("Ships.Flights")))
			config.set("Ships.Flights", 0);
		if (!(config.isSet("Ships.InstaFlightTickets")))
			config.set("Ships.InstaFlightTickets", 1);
		/*
		 * if (!(config.isSet("Ships.Recent"))) config.set("Ships.Recent", new
		 * ArrayList<String>());
		 */
		if (!(config.isSet("Timers.Rewards.InstaFlightTickets"))) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.HOUR, 18);
			config.set("Timers.Rewards.InstaFlightTickets",
					new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(cal.getTime()));
		}
		if (!(config.isSet("Level.EXPNeeded")))
			config.set("Level.EXPNeeded", 5);
		if (!(config.isSet("Level.Level")))
			config.set("Level.Level", 1);
		if (!(config.isSet("Level.ItemsLevelled")))
			config.set("Level.ItemsLevelled", 0);
		if (!(config.isSet("Cosmetics.KillMessage")))
			config.set("Cosmetics.KillMessage", "default");
		if (!(config.isSet("Cosmetics.ChatTag")))
			config.set("Cosmetics.ChatTag", "nothing");
		if (!(config.isSet("Cosmetics.ProjectileTrail")))
			config.set("Cosmetics.ProjectileTrail", "default");
		if (!(config.isSet("Cosmetics.ShipSkin")))
			config.set("Cosmetics.ShipSkin", "default");
		if (!(config.isSet("Punishments.IP")))
			config.set("Punishments.IP", player.getAddress().toString());
		if (!(config.isSet("Punishments.History")))
			config.set("Punishments.History", new ArrayList<String>());
		if (!(config.isSet("Codes.Referral.Code")))
			config.set("Codes.Referral.Code", Codes.genCode("REF-", 8));
		if (!(config.isSet("Codes.Referral.Referrals")))
			config.set("Codes.Referral.Referrals", 0);
		if (!(config.isSet("Codes.Used")))
			config.set("Codes.Used", new ArrayList<String>());
		config.save(playerFile);

		playerFile = getPlayerFile(player, "Player Vaults");
		if (!(playerFile.exists()))
			playerFile.createNewFile();
		config = loadFile(playerFile);
		if (!(config.isSet("Vault.1.Items")))
			config.set("Vault.1.Items", new ArrayList<String>());
		if (!(config.isSet("Vault.2.Items")))
			config.set("Vault.2.Items", new ArrayList<String>());
		if (!(config.isSet("Vault.3.Items")))
			config.set("Vault.3.Items", new ArrayList<String>());
		if (!(config.isSet("Vault.4.Items")))
			config.set("Vault.4.Items", new ArrayList<String>());
		if (!(config.isSet("Vault.5.Items")))
			config.set("Vault.5.Items", new ArrayList<String>());
		config.save(playerFile);
	}

	public static File getPlayerFile(final Player player, final String folder) {
		return new File(
				Main.plugin.getDataFolder() + File.separator + folder + File.separator + player.getUniqueId() + ".yml");
	}

	public static File getPlayerFile(final UUID uuid, final String folder) {
		return new File(Main.plugin.getDataFolder() + File.separator + folder + File.separator + uuid + ".yml");
	}

	public static FileConfiguration loadFile(final File file) {
		return YamlConfiguration.loadConfiguration(file);
	}

	public static FileConfiguration loadPlayerFile(final Player player, final String folder) {
		return YamlConfiguration.loadConfiguration(getPlayerFile(player, folder));
	}

	public static void loadConfiguration() {
		Main.plugin.getConfig().addDefault("Boosters.DefaultMultiplier.Experience", 1);
		Main.plugin.getConfig().addDefault("Boosters.DefaultMultiplier.SellPrices", 1);
		Main.plugin.getConfig().options().copyDefaults(true);
		Main.plugin.saveConfig();
	}
}