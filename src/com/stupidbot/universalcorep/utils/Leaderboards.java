
package com.stupidbot.universalcorep.utils;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.stupidbot.universalcorep.Main;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.NumberFormat;
import java.util.*;

@SuppressWarnings("all")
public class Leaderboards {
	private static final Hologram levelLeaderboard = HologramsAPI.createHologram(Main.plugin,
			new Location(Bukkit.getWorld("world"), -2.5, 103, -2.5));
	private static final Hologram voteLeaderboard = HologramsAPI.createHologram(Main.plugin,
			new Location(Bukkit.getWorld("world"), -18.5, 103, -2.5));
	private static final Hologram killLeaderboard = HologramsAPI.createHologram(Main.plugin,
			new Location(Bukkit.getWorld("world"), -2.5, 103, 2.5));
	private static final Hologram deathLeaderboard = HologramsAPI.createHologram(Main.plugin,
			new Location(Bukkit.getWorld("world"), -18.5, 103, 2.5));
	private static final Hologram blockLeaderboard = HologramsAPI.createHologram(Main.plugin,
			new Location(Bukkit.getWorld("world"), -2.5, 103, 7.5));
	private static final Hologram referralLeaderboard = HologramsAPI.createHologram(Main.plugin,
			new Location(Bukkit.getWorld("world"), -18.5, 103, 7.5));
	static {
		levelLeaderboard.appendTextLine("�4�lLoading Leaderboard Data");
		voteLeaderboard.appendTextLine("�4�lLoading Leaderboard Data");
		killLeaderboard.appendTextLine("�4�lLoading Leaderboard Data");
		deathLeaderboard.appendTextLine("�4�lLoading Leaderboard Data");
		referralLeaderboard.appendTextLine("�4�lLoading Leaderboard Data");
		blockLeaderboard.appendTextLine("�4�lLoading Leaderboard Data");
	}
	private static int levelsT = 0;
	private static int killsT = 0;
	private static int deathsT = 0;
	private static int referralsT = 0;
	private static int votesT = 0;
	private static int blocksT = 0;

	public static void startLeaderboards() {
		new BukkitRunnable() {
			boolean go = true;

			@SuppressWarnings("MagicConstant")
			@Override
			public void run() {
/*				if ((Calendar.getInstance().get(Calendar.HOUR) == 23 || Calendar.getInstance().get(Calendar.HOUR) == 11)
						&& Calendar.getInstance().get(Calendar.MINUTE) == 45)
					new BukkitRunnable() {
						int i = 900;

						@Override
						public void run() {
							if (i == 0) {
								for (Player all : Bukkit.getOnlinePlayers())
									all.kickPlayer("�cServer is restarting!�d Please check back in 5 minutes!");
								Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "restart");
							} else {
								for (Player all : Bukkit.getOnlinePlayers())
									Text.showActionBar(all, "{\"text\":\"�bAuto Restart in �e�l" + +(i / 60) + "m "
											+ (i % 60) + "s" + "\"}");
								i--;
							}
						}
					}.runTaskTimer(Main.plugin, 0, 20);*/
				//noinspection MagicConstant
				if (Calendar.getInstance().get(Calendar.MINUTE) == 0
						|| Calendar.getInstance().get(Calendar.MINUTE) == 30 || go) {
					go = false;
					Bukkit.broadcastMessage("�a�lUpdating Leaderboards");
					try {
						updateLeaderboards();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}.runTaskTimer(Main.plugin, 1200 - (Calendar.getInstance().get(Calendar.SECOND) * 20), 1200);
	}

	public static void updateLeaderboards() throws IOException {
		long startTime = System.currentTimeMillis();
		levelLeaderboard.clearLines();
		voteLeaderboard.clearLines();
		killLeaderboard.clearLines();
		deathLeaderboard.clearLines();
		referralLeaderboard.clearLines();
		blockLeaderboard.clearLines();

		levelLeaderboard.appendTextLine("�a�l�nLevel Leaderboard");
		levelLeaderboard.appendTextLine("");
		levelLeaderboard.appendTextLine("");
		levelLeaderboard.appendTextLine("");
		voteLeaderboard.appendTextLine("�a�l�nVote Leaderboard");
		voteLeaderboard.appendTextLine("");
		voteLeaderboard.appendTextLine("");
		voteLeaderboard.appendTextLine("");
		killLeaderboard.appendTextLine("�a�l�nKill Leaderboard");
		killLeaderboard.appendTextLine("");
		killLeaderboard.appendTextLine("");
		killLeaderboard.appendTextLine("");
		deathLeaderboard.appendTextLine("�a�l�nDeath Leaderboard");
		deathLeaderboard.appendTextLine("");
		deathLeaderboard.appendTextLine("");
		deathLeaderboard.appendTextLine("");
		referralLeaderboard.appendTextLine("�a�l�nReferral Leaderboard");
		referralLeaderboard.appendTextLine("");
		referralLeaderboard.appendTextLine("");
		referralLeaderboard.appendTextLine("");
		blockLeaderboard.appendTextLine("�a�l�nBlocks Leaderboard");
		blockLeaderboard.appendTextLine("");
		blockLeaderboard.appendTextLine("");
		blockLeaderboard.appendTextLine("");

		TreeMap<String, Integer> levels = new TreeMap<>();
		TreeMap<String, Integer> votes = new TreeMap<>();
		TreeMap<String, Integer> kills = new TreeMap<>();
		TreeMap<String, Integer> deaths = new TreeMap<>();
		TreeMap<String, Integer> referrals = new TreeMap<>();
		TreeMap<String, Integer> blocks = new TreeMap<>();
		levelsT = 0;
		killsT = 0;
		deathsT = 0;
		referralsT = 0;
		votesT = 0;
		blocksT = 0;
		if (new File(Main.plugin.getDataFolder() + File.separator + "Player Data").exists()
				&& Objects.requireNonNull(Objects.requireNonNull(new File(Main.plugin.getDataFolder() + File.separator + "Player Data").list())).length > 0)
			Files.list(new File(Main.plugin.getDataFolder() + File.separator + "Player Data").toPath())
					.forEach(path -> {
						File file = path.toFile();
						YamlConfiguration fileLoaded = YamlConfiguration.loadConfiguration(file);
						String name = StringUtils.substring(fileLoaded.getString("Name.Prefix").replaceAll("&", "�"), 0,
								2) + fileLoaded.getString("Name.Name") + ":" + file.getName().replace(".yml", "");
						final int add = fileLoaded.getInt("Level.Level");
						levels.put(name, add);
						levelsT += add;
						final int add1 = fileLoaded.getInt("Stats.Kills");
						kills.put(name, add1);
						killsT += add1;
						final int add2 = fileLoaded.getInt("Stats.Deaths");
						deaths.put(name, add2);
						deathsT += add2;
						final int add3 = fileLoaded.getInt("Codes.Referral.Referrals");
						referrals.put(name, add3);
						referralsT += add3;
						final int add4 = fileLoaded.getInt("Stats.Votes");
						votes.put(name, add4);
						votesT += add4;
						final int add5 = fileLoaded.getInt("Stats.BlocksMined");
						blocks.put(name, add5);
						blocksT += add5;
					});

		NavigableMap<String, Integer> sortedMapL = ((NavigableMap<String, Integer>) MiscellaneousUtils
				.sortByValues(levels)).descendingMap();
		Iterator<Integer> iteratorL0 = sortedMapL.values().iterator();
		Iterator<String> iteratorL1 = sortedMapL.keySet().iterator();
		NavigableMap<String, Integer> sortedMapV = ((NavigableMap<String, Integer>) MiscellaneousUtils
				.sortByValues(votes)).descendingMap();
		Iterator<Integer> iteratorV0 = sortedMapV.values().iterator();
		Iterator<String> iteratorV1 = sortedMapV.keySet().iterator();
		NavigableMap<String, Integer> sortedMapK = ((NavigableMap<String, Integer>) MiscellaneousUtils
				.sortByValues(kills)).descendingMap();
		Iterator<Integer> iteratorK0 = sortedMapK.values().iterator();
		Iterator<String> iteratorK1 = sortedMapK.keySet().iterator();
		NavigableMap<String, Integer> sortedMapD = ((NavigableMap<String, Integer>) MiscellaneousUtils
				.sortByValues(deaths)).descendingMap();
		Iterator<Integer> iteratorD0 = sortedMapD.values().iterator();
		Iterator<String> iteratorD1 = sortedMapD.keySet().iterator();
		NavigableMap<String, Integer> sortedMapR = ((NavigableMap<String, Integer>) MiscellaneousUtils
				.sortByValues(referrals)).descendingMap();
		Iterator<Integer> iteratorR0 = sortedMapR.values().iterator();
		Iterator<String> iteratorR1 = sortedMapR.keySet().iterator();
		NavigableMap<String, Integer> sortedMapB = ((NavigableMap<String, Integer>) MiscellaneousUtils
				.sortByValues(blocks)).descendingMap();
		Iterator<Integer> iteratorB0 = sortedMapB.values().iterator();
		Iterator<String> iteratorB1 = sortedMapB.keySet().iterator();
		for (int i = 1; i < 11; i++) {
			if (i - 1 < levels.size()) {
				Integer amount = iteratorL0.next();
				String[] name = iteratorL1.next().split("\\:");
				levelLeaderboard.appendTextLine("�e" + i + ". " + name[0] + " �3- �e"
						+ NumberFormat.getNumberInstance(Locale.US).format(amount));
				OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(name[1]));
				Main.perms.playerAdd("world", player, "universalrpgp.chat_tag.grinder");
			} else
				levelLeaderboard.appendTextLine("�e" + i + ". �7Unclaimed");
			if (i - 1 < votes.size()) {
				Integer amount = iteratorV0.next();
				String[] name = iteratorV1.next().split("\\:");
				voteLeaderboard.appendTextLine("�e" + i + ". " + name[0] + " �3- �e"
						+ NumberFormat.getNumberInstance(Locale.US).format(amount));
				OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(name[1]));
				Main.perms.playerAdd("world", player, "universalrpgp.chat_tag.voter");
			} else
				voteLeaderboard.appendTextLine("�e" + i + ". �7Unclaimed");
			if (i - 1 < kills.size()) {
				Integer amount = iteratorK0.next();
				String[] name = iteratorK1.next().split("\\:");
				killLeaderboard.appendTextLine("�e" + i + ". " + name[0] + " �3- �e"
						+ NumberFormat.getNumberInstance(Locale.US).format(amount));
				OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(name[1]));
				Main.perms.playerAdd("world", player, "universalrpgp.chat_tag.slayer");
			} else
				killLeaderboard.appendTextLine("�e" + i + ". �7Unclaimed");
			if (i - 1 < deaths.size()) {
				Integer amount = iteratorD0.next();
				String[] name = iteratorD1.next().split("\\:");
				deathLeaderboard.appendTextLine("�e" + i + ". " + name[0] + " �3- �e"
						+ NumberFormat.getNumberInstance(Locale.US).format(amount));
				OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(name[1]));
				Main.perms.playerAdd("world", player, "universalrpgp.chat_tag.bad");
			} else
				deathLeaderboard.appendTextLine("�e" + i + ". �7Unclaimed");
			if (i - 1 < referrals.size()) {
				Integer amount = iteratorR0.next();
				String[] name = iteratorR1.next().split("\\:");
				referralLeaderboard.appendTextLine("�e" + i + ". " + name[0] + " �3- �e"
						+ NumberFormat.getNumberInstance(Locale.US).format(amount));
				OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(name[1]));
				Main.perms.playerAdd("world", player, "universalrpgp.chat_tag.advertiser");
			} else
				referralLeaderboard.appendTextLine("�e" + i + ". �7Unclaimed");
			if (i - 1 < blocks.size()) {
				Integer amount = iteratorB0.next();
				String[] name = iteratorB1.next().split("\\:");
				blockLeaderboard.appendTextLine("�e" + i + ". " + name[0] + " �3- �e"
						+ NumberFormat.getNumberInstance(Locale.US).format(amount));
				OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(name[1]));
				Main.perms.playerAdd("world", player, "universalrpgp.chat_tag.breaker");
			} else
				blockLeaderboard.appendTextLine("�e" + i + ". �7Unclaimed");
		}
		levelLeaderboard
				.appendTextLine("�6�lGlobal Level �3- �e" + NumberFormat.getNumberInstance(Locale.US).format(levelsT));
		levelLeaderboard.appendTextLine("�d�oUpdates Every 30 Minutes");
		voteLeaderboard
				.appendTextLine("�6�lGlobal Votes �3- �e" + NumberFormat.getNumberInstance(Locale.US).format(votesT));
		voteLeaderboard.appendTextLine("�d�oUpdates Every 30 Minutes");
		killLeaderboard
				.appendTextLine("�6�lGlobal Kills �3- �e" + NumberFormat.getNumberInstance(Locale.US).format(killsT));
		killLeaderboard.appendTextLine("�d�oUpdates Every 30 Minutes");
		deathLeaderboard
				.appendTextLine("�6�lGlobal Deaths �3- �e" + NumberFormat.getNumberInstance(Locale.US).format(deathsT));
		deathLeaderboard.appendTextLine("�d�oUpdates Every 30 Minutes");
		referralLeaderboard.appendTextLine(
				"�6�lGlobal Referrals �3- �e" + NumberFormat.getNumberInstance(Locale.US).format(referralsT));
		referralLeaderboard.appendTextLine("�d�oUpdates Every 30 Minutes");
		blockLeaderboard.appendTextLine(
				"�6�lGlobal Blocks Mined �3- �e" + NumberFormat.getNumberInstance(Locale.US).format(blocksT));
		blockLeaderboard.appendTextLine("�d�oUpdates Every 30 Minutes");

		System.out.println("Updating leaderboards took: "
				+ NumberFormat.getNumberInstance(Locale.US).format(System.currentTimeMillis() - startTime)
				+ " milliseconds");
	}
}
