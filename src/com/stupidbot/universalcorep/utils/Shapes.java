package com.stupidbot.universalcorep.utils;

import org.bukkit.Location;
import org.bukkit.util.Vector;

class Shapes {
	public static Location getLocAroundCircle(Location center, double radius, double angleInRadian) {
		double x = center.getX() + radius * Math.cos(angleInRadian);
		double z = center.getZ() + radius * Math.sin(angleInRadian);
		double y = center.getY();

		Location loc = new Location(center.getWorld(), x, y, z);
		Vector difference = center.toVector().clone().subtract(loc.toVector());
		loc.setDirection(difference);

		return loc;
	}
}
